================================
python によるデジタル信号処理
================================

| Last Change: 09-Mar-2016.
| author : qh73xe
|

このページでは python によるデジタル信号処理に関する記述を行います。
これに関しては特に教本を用意している訳ではなく、Web に転がっている情報をまとめていく感じになります。

.. contents::
   :depth: 2
   :local:

プログラム基礎
========================

まずは、信号処理に必要な基本的な情報について記述します。

使用環境
-----------------

- python 3 系
- Scipy: 科学演算の為のライブラリ
- Matplotlib: 描画用ライブラリ

各種環境構築はそれぞれのOS環境に合わせてください。
基本的に Win 以外では python は入っているはずです。

例えば Fedora の場合以下のように環境を構築します。

.. code-block:: bash

   sudo dnf install python3
   sudo dnf install python3-scipy
   sudo dnf install python3-matplotlib

- 当然 pip コマンドからライブラリの導入も可能ですが、 Scipy, Matplotlib ともに C ライブラリ依存な部分もあり、この処理に関しては pip からは行ってくれません。
    - そのため、面倒くさい場合 dnf のようなシェル側のコマンドを利用した方が楽な場合もあります。

プログラミング基礎
---------------------

本当に基礎的な内容（for 文とか if 文とか）は省略します。
基本的なプログラミングはできることを前提にラムダ式とか包括表記とかに関して書きます。

ラムダ式
-----------------

ラムダ式とは基本的には関数 (def) と同様の機能を持つ式です。
def が文（つまり複数の式）であるのに対し ラムダ式は単一の式になります。

例えば以下の二つは同一の意味を持ちます。

.. code-block:: python

   # 関数 (def)
   def add(x, y):
       return x + y

   print(add(1, 2))

   # ラムダ式
   f = lambda x, y: x + y
   print(f(1, 2))

これは map 関数と組み合わせることで
定義した式をリスト等のイテレーター（for 文で回すことができるものだと思ってください）に適応することができます。

.. code-block:: python

   f = lambda x: 2 * x
   itr = [1, 5, 2, 3, 20, 2]
   map(f, itr)

内包的記法
-----------------

python のもう一つの便利な記法に内包表記というものがあります。
これは簡単に言えば リスト 内で for 文が書けるというものになります。


.. code-block:: python

   [x * 2 for x in [1, 2, 3, 4, 5]]

if 文も使用することができます。

.. code-block:: python

   [x for x in [1, 2, 3, 4, 5] if x > 3]

信号処理関連
------------------

信号処理を行うに当たって必要な情報を記述します。

複素数
~~~~~~~~~~~~~~

python ではデフォルトで複素数を使用できます。
虚数を使用するには係数に j をつけます。

.. code-block:: python

   from math import e, pi
   e ** (pi * 1j)

.. note:: "**" に関して

   python において累乗は ** で表記します。

また、平方根を取りたい場合には以下のように記述します。

.. code-block:: python

   from cmath import sqrt
   sqrt(-5)


