===============================================
Think Bayes : プログラマのためのベイズ統計入門
===============================================

| Last Change: 11-Jun-2015.
| author : qh73xe

- 筆者 : Allen B.Downey
- 出版社 : O'RELLY
- 出版年 : 2014

この本について
===================

最近手に入れたベイス統計の入門書です．
副題に "プログラマのため" と銘打ってある通り，この本では Bayes 統計に関して
数式ではなく，プログラミングコードを使って説明をしています．

私個人は，このようにアルゴリズムを説明されるととてもわかり易いと思いました．
このページは本書に関しての私なりのメモ及び，備忘録です．

このページは，上記の書籍の内容を説明していたりするので，
基本的に内部公開資料とさせてもらいます．

.. toctree::
   :maxdepth: 1

   cap1
   cap2
   cap3
   cap4
   cap5
   cap6
   cap7
   cap8
   cap9
   cap10
   cap11
   cap12
   cap13
   cap14
   cap15
