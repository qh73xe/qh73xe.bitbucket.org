#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "wave.h"

int main(void)
{
  /* ---- 変数宣言 ---- */
  MONO_PCM pcm1, pcm2, pcm3;
  int n;
  double A, f0;

  /* ---- 可視化用 ---- */
  double x, y, min, max;
  int str, end;
  FILE *gp;
  str = 2000;
  end = str + 128;

  /* ---- 定数項 ---- */
  pcm1.fs = 8000;
  pcm1.bits = 16;
  pcm1.length = 8000;
  pcm1.s = calloc(pcm1.length, sizeof(double)); // メモリ確保

  pcm2.fs = 8000;
  pcm2.bits = 16;
  pcm2.length = 8000;
  pcm2.s = calloc(pcm2.length, sizeof(double)); // メモリ確保

  pcm3.fs = 8000;
  pcm3.bits = 16;
  pcm3.length = 8000;
  pcm3.s = calloc(pcm2.length, sizeof(double)); // メモリ確保

  /* ---- サイン波の作成 ---- */
  A = 0.25;
  for (n = 0; n < pcm1.length; n++){
      pcm1.s[n] = A * sin(2.0 * M_PI * 250.0 * n / pcm1.fs);
  }

  for (n = 0; n < pcm2.length; n++){
      pcm2.s[n] = A * sin(2.0 * M_PI * 500.0 * n / pcm2.fs);
  }

  /* ---- 畳み込み ---- */
  for (n = 0; n < pcm3.length; n++){
      pcm3.s[n] = pcm1.s[n] + pcm2.s[n];
  }

  mono_wave_write(&pcm3, "result.wav");

  /* ---- s の最大値と最小値を取得 ---- */

  min = max = pcm1.s[str];
  for (n = 0; n < end; n++){
    if (pcm1.s[n] < min) min = pcm1.s[n];
    if (pcm1.s[n] > max) max = pcm1.s[n];
  }
  for (n = 0; n < end; n++){
    if (pcm2.s[n] < min) min = pcm2.s[n];
    if (pcm2.s[n] > max) max = pcm2.s[n];
  }
  for (n = 0; n < end; n++){
    if (pcm3.s[n] < min) min = pcm3.s[n];
    if (pcm3.s[n] > max) max = pcm3.s[n];
  }



  /* ---- グラフ作成 ---- */
  gp = popen("gnuplot -persist","w");
  fprintf(gp, "set multiplot\n");
  fprintf(gp, "set xrange [0:%d]\n", 128);
  fprintf(gp, "set yrange [%f:%f]\n", min, max);

  fprintf(gp, "plot '-' with lines lt 2 lc rgb \"red\" lw 1\n");
  for (n = str; n < end; n++){
    x = n - str;
    y = pcm1.s[n];
    fprintf(gp,"%f\t%f\n", x, y);
  }
  fprintf(gp, "e\n");

  fprintf(gp, "plot '-' with lines lt 2 lc rgb \"salmon\" lw 1\n");
  for (n = str; n < end; n++){
    x = n - str;
    y = pcm2.s[n];
    fprintf(gp,"%f\t%f\n", x, y);
  }
  fprintf(gp,"e\n");

  fprintf(gp, "plot '-' with lines lt 2 lc rgb \"green\" lw 10\n");
  for (n = str; n < end; n++){
    x = n - str;
    y = pcm3.s[n];
    fprintf(gp,"%f\t%f\n", x, y);
  }
  fprintf(gp,"e\n");
  fprintf(gp, "set nomultiplot\n");
  fprintf(gp, "exit\n");
  fflush(gp);
  pclose(gp);

  free(pcm1.s);
  free(pcm2.s);
  return 0;
}
