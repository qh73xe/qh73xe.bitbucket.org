#include <stdio.h>
#include <stdlib.h>
#include "wave.h"

int main(void)
{
  /* ---- 変数宣言 ---- */
  MONO_PCM pcm0;
  int n, i;
  double x, y;
  double min, max;
  FILE *gp;

  /* ---- wav の読み込み ---- */
  mono_wave_read(&pcm0, "../guitar_A4.wav");
  printf(
    "HEAD\nfs: %d\nbits: %d\nlength: %d\n",
    pcm0.fs, pcm0.bits, pcm0.length
  );
  /* ---- pcm0.s の最大値と最小値を取得 ---- */
  min = max = pcm0.s[0];
  for (i = 0; i < pcm0.length; i++){
    if (pcm0.s[i] < min) min = pcm0.s[i];
    if (pcm0.s[i] > max) max = pcm0.s[i];
  }

  /* ---- グラフ作成 ---- */
  gp = popen("gnuplot -persist","w");
  fprintf(gp, "set xrange [0:%d]\n", pcm0.length);
  fprintf(gp, "set yrange [%f:%f]\n", min, max);
  fprintf(gp, "plot '-' with lines linetype 1 title \"wave\"\n");
  for (n = 0; n < pcm0.length; n++){
    x = n;
    y = pcm0.s[n];
    fprintf(gp,"%f\t%f\n", x, y);
  }
  fprintf(gp,"e\n");
  pclose(gp);
  free(pcm0.s);
  return 0;
}
