#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "wave.h"

int main(void)
{
  /* ---- 変数宣言 ---- */
  MONO_PCM pcm1;
  int n;
  double A, f0;

  /* ---- 可視化用 ---- */
  double x, y, min, max;
  int str, end;
  FILE *gp;
  str = 2000;
  end = str + 128;

  /* ---- 定数項 ---- */
  pcm1.fs = 8000;
  pcm1.bits = 16;
  pcm1.length = 8000;
  pcm1.s = calloc(pcm1.length, sizeof(double)); // メモリ確保
  scanf("%lf", &A);
  scanf("%lf", &f0);
  printf("A: %f\n", A);
  printf("f0: %f\n", f0);

  /* ---- サイン波の作成 ---- */
  for (n = 0; n < pcm1.length; n++){
      pcm1.s[n] = A * sin(2.0 * M_PI * f0 * n / pcm1.fs);
  }
  mono_wave_write(&pcm1, "result.wav");

  /* ---- s の最大値と最小値を取得 ---- */
  min = max = pcm1.s[str];
  for (n = 0; n < end; n++){
    if (pcm1.s[n] < min) min = pcm1.s[n];
    if (pcm1.s[n] > max) max = pcm1.s[n];
  }

  /* ---- グラフ作成 ---- */
  gp = popen("gnuplot -persist","w");
  fprintf(gp, "set xrange [0:%d]\n", 128);
  fprintf(gp, "set yrange [%f:%f]\n", min, max);
  fprintf(gp, "plot '-' with lines linetype 1 title \"sin\"\n");
  for (n = str; n < end; n++){
    x = n - str;
    y = pcm1.s[n];
    fprintf(gp,"%f\t%f\n", x, y);
  }
  fprintf(gp,"e\n");
  pclose(gp);
  free(pcm1.s);
  return 0;
}
