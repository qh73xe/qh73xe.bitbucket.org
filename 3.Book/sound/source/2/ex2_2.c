#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "wave.h"

/* ---- のこぎり波作成関数 ---- */
MONO_PCM mk_saw_tooth(int i_max){
  MONO_PCM pcm;
  int n, i;
  double A, f0;

  pcm.fs = 8000;
  pcm.bits = 16;
  pcm.length = 8000;
  pcm.s = calloc(pcm.length, sizeof(double));
  A = 0.25;
  f0 = 250.0;
  for (n = 0; n < pcm.length; n++)
  {
    for (i = 1; i <= i_max; i++)
    {
      pcm.s[n] += A / i * sin(2.0 * M_PI * f0 * i * n / pcm.fs);
    }
  }
  return pcm;
}

/* ---- グラフ作成関数 ---- */
int mkplot(MONO_PCM pcm){
  double x, y, min, max;
  FILE *gp;
  int str, end, n;

  str = 2000;
  end = str + 128;

  min = max = pcm.s[str];
  for (n = 0; n < end; n++){
    if (pcm.s[n] < min) min = pcm.s[n];
    if (pcm.s[n] > max) max = pcm.s[n];
  }

  gp = popen("gnuplot -persist","w");
  fprintf(gp, "set xrange [0:%d]\n", 128);
  fprintf(gp, "set yrange [%f:%f]\n", min, max);
  fprintf(gp, "plot '-' with lines lt 2 lc rgb \"green\" lw 10\n");
  for (n = str; n < end; n++){
    x = n - str;
    y = pcm.s[n];
    fprintf(gp,"%f\t%f\n", x, y);
  }
  fprintf(gp,"e\n");
  fprintf(gp, "exit\n");
  fflush(gp);
  pclose(gp);
  return 0;
}

int main(void)
{
  MONO_PCM pcm2, pcm3, pcm15;
  pcm2 = mk_saw_tooth(2);
  pcm3 = mk_saw_tooth(3);
  pcm15 = mk_saw_tooth(15);
  mkplot(pcm2);
  mkplot(pcm3);
  mkplot(pcm15);

  mono_wave_write(&pcm2, "result.wav");
  mono_wave_write(&pcm15, "result1.wav");
  free(pcm2.s);
  free(pcm3.s);
  free(pcm15.s);
  return 0;
}
