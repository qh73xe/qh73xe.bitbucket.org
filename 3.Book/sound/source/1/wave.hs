data MONO_PCM = MONO_PCM{
    fs :: Int,
    bits :: Int,
    length :: Int
} deriving (Show)

