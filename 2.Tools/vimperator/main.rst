================================
vimperator
================================

| Last Change: 20-Oct-2015.
| author : qh73xe
|

このページでは firefox プラグイン vimperator についてメモしていきます．
このツールは 要は firefox を vim のように操作するプラグインで， とても素晴らしいです．

外部エディタに cui vim を使用する
=========================================

vimperator では外部エディタを設定できます．
これはデフォルトで <C-i> で起動し，text 領域に指定のエディタで書き込むことができます．

この設定をするには ~/.vimperatorrc に以下の記述をします．

.. code-block:: vim
   :caption: .vimperatorrc
   :name: setEditorSample

   set editor="お好きなエディタ"

で，まぁ私の場合 vim 一択なのですよね．
しかも cui のやつがいいです．

一昔前までは， 一度 gnome-terminal を起動してのようなことができましたが，
最近できなくなったので uxterm を利用します．

.. code-block:: vim
   :caption: .vimperatorrc
   :name: setEditorforvim

   set editor="uxterm -xrm '*VT100*forcePackedFont: false' -fa 'Ricty Discord' -fs 11 -fg white -bg black +sb -e vim "

.. note:: uxterm が入っていない場合別途インストールしてください．
