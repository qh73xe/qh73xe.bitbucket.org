================================
zsh : 設定ファイル
================================

| Last Change: 30-Nov-2014.
| author : qh73xe

ここでは :command:`zsh` を使用していく際に記述していくことになる 設定ファイルに関する情報をまとめます.

.. contents :: 目次
   :depth: 3

設定ファイルの種類に関して
==========================

:command:`zsh` の設定ファイルの命名は以下のようになります.

.. list-table:: zsh 設定ファイル
   :widths: 30 30

   * - :file:`~/.zshebv`
     - 環境変数, 検索パス
   * - :file:`~/.zprofile`
     - ログイン時に自動実行するスクリプトの設定
   * - :file:`~/.zshrc`
     - 設定ファイル本体
   * - :file:`zlogin`
     - ログイン時に自動実行するスクリプトの設定 |


- この順番で設定ファイルは読み込まれます. 
    - 基本的には :file:`.zshrc` に記述していく感じですね

補完関係
========

:command:`zsh` の最大の強みはコマンドの補完機能にあるかと思います．
とりあえず，最低限の補完機能を有効にするには以下の設定を記述します．

::

   autoload -U compinit && compinit # 補完の有効化
   zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' '+m:{A-Z}={a-z}' # 大文字と小文字の区別なし
   zstyle ':completion:*:processes' menu yes select=2                      # kill コマンドのようなものに関しては候補をタブで選択できるようにする

標準の補完機能を強化する
============================

標準の補完機能でも十分に強力ですが，私は :command:`auto-fu.zsh` というプラグイン
を導入しています．これを導入するとオプション周りが便利になります．

設定方法
----------

GitHub から導入します．

.. code-block :: bash

    $ git clone https://github.com/hchbaw/auto-fu.zsh.git

:file:`.zshrc` に以下の設定を追記します．

.. code-block :: bash

   if [ -f ~/Documents/ect/zsh/auto-fu.zsh ]; then
     source ~/Documents/ect/zsh/auto-fu.zsh
     function zle-line-init () {
       auto-fu-init
     }
     zle -N zle-line-init
     zstyle ':completion:*' completer _oldlist _complete
   fi

- 参考: http://blog.glidenote.com/blog/2012/04/07/auto-fu.zsh/

補完候補をハイライトする
------------------------

以下の設定を :file:`.zshrc` に記述します．

::

    autoload -U compinit
    compinit
    zstyle ':completion:*:default' menu select=2

コマンド履歴から補完を行う
===================================

以下の設定を行うと :command:`Ctrl-P` を使用することでコマンド履歴からの補完を行
うことが可能になります．

例えば，:command:`ls` のように入力した状態で :command:`Ctrl-P` を押すと過去に使
用した :command:`ls` で始まるコマンドを順次表示します．

- 例えば vim で編集しているスクリプトを実行するときに便利

.. code-block :: bash

   # 履歴から補完を行う
   autoload history-search-end
   zle -N history-beginning-search-backward-end history-search-end
   zle -N history-beginning-search-forward-end history-search-end
   bindkey "^P" history-beginning-search-backward-end
   bindkey "^N" history-beginning-search-forward-end

- 参考: http://news.mynavi.jp/column/zsh/004/

git-flowコマンドの補完
----------------------

::

   mkdir ~/zsh/ && cd ~/zsh/ && git clone https://github.com/bobthecow/git-flow-completion.git && echo "source ~/zsh/.git-flow-completion.zsh" >> ~/.zshrc && exec $SHELL

ヒストリ関連
===============

基本設定
----------

::

   export HISTFILE=${HOME}/.zsh_history # 履歴ファイルの保存先
   export HISTSIZE=1000                 # メモリに保存される履歴の件数
   export SAVEHIST=100000               # 履歴ファイルに保存される履歴の件数
   setopt hist_ignore_dups              # 重複を記録しない
   setopt EXTENDED_HISTORY              # 開始と終了を記録

おまけ
==============

zsh を可愛くする
----------------------

とりあえず以下の設定をしてみれば良いかと思います．

.. code-block :: bash

    # 色設定
    autoload -U colors; colors
    # もしかして機能
    setopt correct
    # PCRE 互換の正規表現を使う
    set opt re_match_pcre
    # プロンプトが表示されるたびにプロンプト文字列を評価、置換する
    setopt prompt_subst
    # プロンプト指定
    PROMPT="
    [%n] %{${fg[yellow]}%}%~%{${reset_color}%}
    %(?.%{$fg[green]%}.%{$fg[blue]%})%(?!(*'-') <!(*;-;%)? <)%{${reset_color}%} "
    # プロンプト指定(コマンドの続き)
    PROMPT2='[%n]> '
    # もしかして時のプロンプト指定
    SPROMPT="%{$fg[red]%}%{$suggest%}(*'~'%)? < もしかして %B%r%b %{$fg[red]%}かな? [そう!(y), 違う!(n),a,e]:${reset_color} "

ちなみに顔文字部分を"はちゅねミク"にすることもできます．::

    PROMPT="[%n] %{${fg[yellow]}%}%~%{${reset_color}%}%(?.%{$fg[green]%}.%{$fg[blue]%})%(?!(＠ﾟ□ﾟ)ノ <!ZzZz(＠￣￢￣%)ノ <)%{${reset_color}%}
    "PROMPT2='[%n]> '
    SPROMPT="%{$fg[red]%}%{$suggest%}(＠ﾟ△ﾟ%)ノ < もしかして %B%r%b %{$fg[red]%}かな? [そう!(y), 違う!(n),a,e]:${reset_color}"

ようは AA にすれば良いじゃないかと

