========================================
zsh : イントロダクション
========================================

このページには zsh の導入及び基本的な使用方法についてメモしていきます.

導入
======================

とりあえず :command:`zsh` をインストールしていきます．
これは :command:`zypper` , :command:`yum` , :command:`apt-get` , 等々各自の環境
に合わせたコマンドを使用すればOKです．

.. code-block :: bash

   $ sudo zypper in zsh

普段使いの shell を :command:`zsh` に変更するには以下のようにします．

.. code-block :: bash

    $ which zsh
    $ chsh -s /usr/local/bin/zsh

- 一行目は :command:`zsh` が何処に入ったのかを確認しています
    - その結果を踏まえて二行目のパスを入力してください

