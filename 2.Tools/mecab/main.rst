================================
Mecab : 形態素解析ツール
================================

| Last Change: 10-Dec-2014.
| author : qh73xe

導入方法
====================

ubuntu, debian
--------------------

.. code-block:: bash

   $ sudo apt-get -y install mecab libmecab-dev mecab-ipadic-utf8 mecab-jumandic-utf8

OpenSUSE
---------------

opensuse の場合，ソースからビルドするのが楽ですね．

まずは以下の手順で mecab を入手

* `公式サイト <https://code.google.com/p/mecab/downloads/detail?name=mecab-0.996.tar.gz&can=2&q=>`_ から ソースファイルを入手
    - 適宜最新版を導入します
* :command:`./configure && make`
* :command:`sudo make install`
    - :file:`libmecab.so.2` 周りでエラーが起きる場合, :command:`ldconfig`

続いて辞書を取得．
辞書には一般によく使用される ipadic と unidic を入れます．

- `ipadic <http://sourceforge.net/projects/mecab/?source=typ_redirect>`_
- `unidic <http://sourceforge.jp/projects/unidic/>`_

+ 上記のサイトからそれぞれのソースファイルを取得する
    + unidic の場合，:file:`unidic-mecab-2.1.2_src.zip` が楽だと思います．
+ :command:`./configure && make`
+ :command:`sudo make install`
    + 私の環境では :file:`/usr/local/lib64/mecab/` に辞書が作成されました．
    + mecab 本体は :file:`/usr/local/lib/mecab/` なのでここに上の :file:`dic` を移動します

- デフォルトでは ipadic が使用されます．

.. note:: mecab の辞書に関して

    mecab 公式では ipadic を使えと書いてありますが，
    個人的には unidic の方が好きです．

    日本語の場合，一口に単語と言っても何処で区切るのかは厳密には決まっていません.
    例えば，"日本国民" は一つの単語なのか，日本と国民なのか，日本という国の民なのかどれがいいでしょうか？

    unidic では短単位という単位をシステマチックに認定し，上記のような問題に対して画一的な処理を施せます．

使用方法
=========================

.. code-block:: bash

   $ echo 総務部長と万年課長は蕎麦屋とうどん屋をはしごした | mecab


.. code-block:: bash

   総務    名詞,一般,*,*,*,*,総務,ソウム,ソーム
   部長    名詞,一般,*,*,*,*,部長,ブチョウ,ブチョー
   と      助詞,並立助詞,*,*,*,*,と,ト,ト
   万      名詞,数,*,*,*,*,万,マン,マン
   年      名詞,接尾,助数詞,*,*,*,年,ネン,ネン
   課長    名詞,一般,*,*,*,*,課長,カチョウ,カチョー
   は      助詞,係助詞,*,*,*,*,は,ハ,ワ
   蕎麦    名詞,一般,*,*,*,*,蕎麦,ソバ,ソバ
   屋      名詞,接尾,一般,*,*,*,屋,ヤ,ヤ
   と      助詞,並立助詞,*,*,*,*,と,ト,ト
   うどん  名詞,一般,*,*,*,*,うどん,ウドン,ウドン
   屋      名詞,接尾,一般,*,*,*,屋,ヤ,ヤ
   を      助詞,格助詞,一般,*,*,*,を,ヲ,ヲ
   はしご  名詞,一般,*,*,*,*,はしご,ハシゴ,ハシゴ
   し      動詞,自立,*,*,サ変・スル,連用形,する,シ,シ
   た      助動詞,*,*,*,特殊・タ,基本形,た,タ,タ
   EOS

形態素辞書: UniDic 
====================

| UniDicは日本語テキストに形態論情報を付与するための電子化辞書です。    
| UniDicは国立国語研究所のコーパスの構築に利用されています。    
| 形態素解析辞書としてのUniDic（unidic-mecab）は形態素解析器MeCabの辞書として利用できます。

- 公式サイト: http://www.ninjal.ac.jp/corpus_center/unidic/

unidic を使用するためには， :file:`/usr/local/etc/mecabrc` を変更する必要があります．

:file:`/usr/local/etc/mecabrc`::

    dicdir = /usr/local/lib/mecab/dic/unidic

mecab-python
=================

mecab を python から利用するためのライブラリはありますが，これは :command:`pip`
からでは導入できません（やめて欲しい...）

+ `公式サイト`_ からソースファイルをダウンロードします．
+ 解凍したディレクトリに移動します．
+ python setup.py build
+ sudo python setup.py install
    + sudo を使用すると :command:`mecab-config` が見つからないとか言われるかもしれません
    + :file:`setup.py` を編集し， mecab-config を絶対パスに変更します．
        + 私の場合，:file:`/usr/local/bin/mecab-config`

茶豆
===================

windows を使用している場合，
構文解析を GUI で行うツールとして :command:`茶豆` というものがあります．

.. note:: OS に関して

    筆者はこのツールに関しては windows でしか試したことがありません．

導入
------------------

:command:`茶豆` は以下のページから UniDic を導入すると自動で入ってきます．

- http://sourceforge.jp/projects/unidic/
