================================
praat: Tips
================================

| Last Change: 04-Sep-2015.
| author : qh73xe


Linux 版 Praat を使っていて，落ちる問題について
==================================================

Linux 版の Praat を使用していると テキストグリッドエディタを使用している際に，
音声を再生しようとすると落ちる場合があります．
これは gnome での音声再生システムとの相性の問題のようですが（そのため，OSのシス
テムが古い場合とgnome以外のデスクトップシステムを採用している場合，問題がおきま
せん），公式サイトに解決策が記述されるようになったようです．

- https://help.ubuntu.com/community/SoundTroubleshootingProcedure
    * なおこの問題は gnome 以外でも ubuntu の unity を使用していても起きるようです．

シンプルな解決策として KDE4 など別のデスクトップシステムを採用するという手もあります(実際に私はそのように対処しました)． ただし，KDE4 の場合，テキストグリッドエディタ上で tab キーによる音声再生はできないようです（何かキーバインドの問題でしょうか）．

- 一昔前の Praat では gnome を使用していてもタブキーによる音声再生ができなかったのでこの時と同様の対策が必要なのかもしれません．

Linux 版の Praat で音がならない場合の対処法
==============================================

Linux 版の Praat を使用していて，音がならない場合というのは結構あるのですが
とりあえず解決する場合のある方法をメモします．

これは PC によって設計が変わる部分なので，そういうエラー（他のPCでは動くのにの場合）には
役に立つかもしれません．
おそらく Praat は Linux の音声関連のデフォルト設定を直接反映はしていない場合があります．
この場合現象として，他の音声再生ソフトでは音声が再生されるのに Praat のみ音声再生がなされません．

デフォルトのサウンドカード設定を変更してやるとうまく音がなるようになります．

解決方法
~~~~~~~~~~~~~

まず，コマンドライン上で， aplay -l とします．
そうすると，現在使用しているPCのサウンドカードとデバイス番号が出てきます．
あとはそれぞれのサウンドカードの番号を利用して以下のコマンドを使用します．

.. code-block:: bash

   aplay -D plughw:0,3 /usr/share/sounds/alsa/Front_Center.wav

- ここでは カード番号０，デバイス番号３の設定で音を再生します．

このコマンドでうまく音声が再生されるカード番号とデバイス番号を確認してください．
その上で， /etc/asound.conf を編集します．

.. code-block:: bash
   :caption: /etc/asound.conf
   :name: asound.conf

   pcm.!default {
       type plug slave.pcm {
           type hw card 0 device 3
        }
    }

その上で Praat を再起動して音声を再生してみてください．

do("command...", x) と command: x の違いについて
========================================================================

少し前の praat では 例えば, :command:`read from file...` を Praat 本体から実行し,
Praat のスクリプトエディタ上で, ヒストリーをペーストすると以下のように書かれていました.

.. code-block:: bash 

   do("Read from file...", fileName)

しかし,最近の Praat [#f1]_ で同様の作業を行うと以下のように変わります.

.. code-block:: bash 

   Read from file: fileName

この2つの記法は実は最新版の Praat ではどちらも使用できます.
この2つの記法の差は前者は,完全に関数として機能するのに対し,後者はコマンドになります.
具体的には後者の書き方では他の関数に直接書き込むことができません.

.. code-block:: bash 

   Read from file: fileName




.. rubric:: 脚注

.. [#f1] 私が使用している Praat は 5.3.81 です

オブジェクトの複数選択
===========================

Praat スクリプトを使用している際にオブジェクトを複数個指定したい場合があります.
このような場合, selectObject 関数に オブジェクトID を複数個指定することで選択が可能になります.

.. code-block:: bash

   Create TextGrid: 0, 1, "Mary John bell", "bell"
   Create TextGrid: 0, 1, "Mary John bell", "bell"
   Create TextGrid: 0, 1, "Mary John bell", "bell"
   Create TextGrid: 0, 1, "Mary John bell", "bell"
   
   selectObject(1,2,4)
   removeObject(1,2,4)

- これは editor コマンドにも対応しています.

.. code-block:: bash

   id = do("Read from file...", "hoge.TextGrid")
   View & Edit
   
   editor:id
   Select: 5.954630385487512, 10.122630385487513
   text$ = Get label of interval
   endeditor

   print 'text$'

praat コマンドの引数指定について
======================================

Mac や linux で praat を使用する場合, シェル上で praat スクリプトを実行することができます.

.. code-block:: bash

   w$ = "hoge"
   appendInfoLine(w$)

上記のような praat スクリプトを 例えば temp.praat という名前で保存し,以下のように実行することができます.

.. code-block:: bash

   $ praat temp.praat
   > hoge

さて,このような praat スクリプトを実行する際に シェル上で引数を指定したい場合があります.
このような場合, 引数の指定には form , endform 構文を使用します.

.. code-block:: bash

   form text
      text w hoge
   endform
   appendInfoLine(w$)

.. code-block:: bash

   $ praat temp.praat hoge
   > hoge
   $ praat temp.praat piyo
   > piyo

ちなみに form を増やすことで使用できる引数は増えますが, これは上から第一,第二引数というようになるようです.

あるディレクトリのファイルを一括で扱う
==========================================

praat script では :command:`Create Strings as file list: "objName", "path/to/file"` と
いうコマンドが存在します.
このコマンドは :file:`path/to/filename` にあるファイル名を文字列のリストとして取得します．
また，ファイルの指定には正規表現を利用することができます．

そのため，例えばあるディレクトリにある :file:`.wav` ファイルを取得するには以下の
ようにします．

.. code-block :: bash

   Create Strings as file list: "fileList", "/path/to/*.wav"

.. warning::

   ファイルパスを '~/' のような形で書くとエラーになりました．

こうすると新規に fileList というオブジェクトが生成されます．
このオブジェクトの行数を取得するには， :command:`Get number of strings` コマンドを使用します．

この２つを組み合わせると任意のディレクトリにある任意の拡張子のファイルすべてに対
して一定の処理を行うことが簡単になります．
例として， :file:`~/wav` にあるすべての :command:`wav` データを読み込むスクリプ
トを作成してみます．

.. code-block :: bash

   wav_directory$ = "/path/to/dir"
   Create Strings as file list: "fileList", "'wav_directory'/*.WAV"
   number_files = Get number of strings
   for i from 1 to number_files
       selectObject ("Strings fileList")
       current_token$ = Get string... 'i'
       Read from file: "'wav_directory$'/'current_token$'"
       Remove
   endfor

undefined
==============

ピッチの抽出を行うスクリプトを記述している際には，
検出に失敗をして値が取得できないことがあります．

例えば，H1-H2 のように，ピッチの値を前提とする特徴量を算出するスクリプトを書く場
合，そもそも，ピッチの抽出に失敗をした場合の例外処理を書く必要があります．
これは例えば以下のようなスクリプトで作成します．

.. code-block :: bash

   selectObject: "Pitch hallo"
   meanPitch = Get mean: 0.1 , 0.2, "Hertz", "Parabolic"
   if meanPitch = undefined
       # Take some exceptional action.
   else
       # Take the normal action.
   endif


Praat script 上で Home を取得する
=====================================

Praat Script を書いている際，複数PCでの使用を前提に
Home ディレクトリを抽象化したい場合があります．
このような場合以下のコマンドが便利です．

.. code-block :: bash

   homeDirectory$ = environment$("HOME")
   appendInfoLine: homeDirectory$

Text ファイルと音声ファイルを一緒に見たい
===========================================

例えば，音声収録時に生理的な計測を一緒に行ったとして，
それが，何らかの時間的かつ波形的なデータであった場合，その生理的な計測結果と音声波形を同期して
観察したい場合には以下のような形式のデータ (AmplitudeTier) に変換すると Praat で読み込んでくれます．

.. code-block:: bash
   :caption: AmplitudeTier
   :name: AmplitudeTier

   File type = "ooTextFile"
   Object class = "AmplitudeTier"

   xmin = 0 
   xmax = 669.9 
   points: size = 663000 
   points [1]:
       number = 0.0 
       value = 0 
   points [2]:
       number = 0.001 
       value = 0 

五行目(スペース含み)までは header です．
とりあえず，変更する必要がある要素は以下の3つです．

- xmin = 収録開始時間 
- xmax = 収録終了時間 
- points: size = サンプルにとったデータ数 

ここで，これらの要素の最後には何故か，半角スペースが一個入っていることに注意しましょう．

points [1]: からが本体部分です．
これは3つの要素で成り立っています．

- points [i]:  i にデータの通し番号
- number = あるデータの計測時間（時間なのに number ...）
- value = 計測値

これに従いデータを変換すれば praat で読み込んで
wav ファイルと同期を取りながら View & Edit できます．

わーい


.. warning:: この Tips について

    この Tips は Praat の正当な使い方では決してない（と思う）Tips です．
    思いついちゃったからやってみたらできてしまった程度に思ってください．

    - http://www.fon.hum.uva.nl/praat/manual/AmplitudeTier.html
        - 本来は時間と音圧を表示するための Tier で，単位はパスカルです
        - パスカルは負の値をとるんで行けるかな−と
            - そういう意味では圧力関係(例えば，被験者に何かを握りながら発話してもらうときとか，肺の空気圧を図るときとかは正当性あるかも)
