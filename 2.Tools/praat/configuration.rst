########
環境設定
########

ここには Praat を使用するにあたっての環境設定を記述していきます.
Praat 本体は Linux, Mac, Windows といった大体のOSで起動できます.

- ただし, 端末から Praat を操作したい場合, Window はやや特殊です [#f1]_ .


Praat の インストール
=====================

Praat 本体は公式ページから直接実行ファイルをダウンロードするのが手っ取り早いかと思います.
Linux を使用している場合は, とりあえずパスの通っているディレクトリに保存します.

- `公式サイト <http://www.fon.hum.uva.nl/praat/>`_

なお, debian 系の Linux に関してもやや特殊で, apt-get から Praat を導入することもできますが, 最近版ではありません(かなり古いものになり,このサイトで書いてあるPraatScriptの一部が使用できなくなります).

また, 最近版の Praat を Debian 7 で実行するには依存関係で面倒くさいです.

Praat の設定
==================

praat の設定は object ウインドウ（praatを起動した際に出てくるやつ）から行います．
日本語を前提とした書き起こしを行う場合には praat の使用を開始する前に，ファイル
の読み書きに使用する文字コードをUTF-8に統一しておくのが無難かと思います．

これは オブジェクトウインドウ -> praat -> Preferences -> Text reading Preferences / Text writing preferences -> UTF-8 を指定します．

.. figure:: fig/config/praat1.png

また praat で日本語の漢字を使用したい場合（デフォルトでは中国語漢字が使用されます），
オブジェクトウインドウ -> praat -> Preferences -> CJK font style preferences を
選択し，Japanese を選択します．

SendPraat の インストール
=========================

SendPraat とは, 端末から Praat を操作するための実行ファイルです.
これに関しても公式サイトからインストールを行います.

- `sendpraat <http://www.fon.hum.uva.nl/praat/sendpraat.html>`_

- なお,ダウンロードされたファイルはパスの通っているディレクトリに保存します.
- また,ファイル名を "sendpraat" に変更する必要があります.

praat コマンド
====================

linux, Mac の場合 Praat のみでも端末から Praat の操作が可能です.
例えば以下のようなスクリプト (playSine.praat) を記述します.

.. code-block:: bash

    form Play a sine wave
    positive Sine_frequency_(Hz) 377
    positive Gain_(0..1) 0.3 (= not too loud)
    endform
    Create Sound as pure tone: "sine" + string$ (sine_frequency), 1, 0, 1, 44100, sine_frequency, gain, 0.01, 0.01
    Play
    Remove

この Praat ファイルを実行するには,端末上で以下のように入力します.

.. code-block:: bash

   $ praat playSine.praat 550 0.9


Praat コマンド と SendPraat の違いについて
============================================

両者は結局のところ Praat スクリプト をコマンド上から実行します.
ただし, 根本的な差として praat コマンドは praat スクリプトを実行しているのに対し,
SendPraat は Praat 自体を操作しています.

そのため, SendPraat を使用する場合, Praat を前もって開いて置く必要があります.
その代わり, スクリプトを実行し, そのまま, Praat 本体での作業が可能になります.

一方, praat コマンドは実行されるたびに Praat を立ち上げ, スクリプトが終了した際に Praat 本体も停止させるようなものです.

そのため, Praat の機能を使用したファイル操作のみを行う場合には Praat コマンドを, 
そのまま, Praat を使用し続けたいような場合には sendpraat を使用します.

.. rubric:: 脚注

.. [#f1] windos で Praat を導入した場合, Praatのみでは端末操作ができません.
         praatcon というアプリケーションも導入する必要があります.
         また, このサイト上で端末から praat コマンドを使用している場合, windows では praatcon コマンドに変更する必要があります.



