================================
praat : jitter
================================

| Last Change: 15-Jan-2016.
| author : qh73xe

Jitter はよく Voice quality の尺度として使われる特徴量の一つです．
この特徴量は声門の開閉の区間であると考えられています．

たとえば，早稲田の 北原 真冬 先生のサイトでは以下のように説明されています．

    Jitterとは声の 高さのゆれ、Shimmerは声の大きさのゆれ、Harmonicityとは振動が 整っ
    ているかどうかを表す。声質(voice quality)は定義するのが難 しい多面的な事象だが、
    Praatのみを用いた初級・中級音声学におい て主に注目するのは、Jitter(ppq5),
    Shimmer(apq5), Harmonics-to-noise raioの3つで十分であろう。この三つの値が大 きい
    ほど、声がザラザラした(Harsh)感じになる。

- http://www.f.waseda.jp/kitahara/Notes/praat.html

上のサイトでは **Harsh** と書いてありますが, 最近では **Creaky Voice** の指標と説明
されることが多い気がします．

- Creaky Voice がどのような音声なのか聞いてみたい方は，以下のサイトに例があります(英語)．
    − https://www.youtube.com/watch?v=Ff1JByylQU0

このサイトでは Praat を使用して音声ファイルから Jitter を計測する方法に関して記
述していきます．

とりあえずやってみる
=========================

何はともあれ，Jitter の検出を行って見ましょう．
使用するのは以下のデータです．

- データは以下のページよりダウンロードしました．
- http://cw.routledge.com/textbooks/9780415498791/chapter11.asp

まずは script のことは考えず手動でやってみます．

手順としては以下の通りです．

+ Praat を起動し，音声ファイルを読み込む
+ Sound Object を選択し， Analyse periodicity -> To PointProcess
    + To PointProcess には複数個の選択肢がありますが，ここでは :command:`To PointProcess (periodic, cc):` を使用しました．
+ Query -> Get jitter
    + Get jitter のはいくつか選択肢があります．
    + とりあえずは :command:`Get jitter (local):` を使用しました．
+ パラメタ設定を行う
+ Jitter が計算される

では実際にやってみます．

