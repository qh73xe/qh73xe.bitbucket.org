###############################################
SendPraat : コマンドラインから praat を操作する
###############################################

コマンドラインを使用して praat を操作したい場合
`sendpraat <http://www.fon.hum.uva.nl/praat/manual/Scripting_8_1__The_sendpraat_subroutine.html>`_ を使用します.

導入方法
================

上記のリンク先から自分の環境にあったファイルをダウンロードします．
この際に，ファイル名を sendpraat に変更しておくと便利です（最低限，Linux の場合は 64 bit , 32 bit で名前が異なるので環境によってコマンドを変えるのが面倒なので）.

このファイルをパスの通っているディレクトリに移動し（私の場合，home 直下に bin というディレクトリを作成し，ここにパスを貼っておくようにしています），実行権限を与えます．

- ダウンロードした直後のファイルには実行権限がありません．
    - そのため上記の処理をしないと機能しないのであしからず．

使用方法
===============

まず， sendpraat は praat を起動してから使用する必要があります．
そのため，sendpraat を使用する際にはまず praatを起動しておく必要があります．

sendpraat の基本的な使用方法は以下の通りです．

Syntax::

    $ sendpraat display, program, long timeOut, text

display
    サブルーチンが実行中の X プログラムから呼び出された場合に表示するポインタ．
    NULLの場合には、sendpraat はそれ自体でディスプレイを開きます.
    Windows と Macintosh では、この引数は無視されます.
program
    Praat Shell で使用している プログラム名．
    例えば "Praat" とか "ALS" とか．
    最初の文字は大文字でも小文字でもよい．
timeOut (Unix and Macintosh only)
    sendpraat がエラー·メッセージを書き込む前に、待つ秒数. 
    0 タイムアウトは、メッセージが、非同期で送信されます.
    その場合， sendpraat にエラー·メッセージを発行せず，すぐに戻ることを意味します．
text
    praat に送る命令．

使用例
===============

hello world::

    $ sendpraat praat 'writeInfoLine: "Hello world"'


Picture editor に 円を描く::

    $ sendpraat praat "for i from 1 to 5" "Draw circle... 0.5 0.5 0.1*i" "endfor"





