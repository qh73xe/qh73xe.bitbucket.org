#####################################
Praat : コーパス作成 & 音響解析ツール
#####################################

このページには コーパス作成, 音響解析ツール Praat に関しての備忘録です.
Praat の基本的な操作に関しては他のページに任せるとして, このページでは基本的には環境設定及び,スクリプト関連の話を書いていきます.

目次
===========================

.. toctree::
   :maxdepth: 1

   configuration
   commandList
   sendpraat
   pichAnalysis
   acousticAnalysis
   tips
   usage
