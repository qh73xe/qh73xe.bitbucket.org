================================
praat : 音響解析
================================

| Last Change: 27-Nov-2014.
| author : qh73xe

このページでは Praat を使用した音響解析に関してメモしていきます．

目次
================================

.. toctree::
   :maxdepth: 1

   pichAnalysis
   formantAnalysis
   intensityAnalysis
   MFCC
   jitta
   Shimmer
   voiceBreaks
   Cochleagram
   loudness
