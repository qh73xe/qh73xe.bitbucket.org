=====================================
pandoc:文章のフォーマットを変換する
=====================================

.. contents::

このページについて
======================

このページでは,色々な形に文章のフォーマットを変換するツールpandocを紹介します.
このツールはHaskellを使用して作成されており,とりあえず「強えー」です.

基本的な使用目的はmarkdownからhtmlの変換ですが,その他,rst,latex,epub,docx等にも変換可能です.

導入方法
======================

haskell製のツールなので,cabal(haskellのpipやcpamみたいなもの)から導入可能です.
また,linuxを使用している場合,yumやapt-getからもインストール可能です.

.. note::

   私の環境ではwinかlinuxを前提としているので試したことはありませんが, Mac ports や Homebrew では
   直接入れるのは面倒そうです.そのため,一旦Haskell環境を導入してそこからインストールが安全そうですね.
   http://d.hatena.ne.jp/sandai/20120604/p1

なお,win,Mac用には本家サイトにてインストーラが配布されています.

- http://johnmacfarlane.net/pandoc/installing.html

基本的な使い方
======================

.. code-block :: bash
   :caption: markdown to html
   :name: markdown to html

   $ pandoc input.md -s -o output.html

Tips
======================

MarkDownからLatexに変換時に見出しをpartにする
----------------------------------------------

一見何でもない話ですが,長い文章を打ち込むときに,各部ごとに(章よりも大きな単位を導入したい)texの方で

.. code-block::tex

   \include{part.tex}

とする場合はしばしばあると思います.そのようなとき,文章自体をMardkDownで記述してtexに変換してから少し,ゴニョゴニョしようと思っている場合は"# → \part{}"に変換してくれたほうが当然うれしいです.
pacdocで章立てのレベルを一段上げるには変換中に以下のオプションを追加します.

- --chapters

具体的には.mdファイルを.texファイルに変換する場合は以下のコマンドになります.

.. code-block:: bash

   $ pandoc part1.md -s -o part1.tex --chapters

.. warning::

   このオプションをつける位置はファイル名の後のようですね.

