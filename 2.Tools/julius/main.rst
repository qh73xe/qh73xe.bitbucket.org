======================================
Julius : 音声認識システム
======================================

このページは音声認識システム Julius の使い方備忘録です.

- 公式サイト : http://julius.sourceforge.jp/index.php?q=newjulius.html

.. contents::
    :depth: 2

Juliusとは
========================

Juliusとは音声認識システムの開発･研究のためのOSS汎用大語彙連続音声認識エンジンです.
私個人の使用の仕方といえば,コーパス作成のための音声認識器としての利用が多いのですが,
実用レベルでの使用はもちろん可能です.
音声認識をとりあえずしてみたい場合,割とメジャーな認識器ではないでしょうか.

導入方法
==========================

Linux (Fedora20でのみ試しています) を使用している場合, Juliusはレポジトリに登録されているようです.

.. code-block:: bash

   $ sudo yum install Julius

簡単ですね.

.. note:: 最新版をビルドする

    julius の更新は割と早いので自分でビルドしていくほうが楽な場合もあります．
    ビルドは以下の手順で行います

    最近 git に移行した模様です.
    インストール環境もかなり整理されているので、
    昔あった、煩雑さは大分減ったかと思います。

    .. code-block:: bash

       # 依存ライブラリの導入
       $ sudo apt-get install build-essential zlib1g-dev flex
       $ sudo apt-get install libasound2-dev libesd0-dev libsndfile1-dev
       # julius の導入
       $ git  clone https://github.com/julius-speech/julius.git
       $ cd julius
       $ ./configure
       $ make
       $ sudo make install

Tips
================================

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   tips/*
