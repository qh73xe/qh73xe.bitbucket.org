================================
shell コマンド
================================

| Last Change: 07-Sep-2015.
| author : qh73xe


目次
================================

.. toctree::
   :maxdepth: 1

なんだかんだで，便利なんだけど，どうにも記事にしにくい shell コマンドに関しては
このページでまとめます．
後に体系だってまとめることができたら，そのように変更していくつもりです．

テキストの行数を調べる
========================

.. code-block:: bash

    $ wc hogehoge.txt
      400  800 2938 hogehoge.txt

上の例の場合 400 が行数，800 が文字数，2938 がファイルサイズです


音声ファイルの基本情報を確認する
=====================================

.. code-block:: bash

    $ soxi home.wav
      Input File     : 'hoge.wav'
      Channels       : 2
      Sample Rate    : 44100
      Precision      : 16-bit
      Duration       : 00:00:01.90 = 84000 samples = 142.857 CDDA sectors
      File Size      : 401k
      Bit Rate       : 1.68M
      Sample Encoding: 16-bit Signed Integer PCM
