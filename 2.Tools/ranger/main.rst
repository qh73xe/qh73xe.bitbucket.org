================================
Ranger: CUI ファイラー
================================

:Last Change: 16-May-2016.
:Author: qh73xe
:Reference: https://github.com/hut/ranger

それでも、ウェブブラウジングと、ファイル操作は GUI メインで行うことが多いです。

とくに ファイル操作に関していうと、確かにシェル上でも充分こなせるのですが、
処理するファイル数が少く、かつ複雑、若しくは全体を考えながらの操作では GUI の方が楽かと思います。

で、このページで取り上げる Ranger ですが、これは CUI で使用できるファイラーです。
どういうことかというと、簡単に言えば、シェル上で GUI の操作性も持ったファイラーが使用可能であるという話です。

導入
===============

恐らく、 Linux を使用している場合、 apt-get や dnf でインストールできるかと思います。

- fedora 23 では可能でした。

.. code-block:: bash

   sudo dnf install ranger
   ranger --copy-config=all

二行目では、設定ファイルを設置しています。

使い方
================

Ranger を起動するには以下のコマンドを実行します。

.. code-block:: bash

   ranger

すると、まるで GUI の様なインターフェースでファイラーが起動します。
なお、基本的な操作は vim を使用していれば大体お分かりになるかと思います。

画像を扱う
=============

ranger のサイトによると， ranger のプレビューで画像を扱うには，
w3mimgdisplay というコマンドが使える必要があります．
そのため，画像のプレビューを行うためには w3m-img を導入する必要があります．

.. code-block:: bash

   sudo dnf install w3m-img

.. warning:: この機能を使用する際には、 ターミナルは xterm などである必要があります。

コマンドを設定する
==========================

個人的に、 ファイラーとして Ranger が便利だと思う点は、
これが python 製 であるという点です。

そのため、独自コマンドを python を利用して記述することが可能です。

コマンド設定は、 :file:`~/.config/ranger/commands.py` で行います。

.. warning:: これはもしかすると ディストリでの設定依存の可能性もあります。

以下に独自コマンドの例を示します。

.. code-block:: python
   :linenos:
   :caption: ~/.config/ranger/commands.py

   class empty(Command):
       """:empty
       Empties the trash directory ~/.Trash
       """
       def execute(self):
           self.fm.run("rm -rf /home/myname/.Trash/{*,.[^.]*}")

注意するべき点としては、
クラス直下のコメントの記述が、そのまま ranger 上の入力になる点でしょうか。

vim と生きる
==========================

Ranger の良いところは拡張が python で記述できる部分でしょう。
もともと vim を前提に作成されている感じもしますので、当然 vim との連携も楽です。

vim から Ranger を呼び出す（vim ファイラの代わりに Ranger を使う感じです）には以下のような設定を記述します。

.. code-block:: vim
   :linenos:
   :caption: .vimrc

   function RangerExplorer()
       exec "silent !ranger --choosefile=/tmp/vim_ranger_current_file " . expand("%:p:h")
       if filereadable('/tmp/vim_ranger_current_file')
           exec 'edit ' . system('cat /tmp/vim_ranger_current_file')
           call system('rm /tmp/vim_ranger_current_file')
       endif
       redraw!
   endfun
   map <Leader>x :call RangerExplorer()<CR>

これで <Leader> + x で Ranger を呼出ます。
