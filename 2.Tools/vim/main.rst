==============================
vim : テキストエディタ
==============================

vim, emacsと並んで有名なテキストエディタですね.
特徴的なキーバインドで有名です.

私の個人的な感想では多分, :command:`emacs` より攻撃的な信者が多いような気もします(いや,名前からくる印象の差かもしれませんが).
大変有名なツールでありますし, 私のような初心者が書かなくてはいけないことはそんなにない
(というか全くない,ここに書いてあることはみんな誰かが書いてますよ) わけですが, だからこそ個人的な備忘録は欲しいわけで.

このページには私なりの :command:`vim` に関する Tips をまとめていきます.

基本設定
====================

以下のページには, プラグインなしで行える操作, 設定について書いていきます.

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./*

プラグイン関係
========================

以下のページには vim プラグイン周りのことを書いていきます.

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./plugin/*

その他メモ
===================================

このページは私の vim 周りの情報をまとめたページの目次として作成しているのですが,
以下, vim の導入に関してのみ記述しておきます.

fedora & opensuse 上で vim をビルドする
-----------------------------------------

コピーボードを使用したい場合などには必要でしょう.
また,普通 linux のディストリから落とせる vim はバージョン等が残念な場合が多いので,なんだかんだで自分でビルドしてしまうことが多いです.

とりあえず以下では fedora や opensuse において if_lua かつ +copybord な vim をビルドします.

- OpenSUSEでは最新版のvimが落ちてくるような.
- SUSEちゃんよいこよいこ.

必要なものを揃える
----------------------------------------------

- ncurses-devel
- lua
- liblua-devel
- python-devel 
- python3-devel
- ruby-deveel

なお,ソースに関してはgitからでも落とせるようです.

- https://github.com/vim-jp/vim

ビルド
-------------------------------------------------------------------

こいつらは基本,ビルドは楽なのでなにか困ったら自分でビルドする.

で./configの設定

.. code-block:: bash
   :linenos:

   $ ./configure \
   --with-x \ 
   --with-gnome \
   --enable-xim \ 
   --with-features=big \ 
   --enable-multibyte
   --enable-multibyte \
   --enable-rubyinterp \
   --enable-pythoninterp \
   --enable-python3interp \
   --enable-luainterp \
   --with-lua-prefix=/usr \
   --enable-perlinterp \
   --enable-tclinterp \
   --enable-mzschemeinterp \
   --enable-gpm \
   --enable-cscope \
   --enable-fontset \

あとは$make,$sudo make installでうまく行くかと.
