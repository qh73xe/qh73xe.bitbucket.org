================================================================
Syntastic: シンタックスチェッカー
================================================================

:Last Change: 22-May-2016.
:Author: qh73xe
:Reference: https://github.com/scrooloose/syntastic

.. contents::

これは何?
=================================

Syntastic プラグインは vim における汎用シンタックスチェッカーです。
勿論、各種プログラミング言語ごとに、シンタックスチェッカーは色々あると思いますが、
このプラグインは網羅的にシンタックスチェックを行えるのが便利な所かと思います。

導入
==============================

基本的には、 :command:`NeoBundle` から導入可能です。
:file:`.vimrc` に以下のように記述します。

.. code-block:: vim
   :linenos:
   :caption: .vimrc
   :name: NeoBundle

   NeoBundle 'scrooloose/syntastic'        " シンタックスチェッカー

基本設定
===============================

取り敢えずシンタックスチェックを行いたい場合、
以下のような設定が良いようです。

- 公式サイトそのまま

.. code-block:: vim
   :linenos:
   :caption: .vimrc
   :name: Base_Syn

   set statusline+=%#warningmsg#
   set statusline+=%{SyntasticStatuslineFlag()}
   set statusline+=%*

   let g:syntastic_always_populate_loc_list = 1
   let g:syntastic_auto_loc_list = 1
   let g:syntastic_check_on_open = 1
   let g:syntastic_check_on_wq = 0 

この設定では、ファイルを開く際と、保存する際に自動でチェックが走ります。

自動チェックを制御する
=============================

特定の言語では、上記の設定が良いけど、
一部の言語では、自動シンタックスチェックはやめて欲しいということも多いかと思います。
このような場合、以下の設定を加ます。

.. code-block:: vim
   :caption: .vimrc
   :linenos:

   let g:syntastic_mode_map = {
       \ 'mode': 'passive',
       \ 'active_filetypes': ['hoge']
       \}

この設定では hoge というファイルタイプのときのみ自動チェックを行い、
それ以外では :command:`:SyntasticCheck` の実行時のみチェックを行います。

シンタックスチェックを無視する
======================================

基本的に自分が書いているコードに関してシンタックスチェックを無視することはありえませんが、
例えば Django 等のテンプレートを利用する場合、
シンタックスエラーというか、警告の山に遭遇することがあります。
このような場合 **# NOQA** のようにコメントを入れるとその箇所のシンタックスチェックを行わないようにします。

各種チェッカーの導入
=============================

ここまで言ってアレなのですが、
基本的にこのプラグインは、ファイルタイプに合わせて特定のチェッカーコマンドを実行し、
その結果を vim につなげてくれるプラグインです。

そのため、ある言語のチェックを行う場合、
必要なチェッカーを導入する必要があります。

対応している言語ごとに必要なチェッカーは以下のページを確認します。

- https://github.com/scrooloose/syntastic/wiki/Syntax-Checkers

一応、個人的に便利だと思っているチェッカーを一覧にしておきます。

.. csv-table:: チェッカー一覧
   :header: 名前, 言語, ドキュメント

   flake8(PEP8 + pyflakes), python, `pep8-ja <http://pep8-ja.readthedocs.io/ja/latest/>`
   Closure Linter, JS, `Google JavaScript Style Guide <http://cou929.nu/data/google_javascript_style_guide/>`
   splint, C, http://www.splint.org/manual/manual.html

チェッカーの選択
======================================

言語によっては特定のチェッカーを利用してたい場合があるかと思います。
このような場合、大体以下のような設定でチェッカーの指定が可能になります。

.. code-block:: vim

   let g:syntastic_hoge_checkers = ['foo']

- hoge は ファイルタイプで、 foo が使用するチェッカー名です。

チェッカーが存在するか否かを調べるには `公式の github <https://github.com/scrooloose/syntastic/tree/master/syntax_checkers>`_ を確認するのが
早いと思います。
