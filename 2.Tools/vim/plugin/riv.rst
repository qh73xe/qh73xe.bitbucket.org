================================================================
Riv.vim : sphinx 編集用プラグイン
================================================================

| Last change: 27-Nov-2015.
| author : qh73xe
|

これは何 ??
==============

Riv.vim は :doc:`sphinx<../../Programmings/python/LIB/sphinx/main>` で使用する rst ファイルの編集を手助けするためのプラグインです.
かなり, 高性能 (ということは,それだけ色々やられているという話でもありますが) なプラグインですので,
文章作成を sphinx で行う, かつ, vimer な人間は導入してみる価値のあるプラグインだと思います.

なかなか, 日本語でこのプラグインに関して網羅的に書いてある情報がなかったため, 適宜メモしていきます.

導入方法
===========

私は vim プラグイン の管理には NeoBundle を使用しています.
これを利用している場合, 以下を記述し :command:`:NeoBundleInstall` を行いましょう.

.. code-block:: vim

   NeoBundle 'Rykka/clickable.vim'       " sphinx の依存プラグイン
   NeoBundle 'Rykka/riv.vim'

- 最近1行目のプラグインも必要になったようです (2014年 10月 25日 追記) 

コマンドの説明
==============

- :RivQuickStart : rst 形式のテンプレートを出力

  1. 最近気が付きましたがこれは riv.rst のチュートリアルだと思ったほうがよいですね．
  2. :Only : 上記テンプレートのみにします

初期設定ではヘッダごとに折りたたまれる形で表示されます．
この場合，編集したいヘッダの行で Enter を押すと展開されます．

折りたたむ場合はノーマルモードで za を使います．

.. note:: 折り畳み系のキーバインド

   一度書いた rst を編集する際には、この折り畳みはとても便利ですが、
   同時に不便な点もあります。
   例えば、ある誤植を一括置換したいときなどですね。

   そのため、折り畳みに関してのキーバインドは覚えてしまった方が楽かと思います。

   - とりあえず全部開く: :kbd:`zR`
   - とりあえず全部閉じる: :kbd:`zM`


Insert 系
~~~~~~~~~

- :RivTitle0 ... :RivTitle6 : ヘッダを作成
- :RivCreateContent : 目次を作成
- :RivCreateDate : 現在の日にちを挿入
- :RivCreateTime : 現在の時間を挿入
- :RivCreateEmphasis : 現在の単語をイタリック化
- :RivCreateStrong : 現在の単語を強化
- :RivCreateLink : 現在の単語に対するリンク付与 (論文の注釈みたいな感じで)
- :RivCreateFoot : 現在の単語に対する注釈を作成 (文末に [1]... のような感じになります)
- :RivCreateInterpreted : 現在の単語を " ` " で囲む
- :RivCreateLiteralInline : 現在の単語を " `` " で囲む
- :RivCreateLiteralBlock : コードブロックを作成
- :RivCreateHyperLink : 現在の単語を html 化
- :RivCreateTransition : Transition を作成
- :RivCreateExplicitMark : ".. " を挿入

キーバインド
~~~~~~~~~~~~~

ノーマルモード
---------------

- ctrl + e cdd : 現在の日付を挿入
- ctrl + e cs  : 現在の文字を強調
- < : インデントを上げる
- > : インデントを下げる
- ctrl + E s1 : h1 要素にする（以下数値を変更するとh要素になる）


設定
=============

rib.vim のデフォルトの作業ディレクトリは以下のように設定します. ::

   let proj1 = { 'path': '~/Dropbox/rst',}
   let g:riv_projects = [proj1]

複数指定を行う場合は以下の様にします. ::

   let proj1 = { 'path': '~/Dropbox/rst1',}
   let proj2 = { 'path': '~/Dropbox/rst2',}
   let g:riv_projects = [proj1 proj2]

- この場合,デフォルトのディレクトリは最初に記述したものになります.


