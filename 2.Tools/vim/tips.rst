================================
Vim Tips
================================

:Last Change: 20-Apr-2016.
:Author: qh73xe

ここでは vim のデフォルトコマンドに関するメモを
書いて起きます。

.. contents::
    :depth: 2
    :local:

現在読み込んでいるプラグインの一覧を出す
==============================================

:Reference: http://ori3.blogspot.jp/2012/05/vim.html

正確には現在読み込んでいる vimscript の一覧を表示する場合ですが、
以下のコマンドを入力します。

.. code-block:: vimscript

    :scriptnames
