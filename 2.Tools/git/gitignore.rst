================================
gitignore
================================

| Last Change: 27-Aug-2015.
| author : qh73xe
|

このページでは gitignore に関係する Tips をまとめていきます．
gitignore とは git でヴァージョン管理を行っているものの内 **管理しないもの** を設定するファイルです
これは結構重要な話で，例えば tex とか rst の場合の生成物自身（というかコンパイルという発想のあるもの）は，管理をする意味はあまりないですし，
pyc ファイル等，監視してしまうと嫌な思いをするファイルは結構多いです．
そういうものをいかに外していくのかは重要な問題になります．

.. contents::
    :depth: 2

.gitignore のテンプレート
==============================

まず gitignore のテンプレートファイルですが，
github 上にまとめられていますので，
これを使用すると楽かと思います．

- https://github.com/github/gitignore

とりあえず設置をしてみる
===============================

.gitignore はデフォルトではそれ自身を git で管理していないため，
作成時には追加を行う必要があります．
なお作成場所は基本的に .git があるディレクトリです．

.. code-block:: bash

   $ git add .gitignore
   $ git commit -m "Add ignore pattern"


あとからまとめて.gitignoreする方法
======================================

色々あって，一番最初に gitignore をおいていない場合のTipsです．

.gitignore を設置しても、既にリポジトリに登録されているファイルは無視されないので無視したいファイルを管理対象から外します。

.. code-block:: bash

   $ git rm --cached hoge.tmp

ただ、既に大量のファイルが登録されている場合, ひとつひとつやっていたら大変です．
以下のようにするとまとめて除外するとらくです。

.. code-block:: bash

   $ git rm --cached `git ls-files --full-name -i --exclude-from=.gitignore`

特定の PC でデフォルトの ignore ファイルを設定する
====================================================

毎回 gitignore を設定するのは結構面倒です。
そんなときは、適当なところにグローバル用の.gitignore ファイルを作成し、git config で そのファイルのパスを core.excludesfile に設定することも可能です．

.. code-block:: bash

   $ git config --global --add core.excludesfile "$HOME/.gitignore"

.. warning:: 

   ただし，これは各PCの設定であることに注意してください．

