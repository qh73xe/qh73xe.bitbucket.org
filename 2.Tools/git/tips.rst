================================
git: Tips
================================

| Last Change: 20-Oct-2015.
| author : qh73xe
|

このページでは git を使用する上での Tips を記述していきます．
主に設定ファイル関係の話です．

.. contents::
    :depth: 2

複数サーバーへ push する
==============================

git で管理しているレポジトリの中には，
バックアップ，その他の都合で，複数のサーバーに push したい場合があるかと思います．

そのような場合，以下の設定を追加するとよいです

.. code-block:: bash
   :caption: .git/config
   :name: config_for_add_url

   [remote "origin"]
       fetch = +refs/heads/*:refs/remotes/origin/*
       url = https://path/to/your/git1
       url = https://path/to/your/git2

もちろん https の部分はお使いの通信形式で，その他のパスも git レポジトリまでのパスです．

ある特定のファイルのみを元に戻したい
========================================

我々は人間ですので，ついうっかり，大事なファイルを消してしまったり，
なんかよくわからなくなってしまったりすることがあると思います．

こういう時，ある特定のファイルのみのバージョンを戻すには以下のコマンドを使用します．

.. code-block:: bash

   git checkout -f hogehoge

これで安心ですね．
