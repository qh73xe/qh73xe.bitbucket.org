================================
cmus: CUI 音楽プレイヤー
================================

| Last Change: 13-Nov-2015.
| author : qh73xe
|

一般に音楽プレイヤーというと
例えば Itunes のように GUI で操作を行うのが基本だと思います．

cmus が他の音楽プレイヤーと比較して面白い点は，
これが CUI で操作できる部分かと思います．
無駄が一切排除された作りになっているので，とても速く起動し，
操作も vim キーバインドで行うことが可能です．

また，対応している形式も幅広く， Flac までは再生することができます．

導入
===============

Fedora を使用している場合，導入は github から行うのが無難かと思います．

- なぜかレポジトリに入っていないので（ubuntu は入っているようです）

.. code-block:: bash

   git clone https://github.com/cmus/cmus.git
   cd cmus
   ./configure
   sudo make install

ただし，以下のライブラリと依存関係にあるの注意です．

- libcue
- libdiscid
- ncurses
- alsa-lib (optional) - for ALSA output plugin support
- faad2 (optional) - for AAC input plugin support
- ffmpeg (optional) - for ffmpeg input plugin support
- flac (optional) - for flac input plugin support
- libao (optional) - for AO output plugin support
- libcdio-paranoia (optional) - for cdio support
- libmad (optional) - for mp3 input plugin support
- libmodplug (optional) - for modplug input plugin support
- libmp4v2 (optional) - for mp4 input plugin support
- libmpcdec (optional) - for musepack input plugin support
- libpulse (optional) - for PulseAudio output plugin support
- libvorbis (optional) - for vorbis input plugin support
- opusfile (optional) - for opus input plugin support
- wavpack (optional) - for wavpack input plugin support
- faad2 (make)
- ffmpeg (make)
- flac (make)
- libao (make)
- libcdio-paranoia (make)
- libmad (make)
- libmodplug (make)
- libmp4v2 (make)
- libmpcdec (make)
- libvorbis (make)
- opusfile (make)
- wavpack (make)
