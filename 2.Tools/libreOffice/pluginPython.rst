===============================
プラグインをpythonで作成する
===============================

Last Change: 19-Feb-2014.

LibreOfficeの素晴らしい所はプラグインをpythonで作成できる所です.
こう,アンチマイクロソフトなlinuxユーザーがオフィス環境を使う気になれます.
ただ,日本語情報があんまりないのが欠点ですかね.

とりあえず,このページではLibreOfficeのプラグインをpythonで記述する場合に必要な環境作成方法と
どこにスクリプトを置くのか,最後に簡単なテストを行うまでを記述しておきます.

環境構築
====================

Libreでpython製のプラグインを作成するには以下のライブラリを導入する必要があります.

.. code-block:: bash

   $ sudo yum install libreoffice-pyuno

- なお,debian系では,python-unoという名前です.

1. これをインストールした状態で,calcを起動します.
2. "ツール" -> "マクロ" -> "マクロの管理" を選択すると"python(A)"という表記がでるかと思います.

以上で環境設定は終了です.

マクロの置き場
====================

さて,では早速マクロを書いてみようとなるわけですがCalc上から書こうとすると混乱します.
編集ボタンが押せません.
では,どうするのかというと,自分で書いてそれを特定のディレクトリの中に置いて使うようです.

ディレクトリは

- $HOME/.libreoffice/4/user/Scripts/python

におきます.

.. warning::

   初期設定ではこのディレクトリは存在しないかもしれません.
   その場合は,自分で作成します.

test用のスクリプトを作成してみる
=======================================

さて,前準備が終わったので早速スクリプトを作成してみます.
今回は以下のようなスクリプトにしてみました.

.. code-block:: python

   #!/usr/bin/env python
   # vim: set fileencoding=utf-8 

   def test():
     doc = XSCRIPTCONTEXT.getDocument()
     sheet = doc.CurrentController.getActiveSheet()
     A1 = sheet.getCellRangeByName('A1')
     B1 = sheet.getCellRangeByName('B1')
     C1 = sheet.getCellRangeByName('C1')
     C1.Value = A1.Value + B1.Value

このスクリプトでは現在アクティブなシートのA1とB1のセルの数値を受け取って,
C1にその合計を足して示しています.

実行してみるには,Calcを起動し,ツール,マクロ,マクロの実行からtestを選択します.

このようにpythonでマクロを作成する場合,関数がひとつの単位になります.

XSCRIPTCONTEXT.getDocument()は現在開いているファイルから情報を取得せよという命令です(多分.実はまだ公式マニュアル読んでいないので...).

で,getActiveSheet()はシートの指定箇所.
これは現在アクティブなシートを指定しています.
この他,シート名から指定する場合は

.. code-block:: python

   sheet = doc.getSheets().getByName('Sheet1')

のようにしますし,

インデックスから指定する場合

.. code-block:: python

   sheet = doc.getSheets().getByIndex(0)

のようにします.

getCellRangeByName()はカラムを指定し,その情報を取得しています.
この情報は"変数名".Valueで参照できます.

あとはまぁ,pythonの流儀に従えばなんとかなるのではないでしょうか.
