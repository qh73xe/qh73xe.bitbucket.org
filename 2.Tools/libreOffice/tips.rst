================================
LibreOffice: Tips
================================

| Last Change: 03-Sep-2015.
| author : qh73xe
|

.. contents::
    :depth: 1
    :local:

==============================
コマンドラインを使用する
==============================

あまり有名ではない使い方ですが，LibreOffice は shell から起動，操作可能です．
わーい．

ここで注意が必要なのはコマンド名で :command:`soffice` という名前です．

.. note:: コマンドの場所に関して

   Linux を使用している場合，通常はデフォルトで導入されるものだと理解していますが,
   Windows, Mac はそうではない可能性があります．

   公式サイトによると :command:`{install}/program/soffice {parameter}` となっているそうで，
   {インストール} 部には、使用環境における LibreOffice ソフトウェアのインストールパスを指定するそうです (たとえば C:\Program Files\Office や ~/office など).

コマンドオプション
=========================

コマンドオプションの詳細は `公式サイト <https://help.libreoffice.org/Common/Starting_the_Software_With_Parameters/ja>`_
を確認ください．

ここでは個人的によく使いそうなものをメモしておきます．


.. csv-table:: コマンドラインオプション
   :header: コマンド名, 説明

   --writer,    起動時に、空白の Writer ドキュメントを開くようにします。
   --calc,      起動時に、空白の Calc ドキュメントを開くようにします。
   --draw,      起動時に、空白の Draw ドキュメントを開くようにします。
   --impress,   起動時に、空白の Impress ドキュメントを開くようにします。
   --view {filename},   {filename} で指定するファイルのコピーを一時的に作成させ、読み取り専用状態で開くようにします。
   --nologo,    起動時のスプラッシュ画面を非表示にします。
   -o {filename},   {filename} で指定するファイルを、テンプレートの場合も含めて、編集可能な状態で開くようにします。
   -n {filename},   {filename} で指定するファイルをテンプレートとして、新規ドキュメントを作成するようにします。
