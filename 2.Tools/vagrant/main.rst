Vagrant
==================================

これは何??
---------------------------------

Vagrant とは仮想マシンを簡単に立ち上げるためのツール.
ローカル開発環境用の仮想マシンを立ち上げる時に使用する.

公式サイト
~~~~~~~~~~~~~~~~~~~

http://www.vagrantup.com/

必要な知識
~~~~~~~~~~~~~~~~~~~

- 簡単な unix コマンド
- Rubyで作られている

準備
-------------------------

Vagrant の導入
~~~~~~~~~~~~~~~~~~~

とりあえず, 私の環境は fedora20 なのでそれ用に記述します.
他のOSに関しては公式サイトよりダウンロードすれば良いかと思います.

fedoraの公式レポジトリには vagrant は登録されていないため,
公式サイトよりrpmファイルをダウンロードします.

一番野暮ったい使用方法を記述しておきますと,

1. 公式サイトに移動
2. Downloadをクリック
3. ご使用のOSに対応するものをクリック
4. ダウンロードされたファイルを解凍といったところでしょうか.

最近のLinuxではGUIでココらへんの作業をこなしてくれはするので,特に深い記述は必要ではないかもしれませんが,
とりあえず依存関係のみメモしておきます.

.. code-block:: bash
  
  $ sudo yum install libvirt-devel libxslt-devel libxml2-devel

VirtualBox の導入
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Vargrantで使用できる仮想環境には以下のものが使用できます.

- VirtualBox
- VMWare
- EC2

いずれの仮想環境を使用してもよいのですが,今回は virtualBox を使用します.

Fedora で virtualBox を使用する場合,以下のようにします.

.. code-block:: bash
  
  $ su -
  $ cd /etc/yum.repos.d/
  $ wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
  $ yum update
  $ yum install binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms
  $ yum install VirtualBox-4.3

仮想マシンを立ててみる
-------------------------------------------

さて,それでは準備が整ったので vagrant を使用してみます.
Vargrant の使用は以下の3つの手順を踏みます.

1. Box(テンプレート)の入手
2. 仮想マシンの初期化
3. 仮想マシンの起動

Boxの入手
~~~~~~~~~~~~~~~~~~~~~~

まずはBoxを入手します.
Boxとは仮想マシンの構成のテンプレートのことだと思ってください.
仮想マシンとは,要は一つのPCで複数のOSを使用する方法のことです.
で,そのOSの構成には色々とあって,手順が複雑なわけですが,
この構成のうちよくある構成を有志の方がテンプレ化してくれているわけです.
Vagrant は,そのテンプレを使用できるため,簡単に仮想マシンを使用できるようになるわけです.

今回は試しに公式サイトに記載されている (2013年5月7日現在) Boxを入手してみます.
端末を開き以下のコマンドを入力します.

.. code-block:: bash
  
  $ vagrant box add hashicorp/precise32


