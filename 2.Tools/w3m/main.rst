================================
w3m: テキストブラウザ
================================

:Last Change: 09-May-2016.
:Author: qh73xe
:Reference:

そういえば、まだこの tool に関して書いていませんでした。
w3m はテキストブラウザという古の技術です。

ようは firefox のような html プラウザ の CUI 版です。
そう、 CUI で google 検索ができるのです.

私のようなスクリプトキャットは、どうしても調べ物が文字中心になります。
そのため、下手に画像や凝った js を見せられるより、テキストだけを読みたい場合も結構あります。
こういう時に便利な tool です。

テキストブラウザというと w3m 以外もの色々あるのですが、
このツールは挙動も軽く、 テーブルの表示が可能であることが良い点かと思います。

導入方法
================================

一般的な Linux であれば（語彙矛盾なんか感じていない...）, apt や dnf などで、導入できます。

.. code-block:: bash

   $ sudo dnf install w3m

画像の読み込み機能が必要な場合以下のライブラリも導入しておくとよいでしょう。

.. code-block:: bash

   $ sudo dnf install w3m-img

だたし、この tool が使用できるのは xterm などのやや古目の端末だけです。
gnome-term 等では効果がないので注意してください。

使い方
======================

w3m は CUI アプリであるため、
ターミナルから呼び出します。
この際、単純に w3m と入力しても意味がありません。
基本的な使用方法は、以下のように url を入力します。

.. code-block:: bash

   $ w3m http://google.com

ただし、このように使用していくのは、
一般的な web ブラウザに慣れてしまった身からすると結構使いずらいかと思います。

ここではその対応方法として二つのやり方を紹介します。

bash/zsh 側で設定を行う
--------------------------------
まず一つ目は環境 WWW_HOME に特定の url を設定してしまうことです。
例えば shell 上で、以下のコマンドを入力した後に w3m を開くと
url の入力なしで google のページが開きます。

.. code-block:: bash

   $ export WWW_HOME="google.co.jp"
   $ w3m

いっそのこと shell コマンドを作成する
------------------------------------------

上記の方法ではとりあえず google が開きますが、
そこから 検索語を入力する手間がかかります。

ならば いっそのこと最初から 検索語を入力すると
該当のページを google で検索してくれるコマンドを作成した方が早いです。

- また、この方法をとると、後で tmux とかそういうのと連携しやすくて好きです。

私の場合 zsh を使用しているので .zshrc に以下のような関数を作成しています。

.. code-block:: bash

   function google() {
       local str opt
       if [ $# != 0 ]; then
           for i in $*; do
               str="$str+$i"
           done
           str=`echo $str | sed 's/^\+//'`
           opt='search?num=50&hl=ja&lr=lang_ja'
           opt="${opt}&q=${str}"
       fi
       w3m http://www.google.co.jp/$opt
   }

こうすることで 以下のようにブラウジングが可能になります。

.. code-block:: bash

   $ google hoge foo

方法は単純ですが、結構便利です。
また, tmux を使用している場合、
.tmux.conf に以下の設定をしておくと、
プレフィックス + g で google 検索が行え、
かつ、検索終了時には ペインが閉じるようになるので
検索がしやすいかと思います。

.. code-block:: bash

   bind-key g command-prompt -p "google:" "split-window 'source ~/Documents/ect/zsh/zshrc; google %%'"

外部コマンドを利用する
=====================================

さて、続いては w3m の機能拡張を考えたいと思います。
w3m 上で ! を入力すると 外部コマンドを走らせることが可能です。
例えば以下のように入力すると現在開いている url を vim で表示します。

.. code-block:: bash

   ! printenv | grep W3M_URL | vi -

つまり w3m 上で任意のコマンドを走らせることが可能です。
一方で、一々コマンドを入力するのもたるいので、設定ファイルに記述してしまうとよいと思います。

ここでは ~/.w3m/keymap を編集（無い場合は作成）してください。

例えば上記のコマンドを A というキーバインドに割り当てるには以下のように記述します。

.. code-block:: bash

   keymap  A   EXEC_SHELL "printenv |grep W3M_URL | vi -"

これで大抵勝てますね。
