================================
Tmuxinator: tmux の設定管理
================================

| Last Change: 15-Jan-2016.
| author : qh73xe
|

このページでは tmux のセッション管理ツール Tmuxinator を紹介します。

.. contents:: 目次

これは何？
====================

tmuxinatorは、tmuxで起動するセッションを予め定義しておき、そのセッションを起動できるようにしたものです。
例えば、何か git で管理している python のプロジェクトがあるとして、
単純に考えた場合、取り敢えず git pull を行い、シェル（Ipython とか）をデバック用に作成し、
vim を開いてスクリプトを行うようなことをするとします。

で、これをいくつかのペインで行うとして、毎回同じペインを作成するのって結構面倒くさいです。
しかし、この tmuxinator を利用するとこれをコマンド一つで再現することができ、結構便利です。

.. note:: コマンドを打つのもタルい場合。

   もしかすると、「どうせ作業を行うディレクトリも決まっているのに一々コマンドを
   打つのもメンドイぜ」という人が居るかもしれません。
   そういうときには direnv と連携させ、あるディレクトリに移動した際には自動で、
   tmuxinator のコマンドを使用するように設定することも可能です。

導入方法
=================

Tmuxinator は ruby 製のアプリケーションなので、 gem からインストールを行うことが推奨されています。

.. code-block:: bash

   gem install tmuxinator

ここで、環境変数に以下の二つを指定する必要があることに注意してください。

.. code-block:: bash

   export EDITOR='vim'
   export SHELL='zsh'

- ここは EDITOR と SHELL が指定されていればよいです。

以下のコマンドを入力すると、
環境変数が設定されているのかを確認できます。

.. code-block:: bash

   tmuxinator doctor

また、使用している shell に合わせて以下の設定を記述します。

.. code-block:: bash
   :caption: .zshrc
   :name: zsh

   source ~/.bin/tmuxinator.zsh

基本的な使い方
====================

Tmuxinator を使う上で覚えておくべきコマンドは基本的に二つです。

.. code-block:: bash

   mux new hogehoge

このコマンドでは hogehoge という名前の Tmuxinator の設定ファイルを作成できます。

.. note:: 設定名に関して。

   Tmuxinator を使う上で一つ注意が必要なこととして設定名があります。
   default と名前を使用すると、基本的にデフォルトで使用されることに（つまりログイン時に勝手に呼び出されることに）注意してください。


.. code-block:: bash

   mux hogehoge

このコマンドで、 hogehoge に設定した状態で tmux を再現できます。


設定ファイルの記述方法
==========================

ruby 製のアプリケーションらしく、 yml を使い設定を行います。
基本的に設定は以下のように記述します。


.. code-block:: bash

   # ~/.tmuxinator/sample.yml

   name: sample
   root: ~/
   windows:
     - editor:
          layout: main-vertical
          panes:
            - vim
            - guard
     - server: bundle exec rails s
     - logs: tail -f log/development.log

簡単に各種の設定の意味する所を説明すると大体以下の通りです。

- name: mux コマンドで呼び出す際に使用する名前
- root: この設定で使用するルートディレクトリ
- windows: tmux 上でのウインドウ
   1. layout: あるウインドウでのパネルレイアウト（even-horizontal, even-vertical, main-horizontal, main-vertical, tiled）
   2. panes: あるウインドウでのパネル
       A. それ以下: あるパネルで初期に使用するコマンド

大体、 セッション、ウインドウ、パネルの順で構造化されていると思えばよいです。
また、各階層には pre: いう設定を行うことが可能です。
これは、各セッション、ウインドウ、パネルが起動する前に行うコマンドを記述することが可能です。
例えば git pull とかはこのレベルで操作をする方が安全かも知れません。

Tmuxinator の切り替えを行いたい
==================================

現状、mux コマンドで何かの設定を呼び出したあと、
別の設定を呼び出すと、 tmux 内で tmux を呼び出すような感じになってしまいます。
ここで、tmux 側の設定で、入れ子呼出を禁止している場合、
当然、うまく mux コマンドが機能しません。

これは、一旦現在のセッションを kill し、新しく mux コマンドを走らせる必要があるようです。
そうではなく、新規ウインドウを作成する設定が欲しいです。
