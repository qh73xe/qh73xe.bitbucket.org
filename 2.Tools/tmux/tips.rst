================================
tmux : Tips
================================


:Last Change: 18-May-2016.
:Author: qh73xe

cygwin でクリップボートを共有する
==================================

:OS: windows

以下の設定を行うことで cygwin 上の tmux のバッファとクリップボートの共有ができます．
tmux 上でコピー範囲を指定したあと :kbd:`Enter` か :kbd:`y` でコピーされます．

.. code-block :: bash

   bind-key -t vi-copy y copy-pipe "iconv -s -t SJIS | clip"; \  
   bind-key -t vi-copy enter copy-pipe "iconv -s -t SJIS | clip"; \


一時ペインを使用する
=======================

以下の構文を覚えて置くと tmux から出来る操作の幅が広がります．

.. code-block :: bash

   bind-key キーバインド split-window 'シェルコマンド'

このようにすると，あるキーバインドを押した際に，
新しいペインを開き，そこで指定のシェルコマンドを実行します．
このコマンドが終了した際には，さきに開かれたペインは自動で閉じます．
ですので，vim や less, pandoc などと組み合わせると，一瞬何かの作業を行い必要なく
なったらそれを閉じることが簡単になります．


command-prompt
===================

command-prompt とは要は tmux の対話的インターフェースだと思えばよいです。
これは以下の構文で設定を行います。

.. code-block :: bash

    bind-key キーバインド command-prompt -p "文字列" "tmuxコマンド 'シェルコマンド %%'"

この設定を行い，指定のキーバインドを使うと tmux の下の方に，入力欄が出てきます．
ここで入力した値は上記の %% に代入されます．

例えば

.. code-block :: bash

    bind-key V command-prompt -p "vim temp" "split-window 'vim ~/Documents/temp/%%'"

としておけば，shift-v を押したタイミングで入力欄が出来， ~/Documents/temp/ 以下
に入力したファイル名のテキストを編集して置くことが可能です．
もちろんこれは自作したスクリプトに対しても使用できますので，実際的には tmux の拡
張がかなり自由にできることになります．

.. note:: よく使う tmux コマンド

    command-prompt 設定を行う際によく使用するコマンドは大体以下の二つではないでしょうか？

    - split-window: 現在のウインドウに一時的なペインを作成する
    - run-shell: 後で指定したシェルスクリプトを実行する

    要は現在のウインドウで特定のシェルスクリプトを実行したい場合には run-shell
    を一旦ウインドウを分割して特定のシェルスクリプトを実行したい場合には split-window を利用すればよいです。

新規ペインで今いる場所を開く
===============================

これは tmux のバージョンによって仕様が異なります．
tmux 1.8 以前では新規ペインを開くと，今いたディレクトリが開くようになっていまし
たが，tmux 1.9 以降では，ホームディレクトリが開くようになっています

これを元に戻すには以下のような設定を行います．

.. code-block :: bash
   :caption: .tmux.conf
   :name: .tmux.conf

   bind c new-window -c '#{pane_current_path}'
   bind '"' split-window -c '#{pane_current_path}'
   bind % split-window -h -c '#{pane_current_path}'

．
