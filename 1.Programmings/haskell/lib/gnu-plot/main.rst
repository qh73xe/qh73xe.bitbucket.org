================================
Graphics.Gnuplot: Plot
================================

:Last Change: 13-May-2016.
:Author: qh73xe
:Reference: https://hackage.haskell.org/package/gnuplot

私は基本的にはデータ屋で、
新しいプログラミングを行うにしても、
とりあえずデータの可視化方法は欲しいと考える人間です。

とりあえず Haskell で行うためには Graphics.Gnuplot が簡単かと思います。

.. contents:: 目次

事前準備
========================================

このライブラリは `gnuplot <http://www.gnuplot.info/>`_ へのラッパーだと思えばよいです。
そのため 先ずは gnuplot をインストールしておく必要があります。
その上で、 Graphics.Gnuplot を導入しましょう。

.. code-block:: bash

   $ sudo dnf install gnuplot
   $ cabal install gnuplot

簡単なサンプル
========================================

.. code-block:: haskell

   import Graphics.Gnuplot.Simple
   let xs = linearScale 1000 (0, 2*pi)
   let ys = fmap sin xs
   let points = zip xs ys
   plotPath [(Title "hello")] points
