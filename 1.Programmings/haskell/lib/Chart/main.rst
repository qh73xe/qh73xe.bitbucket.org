================================
haskell-chart
================================

:Last Change: 05-Jul-2016.
:Author: qh73xe
:Reference: https://github.com/timbod7/haskell-chart/wiki

haskell で plot を行うためのライブラリです。

導入方法
=====================

以下のライブラリを導入します。

.. code-block:: bash

   $ cabal update
   $ cabal install chart-diagrams
   $ cabal install chart-cairo
   $ cabal install chart-gtk

.. note:: stack を利用する場合

   Haskell においてライブラリを導入する場合、 cabal が有名ですが、
   stack を利用して全体的な環境は汚さないようにしていることもあると思います。
   この場合には cabal での方法では上手く行かないので注意してください。

   私の環境の場合、以下のように導入しました。

   .. code-block:: bash

      $ LANG=C stack install Chart-diagrams
      $ LANG=C stack install Chart-cairo

基本的な使い方
==========================

ここでは、具体的な plot 設定は一切行わない感じで
とりあえず描画ができるようにします。

.. code-block:: haskell
   :linenos:
   :caption: testPlot.hs
   :name: plot_png

   import Graphics.Rendering.Chart.Easy
   import Graphics.Rendering.Chart.Backend.Cairo

   signal :: [Double] -> [(Double,Double)]
   signal xs = [ (x,(sin (x*3.14159/45) + 1) / 2 * (sin (x*3.14159/5))) | x <- xs ]

   main = toFile def "example1_big.png" $ do
       layout_title .= "Amplitude Modulation"
       setColors [opaque blue, opaque red]
       plot (line "am" [signal [0,(0.5)..400]])
       plot (points "am points" (signal [0,7..400]))


実行は以下の通りです.

.. code-block:: bash

   $ runghc testPlot.hs

このようにすると、 example1_big.png が作成されます。
一方で単純に GUI で 図 の確認のみをしたい場合以下のようにすればよいです。

.. code-block:: haskell
   :linenos:
   :caption: testPlot.hs
   :name: plot_gui
   :emphasize-lines: 1, 7-8

   import Graphics.Rendering.Chart.Easy
   import Graphics.Rendering.Chart.Backend.gtk

   signal :: [Double] -> [(Double,Double)]
   signal xs = [ (x,(sin (x*3.14159/45) + 1) / 2 * (sin (x*3.14159/5))) | x <- xs ]

   main = toWindow 300 300 $ do
       layout_title .= "Amplitude Modulation"
       setColors [opaque blue, opaque red]
       plot (line "am" [signal [0,(0.5)..400]])
       plot (points "am points" (signal [0,7..400]))

- ghci で対話的に描画を行う方法は現在検索中
