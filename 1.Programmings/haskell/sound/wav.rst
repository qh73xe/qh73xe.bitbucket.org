================================================================
Data.WAVE: Haskell で wav を扱う
================================================================

:Last Change: 13-May-2016.
:Author: qh73xe
:Reference: https://hackage.haskell.org/package/WAVE-0.1.3/docs/Data-WAVE.html

.. contents:: 目次

導入方法
=========================

.. code-block:: bash

   $ cabal install WAVE

wave の読み込み
===========================

とりあえず Haskell で wav ファイルを読み込みます。
これには先に導入した Data.WAVE を利用します。

このパッケージには :command:`getWAVEFile` という関数が定義されているのでこれを利用しましょう。<`0`>

- getWAVEFile :: String -> IO WAVE
    - data WAVESource
        - waveHeader :: WAVEHeader
            - waveNumChannels :: Int  Samples per frame.
            - waveFrameRate :: Int Frames per second.
            - waveBitsPerSample :: Int Number of significant bits of left-justified value.
            - waveFrames :: Maybe Int 
        - waveSamples :: WAVESamples

.. code-block:: haskell

   import Data.WAVE
   WAVE header samples <- getWAVEFile "hoge.wav"

これで header の中に waveHeader が、 samples の中に wavSample が読み込まれます。

wave クラスの確認
===========================

中身を確認するには以下のような感じで。

.. code-block:: haskell

   waveNumChannels header
   waveFrameRate header

wave の書き込み
===========================

wave ファイルを書き出すには以下のようにします。

.. code-block:: haskell

   putWAVEFile "foo.wav" (WAVE header sample)

詳しく見てみる
==========================

ここでは、上記のモジュールを使用するのではなく
:doc:`C 言語による wav ファイルの読み込み <../../c/sound/main>` のように
一から実装できるようにします。

- とはいいつつ、参考にしますが

まず、 WAVE ファイルのクラスを考えます。
これは 形式上、ヘッダ と データに分けることが可能です。

Haskell の基本は小さい部分から作成していくことです。
まずは各データに関して考えると、これは n bit で表現されるデータです。
ここでは 32bit ということで考えます。

この集合が WAVE のデータチャンクですから、
それをリストにしたものも定義しましょう。

.. code-block:: haskell

   import Data.Int

   type Sample = Int32
   type Samples = [[Sample]]

つづいて フォーマットチャンクを考えます。
重要なのはサンプリング周波数と、量子化精度なので、
これらの情報をもつようにします。

.. code-block:: haskell

   data Header = Header {
      fs :: Int,
      bits :: Int,
   }

あとは wav クラスを作成します。

.. code-block:: haskell

   data WAV = WAV {
       header :: Header,
       samples :: Samples
   }

続いては読み込み関数を作成していきましょう。
