import Data.Int

data WAV = WAV {
    header :: Header,
    samples :: Samples
}

data Header = Header {
   fs :: Int,
   bits :: Int
}

type Sample = Int32
type Samples = [[Sample]]

getWAV :: String -> IO WAV
getWAV fn = do
    h <- openFile fn ReadMode
    wav <- hGetWAVE h
    hClose h
    return wav
