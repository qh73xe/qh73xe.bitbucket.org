sinan :: Int -> Double -> [Double] -> [Double]
sinan n x all@(y:_)
 | abs y < precision = all
 | otherwise = sinan (n+1) x (-1.0* x**2 * y / fromIntegral ((2*n+3)*(2*n+2)) : all)
 where precision = 1e-16

sin' :: Double -> Double
sin' x | x == 0.0 = 0.0
       | otherwise = sum (sinan 0 x [x])
