================================
イントロダクション
================================

:Last Change: 13-May-2016.
:Author: qh73xe
:Reference: http://learnyouahaskell.com/introduction

導入方法
========================

とりあえず、 Haskell 環境を導入しましょう。
通常 Linux を使用している場合、 Haskell の導入は パッケージマネージャ系のコマンドからできると思います。

以下では Fedora を例に導入コマンドをメモしておきます。

.. code-block:: bash

   sudo dnf install haskell-platform

簡単ですね。

.. warning:: このサイトに関して

   一応サイト冒頭にも記述しているのですが、
   このサイトでは OS 環境を基本的には Linux 前提にしています。

   - Win や Mac に関する情報もないわけではないのですが、必要を感じない場合特に記述はしません。

対話環境
========================

:Reference: https://wiki.haskell.org/Haskell%E5%85%A5%E9%96%80_5%E3%82%B9%E3%83%86%E3%83%83%E3%83%97

導入が修了したら端末上で :command:`ghci` と入力します。

.. code-block:: bash

   $ ghci
   GHCi, version 7.8.4: http://www.haskell.org/ghc/  :? for help
   Loading package ghc-prim ... linking ... done.
   Loading package integer-gmp ... linking ...done.
   Loading package base ... linking ... done.
   Prelude>

するとなんということでしょう。
対話環境が開きます。
コンパイル言語なのに、コンパイル言語なのに。

とりあえず Hellow world でもやってみましょうか.

.. code-block:: haskell

   Prelude> "Hello, World!"
   "Hello, World!"

   Prelude> putStrLn "Hello World
   "Hello World

コンパイル
========================

対話環境が機能することを確認したら、
次にコンパイルをしてみましょう。

以下の様に hello.hs ファイルを作成します。

.. literalinclude:: ./source/1/hello.hs
   :language: haskell
   :linenos:
   :caption: hello.hs

コンパイルは以下のように行います。

.. code-block:: bash

   $ ghc -o hello hello.hs

こうすると、 hello という実行ファイルができます。

.. code-block:: bash

   $ ./hello
   Hello, World!

コンパイルもできました。
わーい。

関数定義
=====================

楽しいことをしてみましょう。
Haskellでの真に最初のプログラムは階数を計算する関数です。
再びインタプリタに戻って関数を定義してみましょう。

.. code-block:: haskell

   Prelude> let fac n = if n == 0 then 1 else n * fac (n-1)
   Prelude> fac 20
   2432902008176640000

これだけでも面白いのですが、以下のように記述することもできます。

.. literalinclude:: ./source/1/fac.hs
   :language: haskell
   :linenos:
   :caption: fac.hs

そしてコンパイルして実行します。

.. code-block:: bash

   $ ghc -o fac fac.hs
   $ ./fac
   2432902008176640000

並列プログラミング
==========================

コンパイラを導入した段階で、
並列プログラミングにも触れます。

.. literalinclude:: ./source/1/A.hs
   :language: haskell
   :linenos:
   :caption: A.hs

上記の様に、式に `par` と付け加えることで、並列プログラムを書くことができます。

これをコンパイルする際には、以下のようにします。

.. code-block:: bash

   $ ghc -O2 --make A.hs -threaded
   $ time ./A +RTS -N2

- 実行は、 2コアの場合の例です。

Stack
===========================

:Reference: http://qiita.com/tanakh/items/6866d0f570d0547df026

Stack は Haskell のパッケージをビルドしたりインストールしたりするツールです。
イメージ的には、 python の pyenv + virtualenv のような感じでしょうか。

導入方法
--------------------

OS によって導入方法が異なります。
ここではとりあえず主要な Linux ディストリ でインストールする方法を記述します。

Fedora 23 (x86_64)::
    .. code-block:: bash

       $ curl -sSL https://s3.amazonaws.com/download.fpcomplete.com/fedora/23/fpco.repo | sudo tee /etc/yum.repos.d/fpco.repo
       $ sudo dnf -y install stack

openSUSE Leap::
    .. code-block:: bash

       $ sudo zypper ar http://download.opensuse.org/repositories/devel:/languages:/haskell/openSUSE_Leap_42.1/devel:languages:haskell.repo
       $ sudo zypper in stack

Ubuntu 16.04 (amd64)::
    .. code-block:: bash

       $ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 575159689BEFB442
       $ echo 'deb http://download.fpcomplete.com/ubuntu xenial main'|sudo tee /etc/apt/sources.list.d/fpco.list
       $ sudo apt-get update && sudo apt-get install stack -y

使い方
-----------------------

プロジェクトを開始するには該当のディレクトリに移動し :command:`stack new <プロジェクト名>` というコマンドを入力します。

.. code-block:: bash

   $ stack new hoge
   $ cd hoge
   $ stack setup

:command:`stack setup` を実行すると :file:`stack.yaml` の内容に合わせて環境を構築します。
スクリプトのビルドには :command:`stack build`, 実行には :command:`stack exec` を実行します。

.. code-block:: bash

   $ stack build
   $ stack hoge-exe
   someFunc

内容を簡単に見ていくと :file:`app/main.hs` が実行ファイルで、 :file:`src/Lib.hs` が関数の実体です。
あとは作成したい アプリケーションに合わせて適当にスクリプトを書いていけばよいと思います。

ghc-mod
========================

:Reference: https://github.com/DanielG/ghc-mod

Haskell は静的な言語であるため、シンタックスチェックはかなり優秀です。
Haskell のシンタックスチェックを行うには ghc-mod を導入しましょう。

.. code-block:: bash

   cabal update && cabal install ghc-mod

