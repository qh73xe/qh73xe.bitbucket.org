================================
pyslack: slack API ラッパー
================================

| Last Change: 12-Aug-2015.
| author : qh73xe
|

.. contents::
    :depth: 2

これは何
=================

名前の通り, slack というチャットツールの API を叩けます.
まだ, 触ったばかりですが, 使用感は悪くないです.

導入
=================

pip からいけます.
python3 対応です.

.. code-block:: bash

   $ sudo pip install pyslack -U

トークンの取得
================

slack の API を叩くにはトークンという, 文字列を取得する必要があります.
以下, トークンの取得方法をメモしておきます.

1. slack にログイン
2. https://api.slack.com/web にアクセス
3. Authentication の項目から, 自分の使用したいアカウントを見つけ, Create token というボタンをクリック
4. 認証
5. 文字列が発行されるので, それをコピペ

コピペしたトークンを今後使用していくので, どこかにメモしておいてください.

指定のチェンネルに POST する
================================

とりあえず, slack に POST してみます.
大体以下の通りです.

.. code-block:: python
   :caption: post from python
   :name: test.py

   from pyslack import SlackClient
   client = SlackClient('先ほど取得したトークン')
   client.chat_post_message('チャンネル名', "ポストしたい文章", username='ボット名(ユーザー名)')

簡単ですね.
