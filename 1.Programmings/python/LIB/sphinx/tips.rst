================================
Sphinx: Tips
================================

| Last Change: 15-Jan-2016.
| author : qh73xe

このページでは sphinx を運用していくうえで便利な Tips を紹介します．

.. contents:: 目次
   :depth: 2

サーバー上での自動ビルド
============================

このページは私の所属する研究室のサーバーを借りて運営しています．
このサーバーには :command:`git` が入っており，このサイトのファイルも :command:`git` で管理しています．

このような条件で，ローカルで記述した内容をサーバーに :command:`push` したタイミングで
サーバーサイドでも自動でデプロイを行いたいという需要は割と高いかと思います．

これには git の フック機能 と :command:`sphinx-build` を利用するとよいです．

- git のフックに関してはここでは解説を行いません．以下のページを参照してください．
    - http://git-scm.com/book/ja/v1/Git-のカスタマイズ-Git-フック

解決策
--------------

私の場合大体以下のように作業を行いました．

+ サーバーに移動
+ 適当なディレクトリに sphinx プロジェクトを :command:`pull`
+ sphinx project を管理している git レポジトリに移動
+ :file:`hooks/post-update` を編集

.. code-block:: bash
   :caption: post-update
   :name: post-update1

    #!/bin/sh

    cd <サーバーサイドで sphinxのプロジェクトを pull するディレクトリ>
    git --git- dir=.git pull
    sphinx-build -b dirhtml -q . <公開先>

これで，:command:`$ git push origin master` を行えば，
サーバでは 2 で指定したディレクトリ に更新内容を pull してきて 3 で指定した :file:`公開先` へ html ファイルを送ることが可能です．

.. note:: sphinx-build のオプションに関して

   上記の例では sphinx-build にオプションを追加しています．

   -q:
        標準出力に何も出力しないようになります。警告やエラーのみが標準エラー出力に書き出されます。


- 上記スクリプトでは :command:`push` されるたびに :command:`pull` するようにしているため事前に :command:`clone` しておく必要があります．

.. note:: git hooks に関して

   はじめて git hook を使用する場合，:file:`gitレポジトリ/hooks/post-update` は存在しません
   これは自分で作成します．また，このファイルに実行権限( :command:`chmod +x` )を与える必要があります．

- 参考 : http://blog.kzfmix.com/entry/1309561912

Google Analytics や Google Adsence を貼りたい
==============================================

sphinx では毎回 html を書き換えます．
Google Analytics や Google Adsence を使用する場合は header 等に js を貼り付ける
必要があります．

解決
---------

:file:`_templates` に :file:`layout.html` というファイルを作成します。

:file:`layout.html`

.. code-block :: html

    {% extends "!layout.html" %}

    {% block sidebarsearch %}
    {{ super() }}

    ここにGoogle AdsenceのJavaScriptを貼る

    {% endblock %}

    {% block footer %}
    {{ super() }}

    ここにGoogle Analyticsを貼る

    {% endblock %}


- {% extends "!layout.html" %}    既存のlayout.htmlを上書きする設定
- {%block sidebarsearch %}    サイドバーの検索を上書きするいう宣言
- {{ super()}}    ベースのlayout部分を埋め込む
- {% endblock %}    block(ここではsidebarsearch)の終わり


toctree で正規表現
======================

toctree を使用すると sphinx プロジェクト内でリンク一覧を作成することができます．
例えばこんな感じですね．

.. code-block :: python

   .. toctree::
      :maxdepth: 2

      intro
      strings
      .
      .
      .

しかし，ディレクトリ構成をしっかりしている場合，正規表現でまとめて記述したい場合
があるかもしれません．

解決
-----------

このような場合， :command:`:glob:` を使用するとある程度の正規表現を使用できます．
例えば，このような感じです．

.. code-block :: python

   .. toctree::
      :maxdepth: 2
      :glob:

      intro/*/main.rst

この例では :file:`intro` ディレクトリ以下にあるすべてのサブディレクトリ以下の
:file:`main.rst` ファイルに対してリンク一覧を作成します．

