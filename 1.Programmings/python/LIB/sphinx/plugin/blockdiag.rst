================================
sphinx に図を書き込む
================================

| Last Change: 22-Oct-2015.
| author : qh73xe
|

このページでは sphinx にブロック図を書き込む機能を追加するプラグイン sphinxcontrib-blockdiag に関してのメモを書きます.

.. contents::
   :depth: 2
   :local:

導入方法
==================

.. code-block:: bash

   $ sudo easy_install install sphinxcontrib-blockdiag

sphinxcontrib-blockdiag は python のライブラリとして作成されているので,
pip や easy_install から導入できます.

ただ, 私の環境(Linux, fedora22) では pip をすると PIL でコケるので easy_install を使用しました.
