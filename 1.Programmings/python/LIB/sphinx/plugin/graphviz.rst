================================
sphinx.ext.graphviz
================================

:Last Change: 15-Jun-2016.
:Author: qh73xe
:Reference: http://docs.sphinx-users.jp/ext/graphviz.html

graphviz は グラフを記述するためのツールです。
基本的にはスクリプトを書くとそれを svg とか png とかにしてくれるツールですがsphinx から呼び出すことも可能です。

.. warning:: グラフとは

   ここで対象にしているグラフという語は、
   いわゆる図一般のことではなく、
   ノードとパスで表現される図のことをさします。

   一般的にはディレクトリ構造とか、ライブラリの構成といった親子関係を
   示すことに向いています。

sphinx から graphviz を呼び出すためのプラグインは
最新の sphinx では組み込みで容易されています。

使用方法
==========================

上記プラグインを利用するために conf.py の extensions 変数を編集します。

.. code-block:: python
   :caption: conf.py

   ...
   extensions = [
       ...
       'sphinx.ext.graphviz',
   ]
   ...

ドキュメント内部での記法
=============================

sphinx.ext.graphviz モジュールは以下のディレクティブを提供します。

graphviz
--------------------

graphviz の スクリプト をドキュメント内に直接記述できます。

.. code-block:: rst
   :caption: graphviz

   .. graphviz::

      digraph foo {
          "bar" -> "baz";
      }

また、外部ファイルとして graphviz のコードを記述し、
それを読み込むことも可能です。

.. code-block:: rst
   :caption: graphviz

   .. graphviz:: hoge.dot

このディレクティブはコードを全て記述することになるので、
場合によっては面倒くさいことがあります。

そのような場合以下に紹介するディレクティブを利用しましょう。

graph
----------------

無向グラフ (パスが矢印ではないグラフ) を記述したい場合には、
graph ディレクティブを利用するとよいです。

.. code-block:: rst
   :caption: graph

   .. graph:: foo

      "bar" -- "baz";

結果は graphviz ディレクティブで記述したものと同じ構造になると思います。

digraph
----------------

有向グラフを記述する場合には digraph ディレクティブを利用します。
これは以下のように利用します。

.. code-block:: rst
   :caption: digraph

   .. digraph:: foo

      "bar" -> "baz" -> "quux";

設定
===================

デフォルトで出力されるファイルは png 形式ですが、
svg 形式で出力することもできます。

個人的には svg の方が何かと便利なので、
この設定方法を記述します。

基本的には conf.py に graphviz_output_format という変数を追加し、
これに 'svg' という文字列を与えればよいです。

.. code-block:: python
   :caption: conf.py

   ...
   extensions = [
       ...
       'sphinx.ext.graphviz',
   ]
   ...
   graphviz_output_format = 'svg'

