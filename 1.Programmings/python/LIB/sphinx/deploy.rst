================================================================
sphinx ドキュメントをデプロイする
================================================================

| Last Change: 25-Mar-2016.
| author : qh73xe
|

.. contents::

このページでは作成した sphinx ドキュメントを公開する方法に関して記述します。
基本的に、無料で行えて、同期が簡単な方法を説明したいと思います。

デプロイ環境
============================

ドキュメントを公開する環境として `Bitbucket <https://bitbucket.org/>`_ と `Read The Docs <https://readthedocs.org/>`_ という二つのサービスの連携を考えます。

まず `Read The Docs <https://readthedocs.org/>`_ ですが、これは最終的に sphinx ドキュメントを html に変換し、公開するためのサービスです。
もしかしたら、このサービスは sphinx theme の一つとして知っている人もいるかと思います。
このサービスをドキュメント公開先として利用している python ライブラリも結構あり、とても便利に使えます。

一方の `Bitbucket <https://bitbucket.org/>`_ は バージョン管理アプリケーションである git のホスティングサービスです。
同種のサービスとして git hub が有名ですが、個人的には bitbucket の方が無料で使用できる範囲ではサービスが良いので好んでいます。

Read The Docs は基本的にどこかに公開されている git レポジトリを監視し、 Read The Docs 内に自動でクローン、html その他にビルドをするシステムです。
そのため、 Read The Docs を利用するためにはどこかに sphinx ドキュメントをまとめた git レポジトリが必要になります。

.. note:: Read The Docs の対応先に関して

    このサイトでは Bitbucket + Read The Docs という組み合わせをしておりますが、
    別に github + Read The Docs でもよいです。
    但し、この方法に関しては、このサイトでは説明をしないので注意してください。

Bitbucket にレポジトリを作成する
======================================

まずは Bitbucket に sphinx ドキュメントをまとめたレポジトリを作成します。

- Bitbucket の使い方に関しては以下のページが参考になるかと思います。
    - http://babyp.blog55.fc2.com/blog-entry-939.html

.. note:: レポジトリの権限に関して

   今回の場合、外部（Read The Docs）から作成したレポジトリをクローンする必要があるので
   公開レポジトリにする必要があります。

ディレクトリ構成に関してですが、私は簡単のため、
以下のように、直接レポジトリに登録しているディレクトリ直下に
"sphinx-quickstart" で作成されたファイル群を置いています。

- sphinx-root
    - conf.py
    - Makefile
    - index.rst
    - ...

Read The Docs のユーザー登録
=================================

Bitbucket にレポジトリの登録が済んだら
そのレポジトリを Read The Docs に読み込ませます。

まずは以下のリンクから Read The Docs のアカウントを登録しましょう。

- https://readthedocs.org

.. note:: 必要に応じてページ下部の "Change Language" から日本語に変更する事も可能です。

`プロフィール <https://readthedocs.org/accounts/edit/>`_ を設定します。
このページの内、 Connected Services を選択し、 Connect to Bitbucket をクリックします。
すると Bitbucket がリンクされます。

その後、 `ダッシュボード <https://readthedocs.org/dashboard/>`_ から Import a Project をクリックします。
すると Bitbucket 上で公開設定となっているレポジトリが表示されます。
