############################
Sphinx で 論文風に引用を行う
############################

- Last Change: 18-May-2014.

研究系の情報をまとめている際には論文の情報を記述することがあります.
このような場合 (あるいは人のサイトを参考に記事を書いている場合などにもですが),
論文風の引用を行いたい場合があります.

このページでは Sphinx で上記の要件を満たす方法についてメモして置きます.

方法
===========

引用を行う場合, rst 文章に対して以下のような記述を行います

.. code-block:: rst

   # 文章中
   Phonological constraints were first proposed to be characterized by weighted harmony functions in Harmonic Grammar [Ref:1]_ .
   
   # 文献をまとめる箇所

   .. [Ref:1] G. Legendre, Y. Miyata, and P. Smolensky. Harmonic grammar: A formal multi-level connectionist the- ory of linguistic well-formedness: Theoretical foundations. Technical Report 90-5, Institute of Cognitive Science, University of Colorado, 1990. 

- "Ref:1" の部分は任意の文字で構いません

