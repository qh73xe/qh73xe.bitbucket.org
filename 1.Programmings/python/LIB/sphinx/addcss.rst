##############################
sphinx で CSS の設定を追加する
##############################

Last Change: 07-Jun-2014.

目的    
========

sphinx で HTML を作成している場合, 見た目の調整には theme を使用します.
ただ, 既存の theme を微妙に変更したい場合もあるかと思います.
上記のような場合, 自分で theme を作成する方法もありますが,
行いたい変更が少量である場合, CSS を直接記述してしまいたい場合も多いかと思います.

このページでは, Sphinx Theme に独自の CSS / JS ファイルを読み込ませる方法について記述します.

- 参考 : http://sphinx-users.jp/reverse-dict/html/custom-css-js.html

手順
=============

1. CSS を用意する ( :command:`/_static/hogehoge.css` )
2. copy.py に以下の記述を行う

.. code-block:: python

   def setup(app):
     app.add_stylesheet('hogehoge.css')

- 同様に :command:`app_add_javascript('custom.js')` を使えばJSファイルも追加できます。


.. note:: 

   ここに記述しているやり方は Sphinx標準テーマを使用している場合の方法です.
   CSS のファイル名は任意のもので結構です


