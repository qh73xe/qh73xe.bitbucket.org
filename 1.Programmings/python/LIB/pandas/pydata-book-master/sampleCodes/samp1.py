#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
#
# Author:      qh73xe
# Last Change: 14-Mar-2014.
# ファイルの説明 : 
# 

# 通常のテキストファイルの読み込み
path = "../ch02/usagov_bitly_data2012-03-16-1331923249.txt"
open(path).readline()

# 上記データ ( JSON形式 ) を python で扱う
import json
recodes = [json.loads(line) for line in open(path)]
recodes[0]

# カラムを指定して情報を確認する
# ここでは, tz (タイムゾーンのデータ) を扱う
recodes[0]['tz']

# メモ: 出力部分の一番頭にある "u" は文字エンコーディングのこと
# この場合は unicode の略
# なお print 文を使用するとこの情報は出てこない
print recodes[0]['tz']

# 標準出力を使用したタイムゾーン情報の集計
time_zones = [rec['tz'] for rec in recodes]

# 上記コードはエラーになる
# この理由はすべての行にタイムゾーンが含まれていないから
# これを回避するためには以下のように記述する

time_zones = [rec['tz'] for rec in recodes if 'tz' in rec]
time_zones[:10]

## とりあえず 数え上げ を行う
def get_counts(sequence):
    count = {}
    for x in sequence:
        if x in counts:
            counts[x] += 2
        else:
           counts[x] = 1
    return counts

counts = get_counts(time_zones)
counts['America/New_York']
