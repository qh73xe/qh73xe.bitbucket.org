===================
Pandas Tips
===================

ここでは python pandas ライブラリを使用する際に便利な Tips についてメモをしてい
きます．

DataFrame から指定の条件のデータを抽出する
==========================================

| - Author : qh73xe
| - data : 2014年 11月  6日 木曜日 00:40:24 JST

例えば適当なデータを作成します．

.. code-block:: python

    >>> import pandas as pd
    >>> from random import randint
    >>> df = pd.DataFrame({'A': [randint(1, 9) for x in xrange(10)],
                       'B': [randint(1, 9)*10 for x in xrange(10)],
                       'C': [randint(1, 9)*100 for x in xrange(10)]})
    >>> df

このデータは以下のような形になるはずです．::

        A   B    C
    0   9  40  300
    1   9  70  700
    2   5  70  900
    3   8  80  900
    4   7  50  200
    5   9  30  900
    6   2  80  700
    7   2  80  400
    8   5  80  300
    9   7  70  800

このデータに対して例えば B が 50 より大きく， 
C が 900 の時の A のデータを検索するには以下のようにします

.. code-block:: python

    >>> df.loc[(df["B"] > 50) & (df["C"] == 900), "A"]
    2    5
    3    8


また上記の条件のデータのみを操作することも可能です．
例えば上記条件に合致する A のデータのみに 1000 倍したい場合以下のようにします．

.. code-block:: python

    >>> df.loc[(df["B"] > 50) & (df["C"] == 900), "A"] *= 1000
          A   B    C
    0     9  40  300
    1     9  70  700
    2  5000  70  900
    3  8000  80  900
    4     7  50  200
    5     9  30  900
    6     2  80  700
    7     2  80  400
    8     5  80  300
    9     7  70  800


- 参考：http://stackoverflow.com/questions/15315452/selecting-with-complex-criteria-from-pandas-dataframe

指定のカラムがユニークとなるデータを取得する
--------------------------------------------------

正規化していない CSV ファイルをいじっている場合，あるカラムをキーにユニークな
サブセットデータを取得したい場合があります．
このような場合，以下のようにします．

.. code-block :: python

    from pandas import DataFrame
    import numpy as np
    df = ({'N1': [1, 2, np.nan, 4],
           'N2': [6, 6, 4, np.nan],
           'S1': [np.nan, 'B', 'C', 'D']
           'S2': ['A', 'A', 'C', 'D']
           'pk' : [1, 1, 2, 3]
    })

    df = df.drop_duplicates(subset=['pk'])[
        [
            'N2',
            'S2',
        ]
    ]

データフレームにある文字列が含まれているもののみを取得する
----------------------------------------------------------------------------------------------------

データフレームのあるカラムが文字列のみ（Null もない）状態だとします。
その上でそのカラムにある語が含まれているデータのみを取得するには以下のように
記述します。

.. code-block :: python

    from pandas import DataFrame
    import numpy as np
    df = ({'N1': [1, 2, np.nan, 4],
           'S1': [np.nan, 'B', 'C', 'D']
           'S2': ['a', 'b', 'c', 'ab']
           'pk' : [1, 1, 2, 3]
    })

    df[df.S1.str.contains('a')]

条件で分けたカラムの追加
===========================

上記の例のように，あるカラムがある条件の際に別のカラムを変化させたい場合がありま
す（例えばある閾値でバイナリにしたい時など）．

R 言語に馴染みのある方でしたら :command:`ifelse 関数` を想定していただければ良い
かと思います．

このような場合，Pandas では以下のように numpy ライブラリの :command:`where 関数`
を使用して記述するのがシンプルだと思います．

.. code-block:: python

    >>> df = pd.DataFrame({'AAA' : [4,5,6,7], 'BBB' : [10, 20, 30, 40],'CCC' : [100, 50, -30, -50]})
    >>> df
       AAA  BBB  CCC
    0    4   10  100
    1    5   20   50
    2    6   30  -30
    3    7   40  -50

    >>> df['logic'] = np.where(df['AAA'] > 5,'high','low')
    >>> df
       AAA  BBB  CCC logic
    0    4   10  100   low
    1    5   20   50   low
    2    6   30  -30  high
    3    7   40  -50  high


データフレームの空のフィールドを削除する
====================================================================

pandas の データフレームの内，空のデータを削除したデータフレームを取得したい場合
はよくあることかと思います．

上記のような目的では :command:`DataFrame.dropna()` を使用します．
このコマンドはデフォルトでは一行でも空のフィールドがある場合，その列を削除したデ
ータフレームを返します．

一方，すべてのカラムが空の列のみを削除したい場合以下のように指定します．

.. code-block :: python

   from pandas import DataFrame
   import numpy as np
   df = ({'N1': [1, 2, np.nan, 4],
          'N2': [6, 5, 4, np.nan],
          'S1': [np.nan, 'B', 'C', 'D']
          'S2': [np.nan, np.nan, np.nan, np.nan]
   })
   df.dropna(how='all')

また，指定のカラムが空の列のみを削除したい場合には subset を指定します．

.. code-block :: python

   df.dropna(subset=['N2'])


