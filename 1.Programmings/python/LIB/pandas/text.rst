================================
pandas: テキスト処理
================================

| Last Change: 23-Sep-2015.
| author : qh73xe
|

padnas では文字列データを操作するための基本メゾットが組み込みで作成されています．
このページではこの文字列に関しての情報をまとめます．

まず文字列を操作する関数に関しては Series クラスについています．
これは DataFrame 型が複数のデータ型を持つテーブルであるのに対して， Series 型な
いでは基本的に一つのデータ型が想定されるためかと思われます．

サンプルデータ
==================

以下にこのページで使用するサンプルデータを示します．

.. code-block:: python

    s = Series(['A', 'B', 'C', 'Aaba', 'Baca', np.nan, 'CABA', 'dog', 'cat'])

簡単な例
===========

さて，文字列操作用のメゾットは str クラスに実装されています．
そのため，文字列操作用の メゾットにアクセスするには以下のようにします．

.. code-block:: python

   s.str.lower()  # Series ないのすべての文字列を小文字にする
   s.str.upper()  # すべてを大文字にする
   s.str.len()    # 文字列数を取得

Splitting と Replacing
==========================

分割と置換を行うためには以下のようにします．

.. code-block:: python

   s2 = Series(['a_b_c', 'c_d_e', np.nan, 'f_g_h'])
   s2.str.split('_')

冷静に考えて pandas を使用して split を行っているので結果は DataFrame の方がよい
場合の方が多いでしょう．それには以下のようにします．

.. code-block:: python

   s2.str.split('_').apply(Series)

分割を行った上で指定の行のみが欲しい場合， get メゾットを使用します．

.. code-block:: python

   s2.str.split('_').str.get(1)
   s2.str.split('_').str[1]     # あるいは普通にアクセスをしても構いません

置換を行う場合，replace を使用します．
これは正規表現を受け取ることが可能です．

.. code-block:: python

   s3 = Series(['A', 'B', 'C', 'Aaba', 'Baca', '', np.nan, 'CABA', 'dog', 'cat'])
   s3.str.replace('^.a|dog', 'XX-XX ', case=False)
