=============================================
pandas Tips
=============================================

この文章は python の Panda ライブラリの使い方に関する備忘録です.
このページでは pandas のデータアクセスに関する情報をまとめていきたいと思います．

基本的には以下のページの内容を抜粋，日本語訳していると思ってください．

- http://pandas.pydata.org/pandas-docs/stable/comparison_with_sql.html

とりあえず，pandas を使用するには以下のようにするのがおすすめです．

.. code-block:: python

   import pandas as pd
   import numpy as np

サンプルデータに関して
=========================

pandas では公式でサンプルデータをおいてありますのでこれを利用していきます．

.. code-block:: python

   url = 'https://raw.github.com/pydata/pandas/master/pandas/tests/data/tips.csv'
   tips = pd.read_csv(url)

このデータはあるレストランのボーイさんが受け取ったチップの金額のデータです．
テーブルには以下のカラムがあります．

- total_bill: 支払いの合計額
- tip: チップの額
- sex: 性別
- smoker: 喫煙者がいたか否か
- day: 何曜日か
- time: ディナーかランチか
- size: 何人組か

.. note:: read_csv に関して

   上のサンプルからでもわかる通り，
   ここで使用している read_csv 関数は https 通信が可能です．

SQL と対応してみる
=====================

SQL と対応して pandas の挙動を説明してみます．

SELECT
----------

SELECT 文として以下の SQL に相当する pandas のコマンドを示します．

.. code-block:: sql

   SELECT total_bill, tip, smoker, time
   FROM tips
   LIMIT 5;

pandas の DataFrame で SELECT に対応する記法は :command:`df[カラム名]` です．
ここでカラム名をリストにして渡すことが可能です．
具体的には以下の通りです．

.. code-block:: python

   tips[['total_bill', 'tip', 'smoker', 'time']].head(5)

上記コマンドの head 関数が LIMIT に相当します．

WHERE
-------------

続いて WHERE 文です．

まずは単純な SQL から説明します．

.. code-block:: sql

    SELECT * FROM tips
    WHERE time = 'Dinner'
    LIMIT 5;

上記の例は time が Dinner のデータのみを取得するSQLです.
pandas を利用する場合以下のようになります.

.. code-block:: python

    tips[tips['time'] == 'Dinner'].head(5)

ここでカギ括弧の中に注意をしてください．
カラム名 == 条件 ではなく， df['カラム名'] です．
より直感的な記法として以下のような例もあります．

.. code-block:: python

    is_dinner = tips['time'] == 'Dinner'
    is_dinner.value_counts()
    tips[is_dinner].head(5)

こちらの例では一行目で time を SELECT しています．
ここで is_dinner は bool 値になります．

さて，もう少し複雑な例を試してみます．
具体的には複数条件が必要な以下のSQLのような場合です．

.. code-block:: sql

   SELECT * FROM tips
   WHERE time = 'Dinner' AND tip > 5.00;

このように AND，OR は pandas では &, | で対応しています.
ただし，複数条件の場合にはそれぞれの条件を丸括弧で囲む必要があります.

.. code-block:: python

    tips[(tips['time'] == 'Dinner') & (tips['tip'] > 5.00)]

GROUP BY
----------

次は GROUP BY 句です．
以下の SQL では 性別ごとにデータ数をカウントしています．

.. code-block:: sql

    SELECT sex, count(*)
    FROM tips
    GROUP BY sex;

これは以下のようになります．

.. code-block:: python

    tips.groupby('sex').size()

.. note:: size と count に関して

   この例では count 関数ではなく， size 関数を使用しています．
   これは count 関数がすべての カラムのカウントを返す関数であるためです．
