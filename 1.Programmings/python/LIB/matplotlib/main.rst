================================
matplotlib: 可視化用ライブラリ
================================

| Last Change: 16-Nov-2015.
| author : qh73xe
|

設定ファイルの場所を確認する
=============================

matplotlib の一番面倒な部分は，
外部との連携をしないといけないということです．

で，そういった設定を確認するには以下のようにします．

.. code-block:: python

   import matplotlib
   matplotlib.matplotlib_fname()
