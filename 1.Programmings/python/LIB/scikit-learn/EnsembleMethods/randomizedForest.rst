================================
ランダムフォレスト
================================

| Last Change: 18-Sep-2015.
| author : qh73xe
|


このページでは scikit-learn でランダムフォレストの学習を行うための情報を記述します．
ランダムフォレストの最大の特徴は，その頑健で，柔軟な特徴量選択にあると思います．
また，この方法は特徴量選択に関して以下の2つの直接的な方法を提供します．

- mean decrease impurity
- decrease accuracy

このポストでは， scikit-learn に付属する ボストンデータを利用して
ランダムフォレストで学習を行い，各特徴量の影響を考察するサンプルを示します．

Mean decrease impurity
===========================

ランダムフォレストはいくつかの決定木を含んでいます．
決定木にあるすべてのノードは一つの特徴量の状態であり，データセットを2つに分離するように設計されています．
そのため，同様のデータ・セットからは最終的には似たような反応が出てきます．

ここで，impurity と呼ばれる基準にしたがって（局所的かもしれないけど）最適な状態を選択していきます．
識別の際には一般に Gini imuprity や エントロピーが使用され，回帰木のためには分散が使用されます．

決定木の学習の際には，ある木における重み付き impurity がそれぞれの特徴量をなくすごとにどれくらい減少するのかを計算しています．
で，これを組み合わせた森の学習ではそれぞれの 特徴量における impurity の減少を平均化し，その特徴量をランキングするわけです．

では早速，ランダムフォレストで学習した結果の impurity を見てみましょう．

.. code-block:: python

    from sklearn.datasets import load_boston
    from sklearn.ensemble import RandomForestRegressor
    import numpy as np
    boston = load_boston()  # サンプルデータの読み込み
    X = boston["data"]      # 学習に使用する特徴量
    Y = boston["target"]    # 教師情報

    # ランダムフォレストでの学習
    rf = RandomForestRegressor()
    rf.fit(X, Y)

    names = boston["feature_names"]  # それぞれの特徴量のヘッダーを取得
    print sorted(zip(map(lambda x: round(x, 4), rf.feature_importances_), names), reverse=True)


