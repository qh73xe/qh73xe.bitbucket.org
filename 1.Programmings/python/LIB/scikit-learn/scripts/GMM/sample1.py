#!/usr/bin/env python
# -*- coding: utf-8 -*
"""
| Last Change: 19-Nov-2014.
| author : qh73xe
"""
#=======================
# ライブラリの読み込み
#=======================
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from sklearn import datasets
from sklearn import mixture

#=======================
# データの読み込み
#=======================
iris = datasets.load_iris()  # サンプルデータ（iris）の読み込み
datas = iris.data[:, :2]  # とりあえず2次元のみにする
labels = iris.target       # 正解ラベル

#=======================
# 学習
#=======================
clf = mixture.GMM(n_components=2, covariance_type='full')
clf.fit(datas)

#=======================
# 結果の可視化
#=======================

# 学習結果を可視化
#------------------------------------
# 図中 X, y 軸の範囲を指定
x = np.linspace(min(datas[:, 0] - 1.0), max(datas[:, 0] + 1.0))
y = np.linspace(min(datas[:, 1] - 1.0), max(datas[:, 1] + 1.0))
X, Y = np.meshgrid(x, y)
XX = np.array([X.ravel(), Y.ravel()]).T

# 学習結果から密度を推定
Z = -clf.score_samples(XX)[0]
Z = Z.reshape(X.shape)
CS = plt.contour(X, Y, Z, norm=LogNorm(vmin=1.0, vmax=1000.0), levels=np.logspace(0, 3, 10))
CB = plt.colorbar(CS, shrink=0.8, extend='both')
# 正解データの可視化
px0 = datas[labels == 0, :]
px1 = datas[labels == 1, :]
px2 = datas[labels == 2, :]
plt.scatter(px0[:, 0], px0[:, 1])
plt.scatter(px1[:, 0], px1[:, 1], c='g')
plt.scatter(px2[:, 0], px2[:, 1], c='r')

# 図を表示
plt.show()
