#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
#
# Author:      qh73xe
# Last Change: 05-Jun-2014.
# ファイルの説明 : 
# GMM を使用しクラスタリンクを行うサンプルコードです.

# Plot, データ整形用のライブラリの読み込み
import pylab as pl
import matplotlib as mpl
import numpy as np

# サンプルデータライブラリの読み込み
from sklearn import datasets

# GMM関係のライブラリの読み込み
from sklearn import datasets
from sklearn.cross_validation import StratifiedKFold
from sklearn.externals.six.moves import xrange
from sklearn.mixture import GMM

# Plot用関数の定義
def make_ellipses(gmm, ax):
    for n, color in enumerate('rgb'):
        v, w = np.linalg.eigh(gmm._get_covars()[n][:2, :2])
        u = w[0] / np.linalg.norm(w[0])
        angle = np.arctan2(u[1], u[0])
        angle = 180 * angle / np.pi  # convert to degrees
        v *= 9
        ell = mpl.patches.Ellipse(gmm.means_[n, :2], v[0], v[1],
                                  180 + angle, color=color)
        ell.set_clip_box(ax.bbox)
        ell.set_alpha(0.5)
        ax.add_artist(ell)

# サンプルデータの読み込み(今回はIrisを使用)
iris = datasets.load_iris()
# テストデータと訓練データを作成(データを4分割し,最初の1/4をテスト用に,残りを訓練データにする)
skf = StratifiedKFold(iris.target, n_folds=4)
train_index, test_index = next(iter(skf))

X_train = iris.data[train_index]
y_train = iris.target[train_index]
X_test = iris.data[test_index]
y_test = iris.target[test_index]

n_classes = len(np.unique(y_train))

# 共分散のタイプの異なるGMMの実行
classifiers = dict((covar_type, GMM(n_components=n_classes,
                    covariance_type=covar_type, init_params='wc', n_iter=20))
                   for covar_type in ['spherical', 'diag', 'tied', 'full'])

n_classifiers = len(classifiers)

# 可視化のためのベースを作成
pl.figure(figsize=(3 * n_classifiers / 2, 6))
pl.subplots_adjust(bottom=.01, top=0.95, hspace=.15, wspace=.05, left=.01, right=.99)

# 可視化
for index, (name, classifier) in enumerate(classifiers.iteritems()):
    # 学習データのクラスラベルがあるため, GMM パラメータを教師ありに初期化
    classifier.means_ = np.array([X_train[y_train == i].mean(axis=0) for i in xrange(n_classes)])

    # EM algorithm を使用して他のパラメタを学習する
    classifier.fit(X_train)

    h = pl.subplot(2, n_classifiers / 2, index + 1)
    make_ellipses(classifier, h)

    for n, color in enumerate('rgb'):
        data = iris.data[iris.target == n]
        pl.scatter(data[:, 0], data[:, 1], 0.8, color=color, label=iris.target_names[n])
    # テストデータをクロスさせ可視化
    for n, color in enumerate('rgb'):
        data = X_test[y_test == n]
        pl.plot(data[:, 0], data[:, 1], 'x', color=color)

    y_train_pred = classifier.predict(X_train)
    train_accuracy = np.mean(y_train_pred.ravel() == y_train.ravel()) * 100
    pl.text(0.05, 0.9, 'Train accuracy: %.1f' % train_accuracy, transform=h.transAxes)

    y_test_pred = classifier.predict(X_test)
    test_accuracy = np.mean(y_test_pred.ravel() == y_test.ravel()) * 100
    pl.text(0.05, 0.8, 'Test accuracy: %.1f' % test_accuracy, transform=h.transAxes)

    pl.xticks(())
    pl.yticks(())
    pl.title(name)

pl.legend(loc='lower right', prop=dict(size=12))
pl.show()
