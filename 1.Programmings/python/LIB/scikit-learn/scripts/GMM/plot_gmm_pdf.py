#!/usr/bin/env python
# vim: set fileencoding=utf-8 :
#
# Author:      qh73xe
# Last Change: 05-Jun-2014.
# ファイルの説明 : 
# GMM を使用して密度推定を行うサンプルプログラムです.

# 必要なライブラリの読み込み
import numpy as np
import pylab as pl
from sklearn import mixture

# データ数の指定
n_samples = 300

# 2つのコンポーネントを持つ,ランダムデータを生成
np.random.seed(0)
C = np.array([[0., -0.7], [3.5, .7]])
X_train = np.r_[np.dot(np.random.randn(n_samples, 2), C),
                np.random.randn(n_samples, 2) + np.array([20, 20])]

# GMMによる学習
clf = mixture.GMM(n_components=2, covariance_type='full')
clf.fit(X_train)

# 可視化用の軸を作成
x = np.linspace(-20.0, 30.0)
y = np.linspace(-20.0, 40.0)
X, Y = np.meshgrid(x, y)
XX = np.c_[X.ravel(), Y.ravel()]
Z = np.log(-clf.score_samples(XX)[0])
Z = Z.reshape(X.shape)

# 可視化
CS = pl.contour(X, Y, Z)
CB = pl.colorbar(CS, shrink=0.8, extend='both')
pl.scatter(X_train[:, 0], X_train[:, 1], .8)

pl.axis('tight')
pl.show()
