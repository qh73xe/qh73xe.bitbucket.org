#!/usr/bin/env python
# vim: set fileencoding=utf-8 :

# ライブラリ の 呼び出し
import pylab as pl           # 可視化用ライブラリ
from sklearn import datasets # データ・セット

# Iris のデータを呼び出す
iris = datasets.load_iris()
X = iris.data[:, :2]  # 描画する特徴量は最初の二次元のみ
Y = iris.target       # 正解データ ( plot の色分けで使用 )

# plot の X, Y 軸を作成(上の Y とは違うので注意)
x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5

# 可視化のベースを作成
pl.figure(2, figsize=(8, 6))
pl.clf()

# 実際にプロット
pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Paired)
pl.xlabel('Sepal length')
pl.ylabel('Sepal width')

pl.xlim(x_min, x_max)
pl.ylim(y_min, y_max)
pl.xticks(())
pl.yticks(())
