######################################
pylean2 : Deep Learning の python 実装
######################################

.. note:: このページについて

   この記事は, 2014年7月10日 に開催された 
   `第6回AIツール入門講座 講座1 ( Deep Learning ) <http://www.ai-gakkai.or.jp/no06_jsai_tool_introductory_course/>`_ 
   の講習内容及び事前準備に関してのメモ書きになります.

   講習では AWS を利用して環境を共有したのですが,
   私の個人的な事情 (クレジットカードを持っていない...) により AWS を利用できなかったため,
   ローカルPC に pylean2 環境を構築し, 講習に行きます.

   このページには Pylean2 の導入と簡単な実行テストについてメモします.

   - 講習会の内容については以下のリンク先に記述しますのでご参照ください.

第6回AIツール入門講座 講座1 ( Deep Learning )
===============================================

.. toctree::
   :maxdepth: 2

   AI_Lecture_2014

pylean2 の導入方法
===============================================

- 環境
   - OS : Linux ( ubuntu )

- 参考資料
   - `Pylean2 <http://deeplearning.net/software/pylearn2/>`_
   - `今ナウいディープラーニングのライブラリ「Pylearn2」のインストールとチュートリアル <http://qiita.com/mizuki0420@github/items/3f9c97b32e8d6e37abad>`_

Download
-----------------------------------------------

pylean2 は git を使用しダウンロードします.
- pip には登録されていないようです.

.. code-block:: bash

   $ git clone git://github.com/lisa-lab/pylearn2.git

依存ライブラリの導入
---------------------------------------------

`公式サイト <http://deeplearning.net/software/pylearn2/>`_ によれば, pylean2 は以下のライブラリに依存しています.

- `Theano <http://deeplearning.net/software/theano/>`_
- PyYAML
- PIL
- scikit-learn

私の環境では以下のコマンドでこれらのものを導入しました.

.. code-block:: bash

   $ sudo apt-get update
   $ sudo apt-get install build-essential
   $ sudo apt-get install libamd2.2.0 libblas3gf
   $ sudo apt-get install libc6 libgcc1 libgfortran3 liblapack3gf libumfpack5.4.0 libstdc++6
   $ sudo apt-get install gfortran libatlas-dev python2.7-dev
   $ sudo apt-get install python-setuptools git-core

   $ sudo apt-get install libyaml-dev python-yaml

   $ sudo easy_install pip
   $ sudo pip install numpy scipy
   $ sudo pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git
   $ sudo pip install matplotlib scikit-learn
   $ sudo pip install pandas

pylean2 のインストール
--------------------------------------------------

pylean2 は自分で独自にビルドします.
上記のように私の場合 sudo 権限を持っていますので,以下のコマンドを使用します.

.. code-block:: bash

   $ sudo python setup.py develop

以下に成功した際の出力をメモしておきます.

.. code-block:: bash

   running develop
   running egg_info
   creating pylearn2.egg-info
   writing requirements to pylearn2.egg-info/requires.txt
   writing pylearn2.egg-info/PKG-INFO
   writing top-level names to pylearn2.egg-info/top_level.txt
   writing dependency_links to pylearn2.egg-info/dependency_links.txt
   writing manifest file 'pylearn2.egg-info/SOURCES.txt'
   reading manifest file 'pylearn2.egg-info/SOURCES.txt'
   reading manifest template 'MANIFEST.in'
   writing manifest file 'pylearn2.egg-info/SOURCES.txt'
   running build_ext
   cythoning pylearn2/utils/_window_flip.pyx to pylearn2/utils/_window_flip.c
   building 'pylearn2.utils._window_flip' extension
   creating build
   creating build/temp.linux-x86_64-2.7
   creating build/temp.linux-x86_64-2.7/pylearn2
   creating build/temp.linux-x86_64-2.7/pylearn2/utils
   x86_64-linux-gnu-gcc -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -fPIC -I/usr/lib/python2.7/dist-packages/numpy/core/include -I/usr/include/python2.7 -c pylearn2/utils/_window_flip.c -o build/temp.linux-x86_64-2.7/pylearn2/utils/_window_flip.o
   In file included from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/ndarraytypes.h:1761:0,
                    from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/ndarrayobject.h:17,
                    from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/arrayobject.h:4,
                    from pylearn2/utils/_window_flip.c:352:
   /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/npy_1_7_deprecated_api.h:15:2: warning: #warning "Using deprecated NumPy API, disable it by " "#defining NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION" [-Wcpp]
    #warning "Using deprecated NumPy API, disable it by " \
     ^
   In file included from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/ndarrayobject.h:26:0,
                    from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/arrayobject.h:4,
                    from pylearn2/utils/_window_flip.c:352:
   /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/__multiarray_api.h:1629:1: warning: ‘_import_array’ defined but not used [-Wunused-function]
    _import_array(void)
    ^
   In file included from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/ufuncobject.h:327:0,
                    from pylearn2/utils/_window_flip.c:353:
   /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/__ufunc_api.h:241:1: warning: ‘_import_umath’ defined but not used [-Wunused-function]
    _import_umath(void)
    ^
   x86_64-linux-gnu-gcc -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security build/temp.linux-x86_64-2.7/pylearn2/utils/_window_flip.o -o /home/qh73xe/bin/pylearn2/pylearn2/utils/_window_flip.so
   cythoning pylearn2/utils/_video.pyx to pylearn2/utils/_video.c
   building 'pylearn2.utils._video' extension
   x86_64-linux-gnu-gcc -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -fPIC -I/usr/lib/python2.7/dist-packages/numpy/core/include -I/usr/include/python2.7 -c pylearn2/utils/_video.c -o build/temp.linux-x86_64-2.7/pylearn2/utils/_video.o
   In file included from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/ndarraytypes.h:1761:0,
                    from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/ndarrayobject.h:17,
                    from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/arrayobject.h:4,
                    from pylearn2/utils/_video.c:352:
   /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/npy_1_7_deprecated_api.h:15:2: warning: #warning "Using deprecated NumPy API, disable it by " "#defining NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION" [-Wcpp]
    #warning "Using deprecated NumPy API, disable it by " \
     ^
   In file included from /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/ufuncobject.h:327:0,
                    from pylearn2/utils/_video.c:353:
   /usr/lib/python2.7/dist-packages/numpy/core/include/numpy/__ufunc_api.h:241:1: warning: ‘_import_umath’ defined but not used [-Wunused-function]
    _import_umath(void)
    ^
   x86_64-linux-gnu-gcc -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security build/temp.linux-x86_64-2.7/pylearn2/utils/_video.o -o /home/qh73xe/bin/pylearn2/pylearn2/utils/_video.so
   Creating /usr/local/lib/python2.7/dist-packages/pylearn2.egg-link (link to .)
   Adding pylearn2 0.1dev to easy-install.pth file
   
   Installed /home/qh73xe/bin/pylearn2
   Processing dependencies for pylearn2==0.1dev
   Searching for pyyaml
   Reading https://pypi.python.org/simple/pyyaml/
   Best match: PyYAML 3.11
   Downloading https://pypi.python.org/packages/source/P/PyYAML/PyYAML-3.11.zip#md5=89cbc92cda979042533b640b76e6e055
   Processing PyYAML-3.11.zip
   Writing /tmp/easy_install-eHSuaG/PyYAML-3.11/setup.cfg
   Running PyYAML-3.11/setup.py -q bdist_egg --dist-dir /tmp/easy_install-eHSuaG/PyYAML-3.11/egg-dist-tmp-ffW3VR
   build/temp.linux-x86_64-2.7/check_libyaml.c:2:18: fatal error: yaml.h: そのようなファイルやディレクトリはありません
    #include <yaml.h>
                     ^
   compilation terminated.
   
   libyaml is not found or a compiler error: forcing --without-libyaml
   (if libyaml is installed correctly, you may need to
    specify the option --include-dirs or uncomment and
    modify the parameter include_dirs in setup.cfg)
   zip_safe flag not set; analyzing archive contents...
   Adding PyYAML 3.11 to easy-install.pth file
   
   Installed /usr/local/lib/python2.7/dist-packages/PyYAML-3.11-py2.7-linux-x86_64.egg
   Searching for theano
   Reading https://pypi.python.org/simple/theano/
   Best match: Theano 0.6.0
   Downloading https://pypi.python.org/packages/source/T/Theano/Theano-0.6.0.zip#md5=0a2211b250c358809014adb945dd0ba7
   Processing Theano-0.6.0.zip
   Writing /tmp/easy_install-zVvRCS/Theano-0.6.0/setup.cfg
   Running Theano-0.6.0/setup.py -q bdist_egg --dist-dir /tmp/easy_install-zVvRCS/Theano-0.6.0/egg-dist-tmp-gp7hGD
   warning: manifest_maker: MANIFEST.in, line 7: 'recursive-include' expects <dir> <pattern1> <pattern2> ...
   
   zip_safe flag not set; analyzing archive contents...
   theano.scan_module.scan_perform_ext: module references __path__
   theano.tensor.nnet.sigm: module references __file__
   theano.tensor.tests.test_opt: module references __file__
   theano.tensor.tests.test_mpi: module references __file__
   theano.misc.check_blas: module references __file__
   theano.gof.cmodule: module references __file__
   theano.gof.lazylinker_c: module references __path__
   theano.sandbox.test_rng_mrg: module references __file__
   theano.sandbox.gpuarray.basic_ops: module references __file__
   theano.sandbox.cuda.nvcc_compiler: module references __file__
   theano.sandbox.cuda.blas: module references __file__
   theano.sandbox.cuda.type: module references __file__
   theano.sandbox.cuda.__init__: module references __file__
   theano.tests.main: module references __file__
   theano.tests.run_tests_in_batch: module references __path__
   Adding Theano 0.6.0 to easy-install.pth file
   Installing theano-cache script to /usr/local/bin
   Installing theano-test script to /usr/local/bin
   Installing theano-nose script to /usr/local/bin
   
   Installed /usr/local/lib/python2.7/dist-packages/Theano-0.6.0-py2.7.egg
   Searching for argparse==1.2.1
   Best match: argparse 1.2.1
   Adding argparse 1.2.1 to easy-install.pth file
   
   Using /usr/lib/python2.7
   Searching for numpy==1.8.1
   Best match: numpy 1.8.1
   Adding numpy 1.8.1 to easy-install.pth file
   
   Using /usr/lib/python2.7/dist-packages
   Searching for scipy==0.13.3
   Best match: scipy 0.13.3
   scipy 0.13.3 is already the active version in easy-install.pth
   
   Using /usr/lib/python2.7/dist-packages
   Finished processing dependencies for pylearn2==0.1dev

実行テスト
===========================================

とりあえず ipython コマンドを使って,

.. code-block:: python

   import pylearn2

と行い,何も問題なければよいでしょう.

簡単なチュートリアル
-------------------------------

チュートリアルを行うためには少し設定が必要です.
以下のコマンドを端末に入力します.

.. code-block:: bash

   export PYLEARN2_DATA_PATH=<path/to/workingDirectory>
   export PYLEARN2_VIEWER_COMMAND="eog --new-instance"

上のコマンドはどこを指定しても構いませんが, SUDO 権限のいらない場所を指定しましょう.

Step 1: データ・セットを用意する
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

以下のコマンドを入力すると 先ほど, PYLEARN2_DATA_PATH に指定したディレクトリ内にサンプルデータを保存します.

.. code-block:: bash

   $ cd <path/to/clone/pylearn2/Directory>/pylearn2/scripts/tutorials/grbm_smd/
   $ python make_dataset.py

.. warning:: "IOError" が出る場合

   上記のスクリプトを使用した際にエラーが出る場合があります．
   これは，上で指定した :command:`export PYLEARN2_DATA_PATH` にテストに使用するデータが存在しないことが原因です．
   テスト用データを入手するには以下のスクリプトを実行します．

   .. code-block:: bash

      $ /<path/to/clone/pylearn2/Directory>/pylearn2/scripts/datasets/download_cifar10.sh

Step 2: モデルの訓練
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

pylearn2/scripts にテスト用の python 実行ファイルが入っています.
そのため, とりあえず, この場所に パスを通します.

.. code-block:: bash

    export PATH=$PATH:<path/to/clone/pylearn2/Directory>/pylearn2/scripts

その上で pylearn2/pylearn2/scripts/tutorials/grbm_smd/ に移動し,以下のコマンドを入力します.

.. code-block:: bash

   $ train.py cifar_grbm_smd.yaml

するとカチャカチャ動きます

Step 3: モデルの可視化
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

訓練が終了したモデルを可視化するには以下のコマンドを入力します.

.. code-block:: bash

   $ show_weights.py cifar_grbm_smd.pkl
   $ plot_monitor.py cifar_grbm_smd.pkl

すると以下のような図ができるかと思います.

.. figure:: fig/show.png
   :scale: 150%

   show_weights.py の実行結果

.. figure:: fig/plot.png
   :scale: 40%

   plot_monitor.py の実行結果

