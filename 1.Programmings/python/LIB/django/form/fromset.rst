================================
フォームセット
================================

| Last Change: 15-Aug-2015.
| author : qh73xe
|

.. contents::
    :depth: 2

Django を使用して from を作成する場合, 2つの方法があります.
一つは form class を自作する方法で, もうひとつは モデルフォームセットを使用する方法です.

form class は自分で記述するため,柔軟性に飛んでおり,使用方法もシンプルですが,
例えば, 既存のモデルにとりあえずデータを追記するロジックを記述しようとする場合など,
単純なフォームを作成するのに, これを一々設定するのはかったるいです.

こういう時に使えるのが モデルフォームセットです.

.. code-block:: python

    >>> from django.forms.models import modelformset_factory
    >>> BookForm = modelform_factory(Book, fields=("author", "title"))
    >>> formset = BookForm()

上記の例のように使用すると
該当のモデルのフォームを作成してくれます.

.. note:: fields に関して

    私は Django 1.8 を使用していますが, このヴァージョンでは fileds の指定が必須になっています.
    面倒くさい場合には '__all__' とするとよいです.

これは関数であることから, View で使用するようにしていますが,
どこに記述するのかが決まっているわけではなさそうです.

Template
------------------------------------

この関数が返すのは formset です.
これは Template で少し注意が必要です.

.. code-block:: html

    <form method="POST" action="">
        {{ formset.management_form }}
        <table>
            {% for form in formset.forms %}
            {{ form }}
            {% endfor %}
        </table>
    </form>

シンプルに form 要素のみを記述していますが,
form の中に {{ formset.management_form }} という記述があります.
これがないと, view での挙動がうまく行かないので注意です.

