================================
Up load file を処理する
================================

| Last Change: 13-Aug-2015.
| author : qh73xe
|

このページでは Django の数あるフォームのうち, FileField に関するメモを取ります.
このフィールドは, ユーザーにファイルをアップロードしてもらい, それをゴニョゴニョ
するためのフィールドです.

フィールド
------------------

とりあえず, このフィールドを含むフォームを作成します.

.. code-block:: python
   :caption: forms.py
   :name: forms_py

   from django import forms

   class UploadFileForm(forms.Form):
       title = forms.CharField(max_length=50)
       file  = forms.FileField()

単純なフォームです.
このフォームからの入力を扱うビューは、ファイルデータを request.FILES で受け取ります.
request.FILES は ファイルデータの入った辞書で、
辞書のキーはフォームクラス中の FileField の名前です.
上の例では, request.FILES['file'] でファイルデータにアクセスできます.

テンプレート
---------------

このフォームを渡す テンプレートに関しては少し注意が必要です.
Django では通常, form である以上テンプレート内でも <form> 要素で囲む必要があります.
ただし, FileField を持つ form は enctype="multipart/form-data" という属性を付与しないといけません.

.. code-block:: html
   :caption: sample.html
   :name: template

   <form enctype="multipart/form-data" action="/hoge/" method="POST">{% csrf_token %}
       {% for form in csv_forms%}
       {{form.label}}
       {{form}}
       {% endfor %}
       <button type="submit" class="btn btn-default">Submit</button>
   </form>

.. note::

   ここでは html の <form> 要素部分だけ記述しています.
   - action には適当に,この結果を送る url を記述しておいてください

アップロードされたファイルをメモリ上で処理する
-------------------------------------------------

request.FILES['file'] は UploadedFile というクラスです.
このファイルの中身を見たい場合には UploadedFile.read() という関数を使用するのが便利です.

.. warning:: read 関数に関して

    アップロードされたファイルが巨大だと,
    メモリに読み込む際にシステムの容量を越してしまう可能性があります.
    これを前提にする場合 chunks() を使用するべきです.

ファイルの中身を確認できます.
ここで注意しないと行けないのは python3 系を使用している場合です.
ここでの文字は byte 型として処理されるので, csv ライブラリとか使用していると泣き
たくなりますし, UploadedFile.read().decode('utf-8') をしておかないと, split 関数
とかも使用できません.

- というか, csv ライブラリ, 誰かなんとかしてほしい
- つーか pandas を使ってしまうのも手かも.


