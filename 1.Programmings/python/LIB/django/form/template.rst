================================
Template Tips
================================

:Last Change: 26-Apr-2016.
:Author : qh73xe
:Reference: https://docs.djangoproject.com/ja/1.9/topics/forms/

このページでは Django の form を Template 上で使用する際に便利な記法に関してメモしていきます。

.. contents:: 目次



.. warning:: 前提

    基本的に Django では、
    views.py 内でフォームクラスをインスタンス化し、
    Template に context として渡すものだと思います。

    1. この辺の基本事項はこのページでは詳しく説明することはないです。

    ここでは、 form という名前で適当な form を Template に渡していることを前提とします。

Template 上の表示を手動で設定する
============================================================

Template 上でとりあえず form を表示したい場合、以下のような記述が最低限だと思います。

.. code-block:: html
   :linenos:
   :caption: form.html
   :name: base

   <form method="post">
     {% csrf_token %}
     {{form}}
     <input type="submit" value="Vote" />
   </form>

或いは、 {{ form.as_table }} とか、 {{ form.as_p }} とかを利用することもあるかと思います。
しかし、個人的には Template の作成を面倒くさがって Bootstrap とか、その他 CSS のテンプレートを利用することが多いです。
この様な場合、 CSS テンプレートの表記に従ってマニュアルで form を設定したいことが多いです。

基本的には以下のような感じで Template に記述していくと不満がないかと思います。

.. code-block:: html
   :linenos:
   :caption: form.html
   :name: manual

   <form method="post">
     {% csrf_token %}
     {{ form.non_field_errors }}
     <div class="fieldWrapper">
         {{ form.subject.errors }}
         <label for="{{ form.subject.id_for_label }}">Email subject:</label>
         {{ form.subject }}
     </div>
     <div class="fieldWrapper">
         {{ form.message.errors }}
         <label for="{{ form.message.id_for_label }}">Your message:</label>
         {{ form.message }}
     </div>
     <div class="fieldWrapper">
         {{ form.sender.errors }}
         <label for="{{ form.sender.id_for_label }}">Your email address:</label>
         {{ form.sender }}
     </div>
     <div class="fieldWrapper">
         {{ form.cc_myself.errors }}
         <label for="{{ form.cc_myself.id_for_label }}">CC yourself?</label>
         {{ form.cc_myself }}
     </div>
     <input type="submit" value="Vote" />
   </form>

Form 内容を for 文で回す
================================

上記の方法では、Template 内での表記の自由度はかなり高いですが、
抽象化しきれているとは言い難いです。

つまり、 Form の内容が異なる場合にわざわざそれ用のテンプレートを書き直す必要が出てきます。

- 私個人的には Django を使用していて一番無駄な時間を費すのか テンプレートの作成なので、これを減らしたいです

このような場合に便利なのが、 Template 内で For 文を使用する方法です。
具体的には以下のように記述します。

.. code-block:: html
   :linenos:
   :caption: form.html
   :name: for

   <form method="post">
     {% csrf_token %}
     {{ form.non_field_errors }}
     {% for field in form %}
       <div class="fieldWrapper">
           {{ field.errors }}
           {{ field.label_tag }}
           {{ field }}
           {% if field.help_text %}
             <p class="help">{{ field.help_text|safe }}</p>
           {% endif %}
       </div>
     {% endfor %}
     <input type="submit" value="Vote" />
   </form>

From 用の Template を再利用する
=====================================

先にも述べた様にできる限り template を記述する回数は減らしたいものです。
そのうえで、ある web app を作成するときのことを考えると、 Form 用の HTML の種類はそれなりに限定されているものかと思います。

このような場合、いくつかの form 用のテンプレートを作成し、
これを {% include %} で呼び出すと便利です.

まず以下のような form-template を作成します。

.. code-block:: html
   :linenos:
   :caption: form_snippet.html
   :name: snippet

   <form method="post">
     {% csrf_token %}
     {{ form.non_field_errors }}
     {% for field in form %}
       <div class="fieldWrapper">
           {{ field.errors }}
           {{ field.label_tag }}
           {{ field }}
           {% if field.help_text %}
             <p class="help">{{ field.help_text|safe }}</p>
           {% endif %}
       </div>
     {% endfor %}
     <input type="submit" value="Vote" />
   </form>

これを呼び出す場合、以下のようにします。

.. code-block:: html
   :linenos:
   :caption: template.html
   :name: snippet-usege

   {% include "form_snippet.html" with form=comment_form %}

ここで with を使用することで 特定の form (この場合 comment_form という名前です) を
form として form_snippet.html に渡すことができます。
