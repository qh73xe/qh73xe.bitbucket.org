================================
Django: コマンドメモ
================================

| Last Change: 15-Jan-2016.
| author : qh73xe

Django を使用していると :file:`manege.py` や :file:`django-admin.py` を使用することが多くあると思います.
これは多機能で便利なスクリプトですが,つい使い方を忘れることが多いので簡単な備忘録をとっておきます.

.. contents:: 目次

django-admin.py
====================

プロジェクトの開始
-------------------

.. code-block :: bash

    django-admin.py startproject hogehoge

バージョン確認
-----------------

.. code-block :: bash

    django-admin.py --version


manege.py
=================

app の作成
-------------------

.. code-block :: bash

    $ python manage.py startapp hogehoge


モデルの更新
-------------------

.. code-block :: bash
    :caption: django 1.7 以上
    :name: django 1.7 以上

    $ python manage.py makemigrations polls
    $ python manage.py sqlmigrate polls 0001
    $ python manage.py migrate



管理者の作成
-------------------

.. code-block :: bash

    $ python manage.py createsuperuser

データベースのクリア
--------------------------------------

.. code-block :: bash

    $ python manage.py flush


