================================
ER 図の作成
================================

| Last Change: 05-Nov-2015.
| author : qh73xe

Django 製のアプリケーションを作成する際，特にドキュメンテーションの段階では
裏で動いている RDB の構造に関して可視化したい
場合があるかと思います．

.. contents:: 目次

- なお，私は以下の環境で試しています．
    - OS : OpenSUSE 13.2
    - Django : 1.7
    - python : 2.7


このような場合，以下のライブラリを導入すると
簡単に ER 図が作成できます．

.. code-block :: bash

    $ sudo apt-get install python-django-extensions graphviz python-pygraphviz

- この内，graphviz に関しては OS の領分かと思いましたので今回は apt-get を使用しています
    - 適宜 yum や zypper に読み替えてください

続いて django に python-extensions を読み込ませます．

settings.py::

    INSTALLED_APPS = (
        ...
        'django_extensions',
    )


後は以下のコマンドでER図が作成されます．


.. code-block :: python

   $ python manage.py graph_models -a -g -o er.png
