
m .settings import *
 
DEBUG = False
ALLOWED_HOSTS = ["chibiegg-201505.sakura.ne.jp"]
 
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'chibiegg-201505_myprj',
        'USER': 'chibiegg-201505',
        'PASSWORD': 'PASSWORDISHERE',
        'HOST': 'mysql508.db.sakura.ne.jp',
        'OPTIONS': {
               "init_command": "SET storage_engine=InnoDB",
        }
    }
}
 
STATIC_URL = '/static/'
STATIC_ROOT = '/home/chibiegg-201505/www/chibiegg-201505.sakura.ne.jp/htdocs/static/'
