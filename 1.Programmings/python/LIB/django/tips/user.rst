================================================================
django でユーザーモデルを操作する
================================================================

:Last Change: 11-Apr-2016.
:Author : qh73xe
:Reference: https://docs.djangoproject.com/ja/1.9/topics/auth/customizing/

.. contents:: 目次

ユーザーモデルの拡張
==============================

Django でアプリケーションを作成している際に結構便利なのが、ユーザー周りの操作を
自動で作成しておいてくれることです。

一方で自動化されているものだけではなく、もっと自由にユーザーモデルを定義したい場合もあるかと思います

Django において ユーザーモデルに登録したい情報を増やしたい場合, もっとも単純な方法は
ユーザーモデルと onetoone の関係をもったモデルを定義してしまうことです。

以下の例では何か社員を登録する様なモデルを作成している場合に部署情報を追加したいという状況設定でモデルを作成しています。

.. code-block:: python
    :linenos:
    :caption: model.py
    :name: model_extention

   from django.contrib.auth.models import User

   class Employee(models.Model):
       user = models.OneToOneField(User, on_delete=models.CASCADE)
       department = models.CharField(max_length=100)

このようなモデルを生成したうえで以下のように使っていきます。

.. code-block:: python
   :linenos:
   :caption: to use
   :name: to use

   >>> u = User.objects.get(username='fsmith')
   >>> freds_department = u.employee.department

大抵の問題はこのように解決を行うことが可能かと思います。

Admin に登録する
==============================

上記のようにユーザーモデルを拡張した場合、直接 admin からすべて登録できた方が楽かと思います。
以下のような admin.py を作成すると良いでしょう。

.. code-block:: python

   from django.contrib import admin
   from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
   from django.contrib.auth.models import User
   from my_user_profile_app.models import Employee


   class EmployeeInline(admin.StackedInline):
       model = Employee
       can_delete = False
       verbose_name_plural = 'employee'

   # Define a new User admin
   class UserAdmin(BaseUserAdmin):
       inlines = (EmployeeInline, )

   # Re-register UserAdmin
   admin.site.unregister(User)
   admin.site.register(User, UserAdmin)


view 内で ユーザーモデルを利用する
============================================================

現在のログインユーザーを取得する
-----------------------------------

ユーザー情報をサイト内に表示する際には基本的に現在のユーザーの情報のみを使用する
ことが多いかと思います。

上記のような場合、ユーザー情報は以下のように取得します。

.. warning:: 当然、ユーザーログインが修了していることを前提とします

.. code-block:: python
   :linenos:
   :caption: views.py
   :name: get current user

   def sample_view(request):
       current_user = request.user
       print current_user.id
