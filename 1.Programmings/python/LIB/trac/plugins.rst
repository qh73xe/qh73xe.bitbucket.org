================================
Trac: plugins
================================

| Last Change: 17-Oct-2015.
| author : qh73xe
|

このページでは python/trac のプラグインに関する情報をまとめます.
trac は本体自身はなるべくシンプルに設計されていて, それに対して種々のプラグインを導入することで機能を拡張していきます.

TicketCalendarPlugin
=========================

- http://blog.tracpath.com/trac/trac-0-12/trac-%E3%83%97%E3%83%A9%E3%82%B0%E3%82%A4%E3%83%B3ticketcalendarplugin-%E3%81%AE%E8%AA%AC%E6%98%8E%E3%81%A8%E4%BD%BF%E3%81%84%E6%96%B9/



Advanced Ticket Workflow Plugin
==================================

- https://trac-hacks.org/wiki/AdvancedTicketWorkflowPlugin

Workflow Editor Plugin
----------------------------

- http://szk-takanori.hatenablog.com/entry/20090211/p1

TracpathTheme
-------------------

http://blog.tracpath.com/trac/trac-plugin-use-tracpaththeme/
