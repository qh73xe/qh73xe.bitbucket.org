================================
導入方法
================================

| Last Change: 15-Jan-2016.
| Last Change: 15-Jan-2016.
| author : qh73xe
|

このページには trac を本番サーバーでなんとか動作させるところまでを記述したいと思います.

.. contents:: 目次

対象環境
=============================

- 本番環境: Debian 7
    - sudo なし
- python: pyenv, virtualenv を使用
- バージョン管理システム: git
- db: sqlite3
- appache を使用
    - ただし cgi のみが利用可能

結構, 詰んでる感じの環境です.
sudo が使用できませんので apt-get は使用できません.
ついでに, chwon コマンドも使えないですし, appache の設定もいじれません.

こういう状態でも動くのが python ライブラリの良い所です.

下準備
========================

とりあえず, pyenv, pyenv-vertualenv を用意します.
これはホーム以下に仮想の python 環境を構築することで,
sudo のないユーザーでも python のライブラリを自由に導入することが可能になる環境です.

- pyenv, pyenv-virtualenv の設定に関してはここでは省略します

.. code-block:: bash

   pyenv virtualenv 2.7.9 trac  # 新規 virtualenv 環境 trac を構築
   mkdir trac  # trac の実態を置くディレクトリを作成
   cd trac
   pyenv local trac
   pip install -U pip
   pip install -U trac
   trac-admin myTrac initenv

最後のコマンドを打ち込むとなんか色々と聞かれます.
とりあえず, プロジェクト名を入力すると,
次に使用する DB を聞かれます.
ここでは何も入力せずにエンターを押せばよいです.

- この場合: Sqlite3 が選択されます
    - mysql より準備が簡単で私は好きです

これで, trac を導入する下準備ができました.

Trac 設定
=================

続いては trac の設定を行っていきます.
具体的には git の管理が行えるようにしたいと思います.

- trac ではデフォルトで svn を使用します.

上の手順をそのまま実行した場合,
trac/myTrac 以下に色々なファイルが作成されていると思います.
このうち基本的な設定ファイルは myTrac/conf/trac.ini になります.
これを以下のように編集します.

.. code-block:: bash
   :caption: myTrac/conf/trac.ini
   :name: trac_ini

   # 変更
   [trac]
   repository_dir = /home/hoge/foo/git/  # 管理したい git レポジトリの入っているディレクトリ
   repository_sync_per_request =
   repository_type = git

   # 以下追加
   [git]
   cached_repository = false
   persistent_cache = false
   shortrev_len = 6
   git_bin = /usr/bin/git

   [components]
   tracopt.versioncontrol.git.* = enabled

このファイルは trac が読んだり書いたりするので読み書き権限を与えておきます.

.. code-block:: bash

   $ chmod 777 -R myTrac

- 多くのサイトでは chown で www-data の所有にしてしまって対応していますが, sudo 必要なので
  - うちのサーバ 755 じゃダメだった(設定が悪い)
  - 実際問題として本当は危険かなとも思う

cgi スクリプトの準備
=========================

次に cgi スクリプトを準備します.
これは :command:`trac-admin` を使用すれば自動で作成してくれます.

.. code-block:: bash

   mkdir ~/public_html
   trac-admin myTrac deploy ~/public_html/trac

このコマンドを入力すると ~/public_html/trac 以下にいくつかのファイルやディレクトリが作成されます.
このうち cgi-bin/trac.cgi を今回は使用します(うちのサーバこれしか対応してないので).
trac.cgi に実行権限を渡します.

.. code-block:: bash

   $ chmod +X ~/public_html/trac/cgi-bin/trac.cgi

出来上がった trac.cgi を公開用のディレクトリ(例えば ~/public_html 等) に移動すれば,
Trac を使用することができると思います.

Trac の日本語化
===========================

ここで作成された Trac を確認すると, おそらくはすべて英語であると思います.
これを日本語化するには以下の作業を行います.

まず, 公式サイトより, trac のソースファイルを入手します.

- 公式サイト: http://trac.edgewall.org/wiki/TracDownload
- 例えば, 現状の stable 版を入手するには以下の通りです.

  .. code-block:: bash

     $ svn co http://svn.edgewall.org/repos/trac/branches/1.0-stable trac

で, ソースファイルの場所に移動し以下のコマンドを実行してください.

- このページの場合 pyenv を利用しているので, これが効く場所にソースファイルを置くとよいです.

.. code-block:: bash

   $ python setup.py extract_messages
   $ python setup.py update_catalog
   $ python setup.py update_catalog -l nl
   $ python setup.py check_catalog -f
   $ python setup.py compile_catalog -f
   $ python setup.py install

これで日本語化がなされたと思います.

- 公式ドキュメント: http://trac.edgewall.org/wiki/TracL10N
