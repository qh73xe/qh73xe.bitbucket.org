================================
Trac: 設定Tips
================================

| Last Change: 15-Jan-2016.
| author : qh73xe
|

.. contents:: 目次


一つの Trac インスタンスで複数のレポジトリを管理する
=======================================================

Trac では基本的に一つのインスタンスで複数の git レポジトリを管理することが可能です.
デフォルトで使用するレポジトリ以外を登録するには conf/trac.ini に repositories
という項目を追加します.

.. code-block:: ini
   :caption: conf/trac.ini
   :name: repositories1

    [repositories]
    hoge.dir = /var/trac/git/hoge.git
    hoge.type = git
    foo.dir = /var/trac/git/
    foo.type = git

シンタックスハイライトを行う
====================================

trac のシンタックスハイライトは Pygments を使用します．
そのため， wiki でシンタックスハイライトを行いたい場合，
まずは Pygments をインストールする必要があります．

.. code-block:: bash

   $ pip install Pygments

その上で設定ファイルを変更します．

.. code-block:: ini
   :caption: conf/trac.ini
   :name: repositories

   [mimeviewer]
   pygments_default_style = colorful

Trac で対応しているシンタックスハイライトは以下のページで確認できます．

- http://trac.edgewall.org/wiki/TracSyntaxColoring#SyntaxColoringSupport

reStructuredText を使用する
================================

Trac では reStructuredText(rst: sphinx で使用する記法) が使用できます.
これは wiki の記法に比べ, 表現力が高く便利な瞬間があります.

Trac で rst 記法を使用するためには,
前提条件として, docutils が必要です.
これは python 製のライブラリであるため, pip 等でインストール可能です.

.. code-block:: bash

   $ pip install docutils


rst 形式を使用する際には以下の構文の中に rst 表記を使用します.

.. code-block:: bash

   {{{
   #!rst
   これ以下に rst 形式を記述
   }}}

trac の表記を rst 形式の中で使用できるようにすることも可能です.
詳しくは以下のページを参照してください.

- http://trac.edgewall.org/wiki/WikiRestructuredText
