#################################
watchdog : ファイル監視ライブラリ
#################################

これは何?
=========

watchdog は ある任意のディレクトリの任意のパターンの名前を持つファイルの
イベントを監視し, 特定のイベントが発生した場合に, 任意の作業を行うようにする
モジュールです.

スクリプトでも使用することは可能ですが, 端末上でコマンドを打ち込み使用することが多いです.

使用用途としては, 例えば, :doc:`sphinx<../sphinx/main>` を使用する場合に,
rst ファイルを編集した段階で, "make html" を走らせたり, git で管理しているディレクトリのファイルに変更があった場合に, 自動でadd, commit を行ったりすることが可能です.

導入方法
========

.. code-block:: bash

   $ pip install watdog

使い方
======

ここではとりあえず任意のディレクトリを端末上から監視するための
コマンドを記述しておきます.

.. code-block:: bash

   $ watchmedo shell-command --patterns="*.rst" --recursive --wait --command="make html"

- なお, このコマンドは, 実行され続けるため, screen や tmux で複数のshell を建てた上で行うか, & で流す必要があります.


オプションの説明
================

- --patterns : 監視するファイル名のパターン
- --command  : イベントにマッチした場合の処理
    - なお, このオプションでは以下の変数を使用できます
    - ${watch_src_path}   : event source path; 
    - ${watch_dest_path}  : event destination path (for moved events); 
    - ${watch_event_type} : event type;
    - ${watch_object}     : ``file`` or ``directory`` 
- --ignore-pattern : イベントを無視するファイル名のパターン
- --ignore-directories : イベントを無視するディレクトリ (default: False)
- --recursive : 再帰的にディレクトリを監視する (default: False)
- --wait : 複数のインスタンスが同時に避けることを終了するまで待つ (default: False)

