================================
ggplot: 描画作図
================================

| Last Change: 19-May-2015.
| author : qh73xe

この記事は python 版の ggplot を紹介する記事です．
そうです．R 界隈での必需品，ggplot2 は python でもあるよというお話です．

導入
============

とりしま，pip から導入可能です．
しかし， cairocffi に依存しているのと pandas をベースにしている関係で，
そこら辺を入れなくてはいけません．とくに cairocffi は pip が勝手に入れてくれなか
ったので．


基本的な使い方
=================

R 版とほぼ同じです．
ライブラリをインポートして，ggplot 関数にデータと軸を渡して，
geom_<hogeoge> で何を書くのかを指定します．

.. code-block:: python
   :caption: sample1.py
   :name: sample1.py

    from ggplot import *

    ggplot(aes(x='date', y='beef'), data=meat) +\
        geom_line() +\
        stat_smooth(colour='blue', span=0.2)

以上．


