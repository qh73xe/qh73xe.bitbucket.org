================================
pyside: 実戦
================================

| Last Change: 15-Mar-2016.
| author : qh73xe
|

このページはとりあえず pyside を使用して GUI アプリケーションを作成する手順を説明したものになります。
そのため、このページでは pyside の背景に関する記述はあまりしません。

Qt Designer を使う
=====================

とりあえず pyside を使用する環境は整っていることとします。
で、 pyside を利用するには、基本的にどのようなクラスを作成し、
どのように ウィジェット を配置するのかを理解する必要があります。

でも、pyside で使用できるウィジェットをすべて把握しクラスを組み立てていくことは、
日曜スクリプトとして気軽に GUI を作成したい身としては割と面倒です。
色々な人に怒られてしまいそうな気もしますが、とりあえず python で気軽に GUI を作
成したいという場合 Qt Designer というアプリケーションを利用するのがよいでしょう。

このアプリケーションは GUI で作成したいアプリケーションのデザインを作ることが可能です。
起動してみて少し触ってみれば、お好みの GUI を作成できると思います。

起動すると以下のダイアログが表示されます。

.. image:: http://2.bp.blogspot.com/-gcxmoaZEuKs/Tzpb3J7lfeI/AAAAAAAAAAg/MWiok1XKTgQ/s320/first_pyside_01.png

ここでは GUI のテンプレートを選択します。

テンプレートを選択すると以下のような画面に移行します。

.. image:: http://1.bp.blogspot.com/-FlsnEGSD6fA/TzpgRd2Y8VI/AAAAAAAAAAs/5A0hovIa8Bw/s1600/first_pyside_02.png

ここで少し注意ですが、
作成した GUI の objectName を適当な名前に変更してください。
この名前が後で自動生成するコードのクラス名に反映されるので、ここは設定しないと後で困ります。

あとは好きなように GUI を作成してください。
見た目上必要なものができたらファイルを保存します。
保存する名前は任意のもので結構です。

すると .ui という名前のファイルが作成されると思います。
これはまだ、 python コードではないですが、以下のコマンドで python コードに変換されます。

.. code-block:: bash

   $ pyside-uic -o 出力先ファイル uiファイル

ここで出力される python コードは GUI 用のクラスのみが記述されています。
そのためこのファイルを直接実行しても何も起きません。
クラスをインスタンス化するロジックを記述する必要があります。

インスタンスロジックは生成された python スクリプトに直接記述してもよいのですが、
ここでは、別途実行用スクリプトを記述することにします。

以下の例では ui_test.py という名前で ui ファイルを python ファイルに出力したものとして記述します。
また、 作成した GUI の objectName の名前は Test としたことにします。

.. code-block:: python

   # -*- coding: utf-8 -*-
   import sys
   from PySide import QtGui
   from ui_test import Ui_Test

   class Test(QtGui.QWidget):
       def __init__(self, parent=None):
           super(Test, self).__init__(parent)
           self.ui = Ui_Test
           self.ui.setupUi(self)
 
   if __name__ == '__main__':
       app = QtGui.QApplication(sys.argv)
       dlg = Test()
       dlg.show()
       sys.exit(app.exec_())

いくつか注意点を指摘します。
まず、 GUI 用の python スクリプトを import する都合上、
基本的にこの実行スクリプトは ui_test.py と同じディレクトリに配置する必要があります。
また、 import 文ですが、これは Qt Designer で指定した objectName の冒頭に Ui_ が付いた形になります。

あとは上記のスクリプトを実行すれば先程作成した GUI が起動するのがわかります。

シグナルを作成する
=====================

上記のスクリプトだけでは、ボタンを押しても何も起きません。
そのため、各種ボタンにシグナルをつけます。
この作業は要は、各種のボタンがクリックされた時に、ある関数を実行するという構造を記述するものです。

一番単純なシグナルの追加方法は 先程作成した Ui_test.py を利用することです。
このスクリプトは大体以下の様な構成になっています。

.. code-block:: python

   from PySide import QtCore, QtGui
   from script_dics import SPEEKER_DIC, EMOTION_DIC, SCRIPT_DIC, STRENGTH_DIC


   class Ui_test(object):

       def setupUi(self, corpura_tool):
           test.setObjectName("test")
           ...
           self.pushButton = QtGui.QPushButton(self.verticalLayoutWidget)
           self.pushButton.setObjectName("pushButton")

       def retranslateUi(self, corpura_tool):
           test.setWindowTitle(QtGui.QApplication.translate("test", "test", None, QtGui.QApplication.UnicodeUTF8))
           ...

Qt Designer で自動作成されたスクリプトでは GUI を setupUi 関数で定義し、
それぞれの ウィジェット の内容を retranslateUi 関数内で定義しています。
ここでは Ui_test クラスの中に pushButton という名前でボタンが設定されていることがわかります。
このボタンにシグナルを設定するには setupUi 内に以下の記述を追記します。

.. code-block:: python

    class Ui_test(object):
       def setupUi(self, corpura_tool):
           ...
           self.pushButton.clicked.connect(self.hellow)
       ...
       def hellow(self):
           print('Hellow')

このようにある関数と結びつけたいウィジェットに対し、
clicked.connect() 関数を呼び出します。
これは対象のボタンがクリックされたときに引数で受け取った関数を実行します.

あとはその際に実行する関数の実体を記述すれば GUI アプリケーションの完成です。

.. note:: 各種ウィジェットの値を受け取る

   基本的に GUI アプリケーションでは例えばラジオボタンで設定を行い、pushButton をクリックすると何かを実行することが多いかと思います。
   そのため簡単に各種 ウィジェットの値を取得する方法を記述します。

   - チェック系: ラジオボタン, チェックボタン
       - 関数: isChecked
       - ex: self.radio.isChecked(), self.check.isChecked()
   - テキスト系: テキスト入力
       - 関数: text()
       - ex: self.lineEdit.text()
   - スピンボックス
       - 関数: value
       - ex: self.spin.value()
   - コンボボックス
       - 関数: currentIndex(), currentText()
       - ex: self.combo.currentIndex(), self.combo.currentText()
