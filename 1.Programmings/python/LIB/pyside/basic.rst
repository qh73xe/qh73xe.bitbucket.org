================================
pyside : 基本的な使い方
================================

| Last Change: 15-Mar-2016.
| author : qh73xe

このページには基本的な pyside の使用方法に関してメモしておきます．

.. contents:: 目次
   :depth:2

とりあえず使ってみる
==========================

pyside の hello world です．

.. literalinclude:: scripts/test.py

Pyside でアプリケーションを作成する場合，とりあえず２つのライブラリを読み込みます．

* PySide.QtCore : Pyside 内部のウィジェットの操作
* PySide.QtGui : シグナルとスロットの処理，アプリケーション制御

Qt のメインアプリケーションは app = QApplication(sys.argv) の部分で読み込んでい
ます．ここでは Qt にコマンドライン引数をすべて渡せるようにしています．
とりあえず，ここはオマジナイ的なものです．

アプリケーションのウェジェットとして QLabel を使用しています．
これはテキストと画像を表示するウェジェットです．

.. note:: QLabel に関して

   QLabel 関数は HTML タグを引数に受け取ることが可能です．
   例えば上記のスクリプトの記述を以下のように変更することができます．::

       label = QLabel('<font color=blue>Hello World</font>')

ウィジェットを指定したあとには show メゾットを使用します．
これで label を表示することになります．

最後に app.exec_() とすることで app を実行しています．

ウィジェットについて
========================

私自身は基本的に GUI アプリケーションを作成することを職業にしているわけではない
ので正直，どういうものがあるのかイマイチ勘が働かない状態です．
とりあえず wikipedia に書かれているものが使えれば後で色々考えればよいかなと思っています．

- http://ja.wikipedia.org/wiki/ウィジェット_(GUI)

選択
-------------------

* ボタン : QtGui.QPushButton()
* チェックボックス : QtGui.QCheckBox()
* ラジオボタン : QtGui.QRadioButton()
* スライダー : QtGui.QSlider()

誘導
-------------------

テキスト入力
--------------------

* テキストボックス : QLineEdit()
* コンボボックス : QtGui.QComboBox()
    - リストの追加 : addItems メゾットを使用

出力
--------------------

ウィンドウ
--------------------


exe ファイル化
=======================

windows ユーザー用にスクリプトを組んでいる際には，
相手のPC に python が入っているとは限りません．
というか，一般にwindows ユーザーは端末操作に慣れていない場合が多いのでできれば，exe ファイル化してスクリプトをわたしてしまいたい場合が多いかと思います．

このような場合だと :command:`cxfreeze` を使用するのが便利です．

例えば作成したスクリプトが :file:`app.py` であった場合以下のコマンドでexe化できます．

.. code-block:: bash

    $ cxfreeze app.py --target-dir dist --base-name Win32GUI --include-modules atexit

* --target-dir : 出力先のディレクトリ名
* --include-modules atexit : pyside で使用するモジュールを埋め込む

.. note:: cx_Freez の導入方法

    上記のコマンドは cx_Freez ライブラリを導入する必要があります．
    これは :command:`pip` コマンドから導入可能です．

    まだ，windows 環境で上記のコマンドを使用しようとすると，何もしない状態では実行ファイルとして扱ってくれません．
    この問題を解決するためには以下の作業を行う必要があります．
    この作業はコマンドプロンプト上で行います．

    + :file:`C:\Python27\Scripts` に移動
        * python のバーションに合わせてください.
    + :file:`cxfreez-postinstall` を実行
        * :command:`python cxfreez-posrinstall` です
