#!/usr/bin/env python
# -*- coding: utf-8 -*
"""
| Last Change: 25-Nov-2014.
| author : qh73xe
"""

import sys
from PySide.QtCore import *
from PySide.QtGui import *
 
 
# Create a Qt application
app = QApplication(sys.argv)
# Create a Label and show it
label = QLabel("Hello World")
label.show()
# Enter Qt application main loop
app.exec_()
sys.exit()
