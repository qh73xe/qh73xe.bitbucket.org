import sys
from PySide.QtCore import QUrl
from PySide.QtGui import QApplication
from PySide.QtWebKit import QWebView

app = QApplication(sys.argv)
web_view = QWebView()
web_view.load(QUrl('https://srinikom.github.io/pyside-docs/PySide/QtWebKit/QWebView.html'))
web_view.show()
app.exec_()
sys.exit()
