#!/usr/bin/env python
# -*- coding: utf-8 -*
"""
| Last Change: 25-Nov-2014.
| author : qh73xe
"""

import sys
from PySide.QtCore import *
from PySide.QtGui import *


# ボタンを押した際の挙動を記述
def sayHello():
    print 'Hello World!'

# Create a Qt application
app = QApplication(sys.argv)
button = QPushButton("Click me")
button.clicked.connect(sayHello)
button.show()
# Enter Qt application main loop
app.exec_()
