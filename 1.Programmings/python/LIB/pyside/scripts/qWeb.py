import sys
from PySide.QtGui import QApplication
from PySide.QtWebKit import QWebView

html = '''<html>
<head>
<title>A Sample Page</title>
</head>
<body>
<h1>Hello, World!</h1>
<hr />
I have nothing to say.
</body>
</html>'''

app = QApplication(sys.argv)
web_view = QWebView()
web_view.setHtml(html)
web_view.show()
app.exec_()
sys.exit()
