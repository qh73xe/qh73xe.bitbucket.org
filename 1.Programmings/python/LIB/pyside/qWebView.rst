================================
QWebView: html を読み込む
================================

:Last Change: 27-Mar-2016.
:Author: qh73xe
:Reference: `PySide/PyQt Tutorial: QWebView <http://pythoncentral.io/pyside-pyqt-tutorial-qwebview/>`_

QWebView は url から web ページを読み込み作成している GUI アプリケーションに埋め込むことをできるようにします。
web ブラウザエンジンには WebKit を使用しています。

.. contents::

HTML を直書きする
=====================

取り敢えず、最初の実行例を示します。
この例では html を直書きし、それを pyside の GUI アプリケーションで表示します。

.. literalinclude:: ./scripts/qWeb.py
   :language: python
   :caption: qwebview.py
   :emphasize-lines: 3,17,18
   :name: samp1
   :linenos:


以下簡単にこの例の説明を行います。

まず、上記例で強調を行っていない部分に関してですが、これは pyside をアプリケーションとして起動するために必要な作業です。
この部分は pyside を使用したことがある場合、とくに説明はいらないと思います。
詳しくは以下のページを参考にすれば意味は分かると思います。

- https://wiki.qt.io/Hello-World-in-PySide-Japanese

本題の強調カ所ですが、
この例ではまず QWebView クラスをインスタンス化しています。

その上で setHtml() を使用し HTML を読み込んでいます。
この例では簡単のため、 HTML はスクリプト内で直接書き込んでいます。

このスクリプトの実行に成功すると以下のような GUI が立ち上がります。

.. image:: ./figs/qweb_samp1.png
   :scale: 60%

URL からサイトを読み込む
===============================

上の例では html を直接コード内に書き込んでいました。
しかし、通常はそういうことはせず、どこかの web ページを読み込んで使用したいことが多いかと思います。

.. literalinclude:: ./scripts/qWeb2.py
   :language: python
   :caption: qwebview2.py
   :name: samp2
   :linenos:
   :emphasize-lines: 2, 8

この例では、 pyside の QWebView モジュールの説明が書いてあるページを読み込んでいます。
ここで注意する必要があるのは URL に関しては直接 str を渡してはいけないということです。
一度 QUrl オブジェクトに変換し、それを渡します。

結果は以下のようになります。

.. image:: ./figs/qweb_samp2.png
   :scale: 60%

その他のファイルを読み込む
===============================

QWebView で読み込むことが可能なものは html だけではないです。
例えば PNG ファイルのようなファイルも読み込むことが可能です。

.. code-block:: python
   :linenos:
   :caption: qWeb3.py
   :name: samp3
   :emphasize-lines: 6-7

   import sys
   from PySide.QtGui import QApplication
   from PySide.QtWebKit import QWebView

   app = QApplication(sys.argv)
   web_view = QWebView()
   img = open('myImage.png', 'rb').read()
   web_view.setContent(img, 'image/png')

   web_view.show()
   app.exec_()
   sys.exit()
