=========================================================
pip-tool : pip で管理しているライブラリの管理を楽にする
=========================================================

.. warning::

   このページは pip コマンドに関する説明ではなく, pip コマンドを楽にする pip-tool というライブラリの説明です.
   pip コマンド自体については:doc:`python/conf` に書かれています.

.. contents:: 目次
   :depth: 2

python を使用している場合, ライブラリの管理には大抵(最低限私は) pip を使用することになるかと思います.
さて, この pip コマンド, そのままではライブラリのアップデートを一括でこなすのがやや面倒です.

そのため, pip tool というライブラリを導入すると幸せになれます.

.. code-block:: bash 

   $ sudo pip install pip-tools

使い方
====================

上記のライブラリの使用の仕方は以下の 3つ を抑えておけばよいでしょう.

pip レポジトリの確認
----------------------

pip-review コマンドを端末上で使用すると更新の可能なライブラリが出力されます.
このコマンドはあくまでも確認を行うのみであるため, sudo は使用する必要がありません.


.. code-block:: bash

   $ pip-review
   Django==1.6.1 is available (you have 1.5.4)
   Mako==0.9.0 is available (you have 0.8.1)
   MarkupSafe==0.18 is available (you have 0.15)
   PAM==0.1.4 is available (you have 0.4.2)
   ...


pip レポジトリの更新
----------------------

pip-review で確認したライブラリを一括で更新するには
-- auto というオプションを追加します.
これはライブラリの更新であるため, 基本的には(pipで管理しているディレクトリが/homeより上位である場合には), sudo が必要です.

.. code-block:: bash

   $ sudo pip-review --auto

インタラクティブな更新
------------------------

--interactive というオプションを追加するとライブラリごとに更新を行うか否かを指定できます.

.. code-block:: bash

   $ sudo pip-review --interactive
   Django==1.6.1 is available (you have 1.5.4)
   Upgrade now? [Y]es, [N]o, [A]ll, [Q]uit 
