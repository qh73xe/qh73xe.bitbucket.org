================================
StatsModels : 統計モデル
================================

| Last Change: 20-Oct-2015.
| author : qh73xe



これは何？
================================

StatsModels は、さまざまな統計モデルの推定だけでなく、統計的検定を実施し、
統計データ探索するためのクラスと関数を提供する Python ライブラリです。

使用感は R に似ています．
同じく機械学習用のライブラリとして， scikit-learn がありますが，こちらはあくまで
も機械学習用ライブラリで， StatsModels は統計モデル用のライブラリです．

- 公式ページ: http://statsmodels.sourceforge.net/http://statsmodels.sourceforge.net/

.. note:: 機械学習と統計モデル

    この両者は実用上はとても似ています．
    しかし，これらの技術が発展してきた文脈，つまりこれらの技術が目指している対象
    は大きく異なります．

    機械学習は，とりあえず未知のデータを予測することが重要です．
    そのため，こちらの文脈で重要視されるのはモデルの予測精度そのものです．
    一方，統計モデルと言った場合，重要なのは観測データの裏に潜んでいるルールを
    解き明かすことです．

    − ココらへんのコンセントの違いは以下の本が参考になるかと思います
        − データサイエンス講義 : http://www.oreilly.co.jp/books/9784873117010/

導入方法
================================

pip から導入します．

.. code-block :: bash

   $ sudo pip install -U statsmodels

.. note:: openSUSE を使用している場合

    opensuse を使用している場合，python-develがないというエラーがでる場合があります．
    しかし，sudo zypper in python-devel としても最新版があると言われます．
    このような場合は python のレポジトリを登録した上で，zypper から導入したほうが早いです

    .. code-block :: bash

       $ sudo zypper in python-statsmodels

簡単な使い方
================================

以下に StatsModels の簡単な使い方例を示します．

.. code-block :: python

    In [1]: import numpy as np
    In [2]: import statsmodels.api as sm 
    In [3]: import statsmodels.formula.api as smf
    In [4]: dat = sm.datasets.get_rdataset("Guerry", "HistData").data
    In [5]: results = smf.ols('Lottery ~ Literacy + np.log(Pop1831)', data=dat).fit()
    In [6]: print results.summary()

                            OLS Regression Results
    ==============================================================================
    Dep.Variable:                Lottery   R-squared:                       0.348
    Model:                           OLS   Adj. R-squared:                  0.333
    Method:                 Least Squares  F-statistic:                     22.20
    Date:                Tue, 09 Dec 2014  Prob (F-statistic):           1.90e-08
    Time:                        12:33:15   Log-Likelihood:               -379.82
    No. Observations:                  86   AIC:                            765.6
    Df Residuals:                      83   BIC:                            773.0
    Df Model:                           2
    Covariance Type:            nonrobust
    ===================================================================================
                          coef    std err          t      P>|t|      [95.0% Conf. Int.]
    -----------------------------------------------------------------------------------
    Intercept         246.4341     35.233      6.995      0.000      176.358   316.510
    Literacy           -0.4889      0.128     -3.832      0.000      -0.743     -0.235
    np.log(Pop1831)   -31.3114      5.977     -5.239      0.000      -43.199   -19.424
    ================================================================================
    Omnibus:                        3.713   Durbin-Watson:                   2.019
    Prob(Omnibus):                  0.156   Jarque-Bera (JB):                3.394
    Skew:                          -0.487   Prob(JB):                         0.18
    3Kurtosis:                      3.003   Cond. No.                         702.
    ================================================================================

Contents
================================

.. toctree::
   :glob:
   :maxdepth: 1

   *
