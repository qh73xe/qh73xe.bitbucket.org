================================
StatsModels : Tips
================================

| Last Change: 10-Dec-2014.
| author : qh73xe

.. contents:: これは目次です
   :depth: 2

出力フォーマット
====================

StatsModels では出力のフォーマットを変更することが可能です．

- R をご存知のかたは xtable パッケージを想定していただければよいかと思います．

これには :command:`statsmodels.iolib` 以下のクラスを呼び出します．

例えば 統計モデルのサマリーを Latex に変更する場合は以下のようにします．

.. code-block :: python

    In [1]: import numpy as np
    In [2]: import statsmodels.api as sm 
    In [3]: import statsmodels.formula.api as smf
    In [4]: from statsmodels.iolib.summary import Summary

    # テスト用にモデルを作成
    In [5]: dat = sm.datasets.get_rdataset("Guerry", "HistData").data
    In [7]: results = smf.ols('Lottery ~ Literacy + np.log(Pop1831)', data=dat).fit()

    # 出力を Latex にする
    In [8]:  Summary.as_latex(results.summary())

.. note:: サポートしている形式について

   as 関数でサポートしている形式は以下の通りです．

   - Summary.as_latex
   - Summary.as_text
   - Summary.as_csv
   - Summary.as_html

summary ではなくデータそのものを table 化することも可能です．

.. code-block :: python

    In [1]: from statsmodels.iolib.table import SimpleTable
    In [2]: mydata = [[11,12],[21,22]]  # data MUST be 2-dimensional
    In [3]: myheaders = [ "Column 1", "Column 2" ]
    In [4]: mystubs = [ "Row 1", "Row 2" ]
    In [5]: tbl = SimpleTable(mydata, myheaders, mystubs, title="Title")
    In [6]: print( tbl )
    
             Title
    =======================
          Column 1 Column 2
    -----------------------
    Row 1    11       12
    Row 2    21       22
    -----------------------
    
    In [5]: print( tbl.as_html() )
    
    <table class="simpletable">
    <caption>Title</caption>
    <tr>
        <td></td>    <th>Column 1</th> <th>Column 2</th>
    </tr>
    <tr>
        <th>Row 1</th>    <td>11</td>       <td>12</td>
    </tr>
    <tr>
      <th>Row 2</th>    <td>21</td>       <td>22</td>
    </tr>
    </table>
