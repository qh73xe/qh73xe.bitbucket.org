#!/usr/bin/env python
# -*- coding: utf-8 -*
"""
| Last Change: 09-Jan-2015.
| author : qh73xe
"""

import time

start = time.time()  # start time
num = 10000
out = 0

for n1 in range(1, num + 1):
    for n2 in range(1, num + 1):
        out += 1

print '[info] ' + str(out)

end = time.time()  # end time
print '[info] TIME: ' + str(end - start) + ' seconds\n'
