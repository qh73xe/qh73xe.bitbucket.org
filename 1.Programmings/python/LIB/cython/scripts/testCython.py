#!/usr/bin/env python
# -*- coding: utf-8 -*
"""
| Last Change: 09-Jan-2015.
| author : qh73xe
"""
import time
import testPYX

start = time.time()
testPYX.for_test()
end = time.time()

print '[info] TIME: ' + str(end - start) + ' seconds\n'
