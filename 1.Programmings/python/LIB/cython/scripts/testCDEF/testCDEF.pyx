def for_test_cdef():
    cdef int num, n1, n2, out
    num = 10000
    out = 0

    for n1 in range(1, num + 1):
        for n2 in range(1, num + 1):
            out += 1

    print '[info] ' + str(out)
