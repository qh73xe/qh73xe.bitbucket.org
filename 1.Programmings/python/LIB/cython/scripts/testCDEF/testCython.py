#!/usr/bin/env python
# -*- coding: utf-8 -*
"""
| Last Change: 09-Jan-2015.
| author : qh73xe
"""
import time
import testCDEF

start = time.time()
testCDEF.for_test_cdef()
end = time.time()

print '[info] TIME: ' + str(end - start) + ' seconds\n'
