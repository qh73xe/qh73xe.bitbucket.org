#!/usr/bin/env python
# -*- coding: utf-8 -*
"""
| Last Change: 09-Jan-2015.
| author : qh73xe
"""
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension('testCDEF', ['testCDEF.pyx'])]
setup(
    name='TestCDEF app',
    cmdclass={'build_ext': build_ext},
    ext_modules=ext_modules)
