================================
word2vec 
================================

| Last Change: 04-Dec-2014.
| author : qh73xe

導入方法
=========

実行テスト
============

使用例
=========

とりあえず, wikipedia を使用してなんかやって見ます.

データの入手.

.. code-block:: bash

   $ wget http://dumps.wikimedia.org/jawiki/latest/jawiki-latest-pages-articles.xml.bz2
   $ bunzip2 jawiki-latest-pages-articles.xml.bz2

   $ cat jawiki-latest-pages-articles.xml-* > jawiki.txt # データの整形
