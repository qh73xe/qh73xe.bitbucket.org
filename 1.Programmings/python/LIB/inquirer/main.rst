================================
inquirer: 対話的コマンドライン
================================

:Last Change: 07-May-2016.
:Author: qh73xe
:Reference: https://pypi.python.org/pypi/inquirer

.. contents:: 目次
    :depth: 
    :local:

これは何？
===============================

inquirer は一般的によく使う対話的コマンドラインインターフェースのコレクションです。
もともとは javaScript のライブラリに同名のものがあり、これをベースに python で実装したものになります。

個人的には GUI よりも CUI が好きな人間なのですが、
CUI って別にコマンドラインだけで全てを片付ける必要はないんですよねと、
テキストブラウザや、テキストファイラーを使っていて思います。

それこそ、 apt や dnf だって イエスノー の質問をしますし、
git とか、普通に vim が開いたりします。
そも vim だって、普通に対話的に使っているわけで、
CUI だからって、コマンドラインしかつかえないのはおかしな話です。

- うん、結局端末操作がしたいだけで、GUIつくるのとか起動するのがタルいだけって説もあります。
    - 本格的にこったものを作りたい場合 標準ライブラリの curses を使うとよいので
      すが、結構タルいです。

ともかく、結構お洒落な CUI アプリを作るための基本セットになります。

導入
=====================================

導入は pip から行けます。

.. code-block:: bash
   :linenos:

   $ sudo pip install inquirer

使用例
====================================

とりあえず公式に上がっている使用例を示しておきます。

.. code-block:: python
   :linenos:
   :caption: text.py
   :name: text-py

   # -*- coding: utf-8 -*-
   import inquirer
   import re

   questions = [
       inquirer.Text('name', message="What's your name"),
       inquirer.Text('surname', message="What's your surname"),
       inquirer.Text('phone', message="What's your phone number", validate=lambda x, _: re.match('\d+', x),)
   ]
   answers = inquirer.prompt(questions)

.. code-block:: python
   :linenos:
   :caption: list.py
   :name: list-py

   # -*- coding: utf-8 -*-
   from pprint import pprint
   import inquirer

   questions = [
       inquirer.List(
           'size',
           message="What size do you need?",
           choices=['Jumbo', 'Large', 'Standard', 'Medium', 'Small', 'Micro'],
       ),
   ]
   answers = inquirer.prompt(questions)
   pprint(answers)

.. code-block:: python
   :linenos:
   :caption: choice.py
   :name: choice-py

   # -*- coding: utf-8 -*-
   from pprint import pprint
   import inquirer

   questions = [
       inquirer.Checkbox(
           'interests',
           message="What are you interested in?",
           choices=['Computers', 'Books', 'Science', 'Nature', 'Fantasy', 'History'],
       ),
   ]

   answers = inquirer.prompt(questions)
   pprint(answers)
