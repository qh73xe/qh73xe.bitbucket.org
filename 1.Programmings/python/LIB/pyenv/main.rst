================================
pyenv: python 用仮想環境
================================

| Last Change: 20-Oct-2015.
| author : qh73xe
|

.. contents::
    :depth: 2

このページでは python 仮想環境 pyenv について記述します．
このツールは簡単に言ってしまえば，各ユーザーのホーム以下に python の実行環境を作成することで，
SUDO 権限のないユーザーでも簡単に python ライブラリを使用することが可能です．
また，python 自身のバージョンを管理するのにも便利です．


導入方法
================

導入は git から行うのが現実的だと思います．
ここでは virtualenv も一緒に導入しておきます．

- ここでは，サーバーでの利用を考えているので基本は Linux かなと

.. code-block:: bash

    $ git clone git://github.com/yyuu/pyenv.git ~/.pyenv
    $ git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv

git での導入ですので，実行ファイルに対してパスを貼る必要があります．

これは .bash_profile とか， .zshenv とかお使いの shell に合わせて設定を行えばよいかと

- まあ，普通は bash を利用していると思いますが

.. code-block:: bash
   :caption: .bash_profile
   :name: bash_profile_for_pyenv

   export PYENV_ROOT="$HOME/.pyenv"
   export PATH="$PYENV_ROOT/bin:$PATH"
   export TMPDIR="$HOME/tmp"
   export PYTHON_PATH=./
   eval "$(pyenv init -)"
   eval "$(pyenv virtualenv-init -)"

.. note:: git の hook とかで利用する場合

    上記の設定では ssh を使用してサーバーにログインした場合にのみ，pyenv 環境が使用可能になる設定です.
    しかし，場合によっては git で サーバーのレポジトリに push をしたときに pyenv 環境を利用したい場合も多いかと思います．
    例えば， Django 等の web アプリを自動デプロイメントしたい時とか， sphinx でドキュメントをビルドしたいときとかですね.
    こういう場合には上記のコマンドを hook/post-update 等に記述すると都合がよいかと思います.

python をインストールする
================================

pyenv は python 自身を含む仮想環境です(ちなみにライブラリのみの管理であれば pip install hogehoge --user が便利です)．
そのため，まずはじめに OS がデフォルトで管理をしている python とは異なる，ユーザー独自の python を導入する必要があります．

以下のコマンドを実行します．

.. code-block:: bash

   $ pyenv install 3.4.3
   $ pyenv install 2.7.9

~/.pyenv/versions/ にそれぞれ 3 系の python と 2 系の python が作成されます．
インストールしてきた後はリフレッシュをしておくと安全です．

.. code-block:: bash

   $ pyenv rehash

アプリケーション環境を作成する
================================

後は，使用したい環境によって自由にすればいいです．
基本的には何かのプロジェクト単位で pyenv の環境を構築していきます．

例えば Djnago (python で web アプリを作成する際の大御所ライブラリ) のプロジェクトを作成する場合には以下の通りです．

.. code-block:: bash

    $ pyenv virtualenv 2.7.9 django-example
    $ pyenv activate django-example
    $ pip install django

ざっくりコードを説明すると，まず一行目では，
python 2.7.9 の環境を利用した django-example という virtualenv 環境を作成します．
ここで virtualenv 環境とはライブラリを管理する単位であると思っておいてくれればよいかと思います．
ここでは django を利用するとしたので django-example としていますが，
この名前は ** 任意 ** のもので構いません.

二行目では先ほど作成した virtualenv 環境を activate にしています．
こうすることで djnago-example で管理をしているライブラリを使用できるようになります．
そのため，このコマンドは 各プロジェクトを開始するごとに実行する必要があります．

最後のコマンドはライブラリの導入です．
ここでは django のライブラリのみを導入しました．
このライブラリは現在 activate されている virtualenv 環境に導入され，
この環境が activate になっている間使用できます．

.. note:: pip で導入されるコマンドについて

   pyenv で管理される環境はあくまでも python 周りのみです．
   何が言いたいのかというと，pip で導入したいライブラリが C 及びその他 python 以外の環境に依存している場合には，
   pip の install にミスる場合があるということです(だってサポートの範囲外ですもの)．
   これは少し注意が必要です．

その他の情報
==============

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   *
