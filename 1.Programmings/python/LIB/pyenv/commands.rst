================================
コマンド
================================

| Last Change: 20-Oct-2015.
| author : qh73xe
|

このページでは pyenv で主に使用するコマンドを記述します．

.. csv-table:: pyenv
   :header: "コマンド", "説明"

   pyenv install -l, pyenv でインストールできる環境の一覧を表示
   pyenv versions, 現在有効になっている環境を確認する
   python --version, 現状利用可能な環境を表示する
   pyenv which python, 現在有効になっている python の場所を確認する
   pyenv install hogehoge, hogehoge のバージョンの python を導入する
   pyenv uninstall hogehoge, hogehoge のバージョンの python を削除する
   pyenv global hogehoge, 基本的に hogehoge のバージョンの python を利用する
   pyenv global system, システムの python を利用する
   pyenv rehash, 

.. csv-table:: pyenv-virtualenv
   :header: "コマンド", "説明"

   pyenv versions, pyenv-virtualenv で設定されたすべての環境を確認
   pyenv virtualenv hogehoge foofoo, hogehoge のバージョンの python を利用し，foofoo の virtualenv 環境を作成する
   pyenv local foofoo, 現在のディレクトリで使用する virtualenv 環境を foofoo にする
