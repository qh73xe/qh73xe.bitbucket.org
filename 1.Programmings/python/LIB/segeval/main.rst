======================================
segEval: セグメンテーションチェック
======================================

| Last Change: 25-Sep-2015.
| author : qh73xe
|

最近見つけた．コーパスのセグメンテーションのチェックを行うためのライブラリです．
テキストセグメンテーション(アノテーション)とは，
いくつかの基本単位（例えば、形態素、単語、行、文、段落、セクションなど）の間に境界線を引くことにより，
テキストの任意に分割を行う作業です．

アノテーションは多くの自然言語処理（NLP）の作業で共通の前処理ステップです.
この作業は自動で行う場合もあれば，手動で行う場合もありますが，どうしても手動で行った方が精度がよく
自動化を行った場合には，手動アノテーションの精度と比較を行う必要がある場合が多いです．
このライブラリでは Boundary Edit Distance と Boundary Similarity を計測することができます．

- http://segeval.readthedocs.org/en/latest/

.. note:: 

    こんなライブラリあるんですね.
    上記の解説は，本家サイトの意訳になります．
    サポートの範囲はテキストコーパスですので，
    時系列処理が必要な音声コーパスの場合，少し拡張が必要かもしれません．

導入方法
===============

導入は pip  から行うのが楽かと思います．

.. code-block:: bash

   $ sudo pip segeval

基本的な使い方
=================

セグメンテーションチェックは，生成された各セグメントの大きさを
文章の各コーダによって符号化することによって開始します.

例えば [Hearst1997] のようにスターゲイザーのテキストを符号化します．
これは JSON 形式で示すと以下のようになります．

.. code-block:: JSON
   :caption: hearst1997.json
   :name: sample_json

    {
        "items": {
            "stargazer": {
                "1": [2,3,3,1,3,6,3],
                "2": [2,8,2,4,2,3],
                "3": [2,1,2,3,1,3,1,3,2,2,1],
                "4": [2,1,4,1,1,3,1,4,3,1],
                "5": [3,2,4,3,5,4],
                "6": [2,3,4,2,2,5,3],
                "7": [2,3,2,2,3,1,3,2,3]
            }
        },
        "segmentation_type": "linear"
    }

要はひとつ目の要素に下位要素がいくつ入っているか，2つ目の要素に下位要素がいくつ入っているのかを記述していきます．
例えば， /あらゆる現実をすべて自分の方へねじ曲げたのだ/ というセンテンスがあるとします．
このセンテンスを単語と文字という要素で区切って行った場合， 4, 2, 1, 3, 2, 1, 1, 1, 4, 1, 1, 1 とかなるわけです．

- ここでは /あらゆる/現実/を/すべて/自分/の/方/へ/ねじ曲げ/た/の/だ と区切りその際の文字数を指定しています．

ここで，上記のような json ファイルがあった場合，以下のようにしてデータを読み込みます．

.. code-block:: python

   import segeval
   dataset = segeval.input_linear_mass_json('hearst1997.json')

ただし，ここではライブラリに付属しているデータを使用します．

.. code-block:: python

   import segeval
   dataset = segeval.HEARST_1997_STARGAZER
   segmentation1 = dataset['stargazer']['1']
   segmentation2 = dataset['stargazer']['2']

ここで segmentation1 がある一人の評価， segmentation2 は別の人間の評価だと思ってください．

で境界類似度を計算するには以下のようにします．

.. code-block:: python

   segeval.boundary_similarity(segmentation1, segmentation2)
   segeval.segmentation_similarity(segmentation1, segmentation2)
   segeval.boundary_statistics(segmentation1, segmentation2)
