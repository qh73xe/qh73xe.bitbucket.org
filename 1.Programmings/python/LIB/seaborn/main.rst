================================
Seaborn
================================

| Last Change: 21-Feb-2016.
| author : qh73xe
|

python のデータ可視化用ライブラリです。
もともと python で可視化を行うと言えば、 matplotlib が有名です。
しかし、これはかなり低レイヤーの可視化ライブラリであり、
複雑な plot を記述することはできても、「簡単なプロットを簡単に行う」ことに長けた
ライブラリではないと思います。

Seaborn は matplotlib のラッパーライブラリで、単純な操作で単純なプロットを記述することが可能です。
R をお使いの方で ggplot2 を使用したことのある方でしたら、 R デフォルトの plot に
対する ggplot2 の役割が Seaborn であると思っていただければ分かり易いかと思います。

- 公式ページ: http://stanford.edu/~mwaskom/software/seaborn/

サンプル
==================

実行準備
----------------

.. code-block:: bash

   pip install seaborn

サンプルデータの読み込み
---------------------------

とりあえず、サンプルとして Seaborn に含まれる iris のデータを使用します。
このデータを散布図にする場合以下のように使用します。

.. code-block:: python

   >> %pylab
   >> import seaborn as sns
   >> iris = sns.load_dataset("iris")
   >> sns.jointplot('sepal_width', 'petal_length', data=iris)

ここで1行目に関しては私がコンソール上の ipython （notebook の方はあまり好きでは
ないです）を使用しているため記述しているものです。
そのため、本題は二行目からになります。

2行目はライブラリの読み込みで、3行目はサンプルデータの読み込みですね。
ここで iris は pandas ライブラリの dataframe クラスであることに注意してください。
この確認は以下のコマンドでできます。

.. code-block:: python

   >> iris.__class__
   pandas.core.frame.DataFrame

pandas に関しては別途ページを用意しているのでこれを確認してください。
基本的に R における DataFrame と同義です。
ここから、 Seaborn の作者はかなり R を意識していることがわかります。

3行目が実際の plot 関数です。
但し ggplot2 との相違点はデータ定義関数 と plot関数が分けられていないことにあります。
これが嫌な場合 python にも ggplot ライブラリは存在し、これを使用することが
可能ですが、これも R の ggplot2 とは「なんか違う」ので使い難い場合も多いです。
端的に ggplot2 が偉大すぎるのかもしれません。

各種の plot 関数は以下のページにまとめてあります。

- http://stanford.edu/~mwaskom/software/seaborn/tutorial/distributions.html
  http://stanford.edu/~mwaskom/software/seaborn/tutorial/regression.html
