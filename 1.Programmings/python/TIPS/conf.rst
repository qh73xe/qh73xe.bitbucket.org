==============================
python の開発環境を構築する
==============================

このページには python の開発環境周りの Tips をメモしていきます.


pip 周辺
===========================

Proxyを設定する
---------------------

proxy 環境の設定が必要な場合, コマンドラインで指定する方法と設定ファイルに記述する方法があります.
コマンドラインから直接 Proxy を指定するには以下のように記述します.

.. code-block:: bash

   $ pip install hogehoge --proxy=http://user@proxy.example.jp:8080

- hogehoge の部分にはインストールしたいライブラリ名が入ります.

管理者権限のない状態で pip コマンドを使用する
-----------------------------------------------

管理者権限のない ( sudo コマンドを使用する権限のない ) サーバーで pip コマンドを使用する場合以下のように --user オプションを指定します.

.. code-block:: bash

   pip install pycrypto --user

- このオプションを指定するとライブラリが, ~/.local/ にインストールされます.

ReadTimeoutError が起きる
----------------------------

なんと言うか，大抵の場合，悪いのはネットワークというか，サーバーなんですがね．
私が使用しているサーバーは時々妙に pip install に時間がかかります．
で起きる ReadTimeoutError.

やめて欲しいです．

これが起きた際のごまかしとして，--default-timeout を設定してしまうことが可能です．

.. code-block:: bash

   pip --default-timeout=100 install hogehoge

なんというか力技．

Shell で補完を行う
----------------------------

zsh を使用している場合は以下のコマンドを打ち込みます.

.. code-block:: bash

   $ pip completion --zsh >> ~/.zprofile

bash の場合は以下の通りです.

.. code-block:: bash

   $ pip completion --bash >> ~/.profile

Virtualenv 周辺
===================

virtualenv とは pithon の仮想環境です.
要は好きな場所に好きな python 環境を建てられるので大変重宝します.


