================================
Python : Gof
================================

| Last Change: 20-Oct-2015.
| author : qh73xe
|

python のメモとしては微妙ですが，
Gof のデザインパターンの勉強ノートです．

なんかすごい今更な気もしますが，自分で Class を書かざる負えなくなったときとか便利でしょう.

Index
================================

.. toctree::
    :glob:
    :maxdepth: 1

    *

Gof のデザインパターン
================================

以下，Gofのデザインパターンの種類と，どのような場合に使用するのかをメモします．


Iterator
Adapter
TemplateMethod
FactoryMethod
Singleton
Prototype
Builder
AbstractFactory
Bridge
Strategy
Composite
Decorator
Visitor
ChainOfResponsibility
Facade
Mediator
Observer
Memento
State
Flyweight
Proxy
Command
Interpreter
