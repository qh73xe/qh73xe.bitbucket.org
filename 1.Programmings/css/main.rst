================================
CSS Tips
================================

| Last Change: 15-Jan-2016.
| author : qh73xe
|

プログラミング言語であるとは言い難いのですが、
何だかんだでよく使うので。

ここではCSSの概要説明というよりも、
Tips をまとめる感じにするつもりです。

お気に入り配色
==================

とりあえず、配色関係です。
ここは、完全に個人の趣味だと思うのですが、
よく使うものの備忘録だと思ってください。

まず、やっぱり CSS を使う場合基本的には web ページ作成が基本になると思います。
そのため、可読性の高い配色にするほうがよいです。
でも所謂デフォルトの白黒って結構目が疲れる気がしています。

適当な配色は結構なやんでいるのですが、暫定的に日本の伝統色から探すことが多いです。

- http://www.colordic.org/w/

このサイトでは、文字の背景には生成り色、文字の色は墨色を使用しています。

- #fbfaf5
- #595857

これとは別に、普段のターミナルでは solarized を使用しているので、
この配色もよく使用します。

- http://ethanschoonover.com/solarized

このカラーテーマに関しては上記公式サイトに CSS カラーコードも記載されているのですが、
色が結構確認しずらいので、その部分のみまとめておきます。

http://ethanschoonover.com/solarized/img/solarized-palette.png

- $base03:    #002b36;
- $base02:    #073642;
- $base01:    #586e75;
- $base00:    #657b83;
- $base0:     #839496;
- $base1:     #93a1a1;
- $base2:     #eee8d5;
- $base3:     #fdf6e3;
- $yellow:    #b58900;
- $orange:    #cb4b16;
- $red:       #dc322f;
- $magenta:   #d33682;
- $violet:    #6c71c4;
- $blue:      #268bd2;
- $cyan:      #2aa198;
- $green:     #859900;

レイアウト関係
=========================

CSS で要素のレイアウトを行う際、
幅や高さは、可変の部分が大きく、決め打ちはしずらいかと思います。

ここで、幅に関しては基本的に 100 % とかの単位できめればよいのですが、
高さに関しては、 % が予想通りに機能しないことがあるかと思います。
このような際には vh という単位が便利です。


vw
    ビューポートの幅の1/100
vh
    ビューポートの高さの1/100
vmin
    ビューポートの幅か高さの値が小さい方の1/100
vmax
    ビューポートの幅か高さの値が大きい方の1/100 

代表的な例として、ページのそれぞれのセクションをビューポートの高さと幅いっぱいに表示させる例を示します。

.. code-block:: CSS

   section {
      width: 100%;
      height: 100vh;
   }

最低限の高さ指定
-----------------------

CSS において、基本的に高さは、
そのコンテンツの量に対して動的に決まります。
しかし、最低限の高さを指定したい場合は多いかと思います。
このような場合、 min-height を利用すると便利です。

以下に具体例を示します。

.. code-block:: CSS

   #hoge {
       min-height:500px;
       height:auto !important;
       height:500px;
       }

幅に応じた設定
=======================

現在表示されている幅に対応した設定を css で設定するには以下のようにします。

.. code-block:: CSS

   @media only screen
   and (min-width : 321px)
   {
   /* Styles *
   /}

ここで media only に関しては以下の要素が用意されています。

- screen: パソコンとか
- print: 印刷物

and 以下の記述は、以下の二つを使用します。

- min-width: これ以上の幅がある場合に適応
- max-width: これ以下の幅がある場合に適応

ここでの min, max は適応範囲の最低、最大であることに注意してください。

ボックスシャドウ
=========================

個人的に、取り敢えず見た目をよくするには、
ボックスに影をつければいいんじゃないかなとか思っています。

とりあえず、オススメのエフェクトは以下の通りです。

- http://www.paulund.co.uk/wp-content/uploads/2012/01/CSS-Box-Shadow-Effects8-590x199.png


.. code-block:: html

   <div class="box">
      <h3>box</h3>
   </div>

.. code-block:: css

   .box
   {
       position:relative;
       box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
   }
   .box:before, .box:after
   {
      content:"";
       position:absolute;
       z-index:-1;
       box-shadow:0 0 20px rgba(0,0,0,0.8);
       top:10px;
       bottom:10px;
       left:0;
       right:0;
       border-radius:100px / 10px;
   }
   .box:after
   {
       right:10px;
       left:auto;
       transform:skew(8deg) rotate(3deg);
   }
