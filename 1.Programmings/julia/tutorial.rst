================================
julia : 基本操作
================================

| Last Change: 12-Jun-2015.
| author : qh73xe
|

ライブラリの導入
==================


:command:`julia` のライブラリ導入は 
:command:`R` のように対話式インターフェースから行います．
とりあえず，以下の構文でライブラリが導入されます．

.. code-block:: julia

   Pkg.add("hogehoge")

- hogehoge の部分がライブラリ名
- 基本的に :command:`git` で管理されているようです．

導入したライブラリを使用するには，以下の宣言を行います．

.. code-block:: julia

   useing hogehoge

文字列処理
===================

とりあえず, :command:`python` や :command:`R` と比較して
文字列の記法が異なるので戸惑うかもしれません．

とりあえず，文字列の結合演算子は積 :kbd:`*` です.
これは， :kbd:`+` が，基本的には交換則が成り立つイメージがあるのに対し，
文字列は交換則が成り立たないためだそうです．

ファイルパス関係
=================

パスの操作を行う場合，基本的に python と似たようなことができます．

例えば ~/Documents/hogehoge.text というパスを考えます．

- linux で julia を使っている場合のみかもですが，'~/' の表記が効きません
   - そのため，ワーキングディレクトリを一旦 home に移動したうえで作業を行います．

.. code-block:: julia

   cd(homedir())
   filePath = "./Documents/test.txt"

このうちファイル名（test.txt）のみを取得したい場合， basename 関数を使用します．

.. code-block:: julia

   basename(filePath)

また，拡張子を除きたい場合以下のようにします．

.. code-block:: julia

   name, ex = splitext(basename(filePath))

今度はあるディレクトリ内にあるファイル（ディレクトリ含む）の一覧を取得してみまし
ょう．

これには readdir 関数を使用します．

.. code-block:: julia

   readdir("./Documents/")
