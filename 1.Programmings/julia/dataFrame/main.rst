================================
DataFrames : データフレーム
================================

| Last Change: 29-May-2015.
| author : qh73xe
|

julia を利用しようと思う人間は大抵 R とか python とかに触れている人間かと思いま
す．で，R の使いやすさの大本は DataFrame 型にあったと思います．
というわけで，これは julia でも使えます．

導入方法
===========

.. code-block:: julia

   Pkg.add("DataFrames")

DataFrame
================

とりあえずデータフレームを使用してみましょう．
基本的な使い方は R と同様です．


.. code-block:: julia

   useing DataFrames
   df = DataFrame(A = 1:4, B = ["M", "F", "F", "M"], name = "hoge")
   df[1]
   df[1,:]
   df[:,1]

データの結合
=============

データフレーム同士を結合したい場合はよくあるかと思います．
R の場合， rbind() や cbind() を使用しました．
古い資料では julia でも同じ名前の関数だったのですが，
最近，名前（と多分実装方法も）が変わったようです．

.. code-block:: julia

   vcat(df, df)  # 旧 rbind
   hcat(df, df)  # 旧 chind
    <`2:#:content`>
