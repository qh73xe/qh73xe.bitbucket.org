================================
bower: JS ライブラリ管理
================================

:Last Change: 21-May-2016.
:Author: qh73xe
:Reference:

やりたいのは python でいうところの pip です．
これを実現するためには :command:`bower` が便利です．

導入方法
=======================

bower の導入は :command:`npm` から行います．
そのため，まずは :command:`npm` を導入しましょう．

.. code-block :: bash

   $ sudo zypper in nodejs

- なお，私の環境では OpenSUSE を使用しています．
    - ここは各 OS，ディストリビューションによって読み替えてください．

:command:`npm` が使用可能になったあとで以下のコマンドを入力します．

.. code-block :: bash

   $ sudo npm install -g bower

- :command:`-g` を指定しているため，:command:`sudo` を使用しています
    - ここも作法はご使用の OS によって異なるかと思います

使用方法
==================

ここでは試しに JQuery を導入してみます．

.. code-block :: bash

    $ bower install jquery

すると :command:`bower` を実行したディレクトリ以下に bower_components というディレクトリが作成され，この中に JQuery が入ります．
