================================
JS チュートリアル
================================

:Last Change: 23-May-2016.
:Author: qh73xe

このページは JS を本格的に触れるのは初めてで、
かつ、 他の言語に関してはある程度慣れている人間を対象に記事を書きます。

筆者の場合、 python はある程度使用できる人間で、
python なら出来るアレを JS でやるためにはどうすればよいのかを中心に
記事を記述するつもりです。

実際のところ JS に関してはとても熟達しているとは言えないレベルですので、
筆者の勉強のメモ書きのような感じの記述になるかもしれません。

.. contents:: 目次
    :local:

環境構築
=====================

一般に JS を使用するのは HTML 上ではあると思いますが、
ここではまず コマンドライン で js を使用できる環境を作成する所から始めます。
これはこの記事の目標が プログラム言語としての JS の記法を知りたい、試したいから
であって、 HTML に綺麗なエフェクトをかけたいからではないためです。

上記の目的からすると一々 HTML を書いて、サンプルを考えて、表示させてみて、ブラウザに付属しているコンソールを確認して、
という作業は冗長でしかないと思います。
それだったら python の様に対話環境を開いて、ゴニョゴニョ書いたら標準出力に表示をとりあえずはしてみるという方法が楽だと思います。

SpiderMonkey
---------------------

:Reference: https://developer.mozilla.org/ja/docs/SpiderMonkey

上記目的にあった JS 環境として、
この記事では SpiderMonkey を利用しようと思います。

これは C言語で実装されたJavaScriptエンジンで Mozilla Fundationでメンテされてます。

導入
~~~~~~~~~~~~~~~~~~

導入は一般的な Fedora の場合 :command:`dnf` から導入可能です。

.. code-block:: bash

   $ sudo dnf install js  # for fedora23

導入が修了したら端末上で :command:`js` とタイプすると対話環境が作成されます。


とりあえず **世界にこんにちわ** するには以下のようにすればよいです。

.. code-block:: js

   js> print("hello world");

対話環境を修了させるには :command:`quit()` を使用します。

勿論、 js コードをファイルに書き込み実行させることも可能です。

.. code-block:: js
   :linenos:
   :caption: hello.js

   print("hello world")

.. code-block:: bash

   $ js hello.js
   hello world

その他組み込み関数を確認するには 以下のページを参照してください。

- https://developer.mozilla.org/ja/docs/SpiderMonkey/Introduction_to_the_JavaScript_shell

Closure Linter(gjslint)
----------------------------------------

上記ツールは google が提供しているコーディングスタイルチェックツールです。

はじめてある言語を記述する際には、
何らかのコーディングスタイルに準拠した方がよいと思います.

導入は pip から行うことができます（なんで?）
使うさいには gjslint コマンドを使用します。

.. code-block:: bash

   $ sudo pip install closure_linter
   $ gjslint path/to/my/file.js  # use this tool

なお、 vim + Syntastic を使用している場合、
以下のように設定可能です。

.. code-block:: vim

   let g:syntastic_js_checkers = ['gjslint']

- くわしい使い方は :doc:`../../../2.Tools/vim/plugin/syntastic` を参照ください

コンテンツ
============================

.. toctree::
   :glob:

   ./*
