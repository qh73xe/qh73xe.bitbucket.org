var myMojule = {
  myProperty: "hoge",
  myConf: {
    useCaching: true,
    language: "ja"
  },
  myMethod: function(){
    return "hogehoge";
  },
  myMethod2: function(){
    return "The language is " + this.myConf.language;
  },
  myMethod3: function(newConf){
    if(typeof newConf == "object"){
      this.myConf = newConf;
      text = this.myMethod2();
      return text;
    }
  },
};

// use myMethod
r1 = myMojule.myMethod();
r2 = myMojule.myMethod2();
r3 = myMojule.myMethod3({
  language: "en",
  useCaching: true
});

print(r1);
print(r2);
print(r3);
