var hoge = new Object();
Object.defineProperty(
    hoge,
    "a", {
        value: "hogehoge",
        writerable: true,
        enumerable: true,
        configurable: true
    }
);

print(hoge.a);
