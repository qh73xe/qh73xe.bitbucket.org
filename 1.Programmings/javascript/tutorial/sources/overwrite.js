var defineProp = function(obj, key, value){
    var config = {};
    config.value = value;
    Object.defineProperty(obj, key, config);
}

// 使うには
var person = Object.create(null);
// プロパティーでオブジェクトを追加
defineProp(person, "fname", "Tanaka");

var taro = Object.create(person)
var ziro = Object.create(person)
defineProp(taro, "forder", "1");
defineProp(ziro, "forder", "2");

print(taro.fname, taro.forder);
print(ziro.fname, ziro.forder);
