var testModule = (function () {
  // プライベート
  var counter = 0;

  // パブリック
  return {
    countInc: function(){
      return ++ counter;
    },
    countReset: function(){
      counter = 0;
    },
    show: function(){
      return "count is " + counter;
    }
  };
})();

testModule.countInc();

print(testModule.show());
print(testModule.counter);  // counter に直接アクセスができない

testModule.countReset();
print(testModule.show());
