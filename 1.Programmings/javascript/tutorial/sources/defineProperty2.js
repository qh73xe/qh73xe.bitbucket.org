var defineProp = function(obj, key, value){
    var config = {};
    config.value = value;
    Object.defineProperty(obj, key, config);
}

// 使うには
var person = Object.create(null);

// プロパティーでオブジェクトを追加
defineProp(person, "name", "Tanaka Taro");
defineProp(person, "dateOfBirth", "1981");

print(person.name);
print(person.dateOfBirth);
