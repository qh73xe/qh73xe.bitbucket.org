function Car(model, year, miles) {
  this.model = model;
  this.year = year;
  this.miles = miles;
}

Car.prototype.show = function(){
  return this.model + "has done " + this.miles + " miles";
};

var civic = new Car("Honda Civic", 2009, 20000);
civic.miles = 10000;

print(civic.show());
