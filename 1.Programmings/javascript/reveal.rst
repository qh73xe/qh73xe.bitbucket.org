================================
reveal.js: HTML スライド
================================

:Last Change: 25-May-2016.
:Author: qh73xe
:Reference: https://github.com/hakimel/reveal.js/


reveal.js は HTML でスライドを作成することができるツールです。
HTML でスライドを作成することで, D3.js と連携をした動的な図表を作成したり、
bootstrap と連携して、一定のレイアウトを簡単に行うことが可能です。

また、基本的には HTML で表示を行うことになりますが、
それを PDF に エクスポートすることも可能です。

導入方法
====================

基本的にはスライドを作成するたびに、 github からクローンを行うとよいでしょう。

.. code-block:: bash

   git clone https://github.com/hakimel/reveal.js.git

直接ファイルをブラウザで開けばスライドが確認できますが、
一般に簡易サーバーを立ててしまって確認をする方が何かと楽だと思います。
これは node.js を利用するとよいでしょう。
以下に fedora 環境でフルセットアップを行う方法を記述しておきます。

.. code-block:: bash

   sudo dnf install node.js
   sudo npm install -g grunt-cli
   git clone https://github.com/hakimel/reveal.js.git
   cd reveal.js
   sudo npm install
   grunt serve


:command:`grunt serve` を使うことで、
http://localhost:8000 にアクセスしてスライドを確認できるようになります。

なお、 PDF にエクスポートしたい場合には http://localhost:8000/?print-pdf#/ にアクセスします。

.. warning:: PDF エクスポート

   Google Chrome でアクセスする必要があります。
   単純に HTML を確認するだけでは、どうしようもなくレイアウトがアレな感じになりますが、
   <Ctrl> + p で印刷画面を呼び出すとまともにプレビューを行います。
   あとはこれを PDF にすればよいです。
