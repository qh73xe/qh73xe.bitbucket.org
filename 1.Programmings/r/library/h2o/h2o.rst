================================
H2O : R で Deeplearning を行う
================================

| Last Change: 05-Dec-2014.
| author : qh73xe

これは何？
===================

最近 R で簡単に Deeplearning ができることで話題になっているライブラリです．
元々はin-memoryプラットフォームとしてHadoop上やSpark上で動かすのを前提として配布されているデータ分析＆機械学習フレームワークだそうです．

これが何故か R 用のライブラリ（多分ラッパー）も用意していてくれていて，話題になっているようです(2014 年の Japan.R でこれに関する発表があるようです)．
実装は java で行っているようなので，java の実行環境が必要です．

- http://d.hatena.ne.jp/dichika/20140503/p1
- http://tjo.hatenablog.com/entry/2014/10/23/230847

導入
===================

最新版の R , Rstudio 環境では :command:`install.packages` で問題なく導入できるようです．

.. code-block :: r

    install.packages("h2o")


.. warning:: Linux で試す場合

    OpenSUSE を使用している場合，'make: gfortran: コマンドが見つかりませんでした'というエラーが置きました.
    以下のツールを導入しました．::

        $ sudo zypper install gcc-fortran

    また RCrul の導入でも失敗があるかもしれません．
    'curl-config' が見つかりませんでしたと言われた場合以下のツールも導入します．::

        $ sudo zypper install libcurl4 libcurl-devel

    make: xml2-config: コマンドが見つかりませんでした と言われた場合以下のツールを導入します::

        $ sudo zypper install libxml2-devel

使い方
=================

とりあえず，最小の実行を行ってみます．
学習するデータには iris を使用します．

h2o を使用していく手順は以下の通りです．

+ ライブラリを呼び出す
+ h2o 本体を起動する（元々はサーバーでの運用が前提のようです．おそらくは簡易サーバーを起動しているのかと思います）
+ 学習データを h2o の求める形式に変更する
+ 学習を行う
+ h2o をシャットダウンする

.. code-block :: r

    library(h2o)
    localH2O = h2o.init(ip = "localhost", port = 54321, startH 2O = TRUE)
    irisPath = system.file("extdata", "iris.csv", package = "h2o")  # データの読み込み（今回はiris）
    iris.hex = h2o.importFile(localH2O, path = irisPath)
    h2o.deeplearning(x = 1:4, y = 5, data = iris.hex, activation = "Tanh")
    h2o.shutdown(localH2O)  # H2Oのシャットダウン

結果は以下のようになります．::

     > h2o.deeplearning(x = 1:4, y = 5, data = iris.hex, activation = "Tanh")
       |============================================================================================================| 100%
     IP Address: localhost 
     Port      : 54321 
     Parsed Data Key: iris.hex 

     Deep Learning Model Key: DeepLearning_979f164e60e40515de9c241158ac435d

     Training classification error: 0.02666667

     Validation classification error: 1

     Confusion matrix:Reported on iris.hex
                                     Predicted
     Actual            Iris-setosa Iris-versicolor Iris-virginica Error
       Iris-setosa              50               0              0 0.000
       Iris-versicolor           0              49              1 0.020
       Iris-virginica            0               3             47 0.060
       Totals                   50              52             48 0.027

Tips
===============



