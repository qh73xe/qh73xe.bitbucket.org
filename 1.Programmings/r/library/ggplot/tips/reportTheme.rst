==================================
ggplotで論文用のthemeを作成する
==================================

.. contents::

このページについて
==============================

さて.みんな大好きggplotについてのお話しです.
ggplotは慣れると,とても簡単に複雑なプロットをこなしてくれるのですが,
よくも悪くもplotのデザインが独特です.

まぁ,私個人の意見を言えば,あのデザイン性はとても好ましく思っているわけですが,
そうも言ってられない場合があります.
例えば,学会投稿などで白黒印刷をしなくてはいけない場合などですね.
このような場合,theme_bw()を基本にごまかしていくことが多いのですが,
もう少し学会誌っぽい(一体何を言っているのでしょう)感じにしたい場合.
自分でthemeを自作しなくてはいけません.

幸いにして,例えば以下のサイトでは『心理学研究』に準拠したthemeを公開しています.
ご本人は正しさに責任は持たないと明記なされておりますが,
大変ありがたく使用させて頂いています.

http://m884.hateblo.jp/entry/2012/12/18/152040

問題
================================

しかし,ggplotのバージョンによって,最新のバージョンでは,上記のサイトのthemeは使用できません.
これは新しいggplotにunit関数が削除されたことが原因のようです.

解決
=====================================

で,この問題に関して,とりあえず,2013年1月20日の時点で解決をできる方法がありました.
この方法で解決できる問題は,恐らく,"何故か古いthemeが使用できない"場合の対処方法になるかと思います.

結論から言えばスクリプト冒頭で "library(grid)" とすれば良いようです.

ggplot2で使用されていたunit関数は実はこのgridパッケージに収録されている関数です.
そのため,このパッケージさえ読み込めば,とりあえず,問題なく使用ができるはずです.

