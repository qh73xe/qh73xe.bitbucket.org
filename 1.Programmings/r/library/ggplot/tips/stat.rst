================================
ggplot2 : stat ( Statistics )
================================

| Last Change: 14-Dec-2014.
| author : qh73xe


これは何？
================================

データを可視化する場合，大きく分けて２種類の可視化方法があります．
つまり，データを **そのまま** 可視化する場合と，データに何らかの処理を施し **その結果** を可視化する場合です．

:command:`ggplot2` の中では前者の処理を行う :command:`geom` 系の関数と，
後者の処理を行う :command:`stat` 系の関数があります．
このページでは主に後者の可視化方法に関して簡単にまとめておきます．

.. contents:: 目次
  :depth: 3

stat_smooth
===============

:command:`stat_smooth` は回帰や，loess (局所多項回帰) などの平滑化関数にデータを fitting した際の結果を可視化します．

引数は以下の通りです．

.. csv-table:: Arguments
   :header: "引数名", "説明", "デフォルト値"

   method, "データに当てはめる回帰式（例えば，:command:`lm`, :command:`glm`, :command:`loess` など）", :command:`loes`
   formula, method で使用する定式, ""
   se, 信頼区間を表示するか否か, ":command:`True`"
   level, 信頼区間のレベル, "0.95"
   fullrange, プロットの全領域に平滑化結果を表示する, :command:`FALSE`
   geom, 出力の表示方法, ""
   n, 平滑化を行う点の数, ""
   "na.rm", "欠損値の削除を警告しないでよいか (bool)", "FALSE"
   mapping, "普通は :command:`aes` や :command:`aes_string` で構成されている", "デフォルトを上書きするのみ必要"
   data, 対象のデータフレーム, "デフォルトを上書きするのみ必要"
   position, この層の Overlapping ポイントに使用する位置合わせ, ""
   "その他", 平滑化関数に渡すその他の引数, ""

これらの引数のうち，このページで解説するのは，method, formula, se, fullrange のみです．

- 他の引数は滅多に使用しないので必要に応じて別枠で Tips を書くかもしれません．

基本的な使い方
-----------------

とりあえずはすべてデフォルトの設定で使用してみます．
なお，データには iris を使用します．


.. literalinclude:: stat/script/stat_smooth1.r

- colour を :command:`geom_point` で指定しているのは stat_smooth の結果を一本にしたかったためです．

.. image:: stat/fig/stat_smooth1.png

単回帰
------------------

引数の項目を見ていただけば分かるように， method を指定することで様々なアルゴリズムが使用できます．
例えば単回帰を行って見ます．

.. literalinclude:: stat/script/stat_smooth2.r
.. image:: stat/fig/stat_smooth2.png

iris のデータは setosa のみ変な分布をしているので，
factor ごとにわけで回帰をしたい場合もあります．
こういう際には colour で指定すると楽です（すべての factor をわけで回帰したい場合は）

.. literalinclude:: stat/script/stat_smooth3.r
.. image:: stat/fig/stat_smooth3.png

一般化線形モデル
------------------------

ではもう少し難しいモデルとして glm を使用してみます．
x も y も連続量で, 下限は 0 以上なので，とりあえずガンマ分布を仮定してみます．

.. literalinclude:: stat/script/stat_smooth4.r
.. image:: stat/fig/stat_smooth4.png

念の為ガウシアンも検討してみましょう．

.. literalinclude:: stat/script/stat_smooth5.r
.. image:: stat/fig/stat_smooth5.png

family の項目は上の引数リストの中にはありません．
:command:`stat_smooth` では使用する :command:`method` に応じた引数を新規に追加することもできます（上の引数表でその他と書いた部分のことです）.

その他オプション
------------------

信頼区間を表示したくないときには :command:`se=F` とします．

.. literalinclude:: stat/script/stat_smooth6.r
.. image:: stat/fig/stat_smooth6.png

データポイントのみではなく，プロット全体に回帰線を書きたい場合は :command:`fullrange=T` とします．

.. literalinclude:: stat/script/stat_smooth7.r
.. image:: stat/fig/stat_smooth7.png

計算された値が欲しい
======================

あまり，可能性は高くないですが，:command:`stat_smooth` は何らかの平滑化関数の結果を **計算** し，
可視化しているわけですから，その計算結果を **数値** として欲しい場合もあるかもしれません． 

あまり（に） 美しくない形ですが，以下のような表現が可能です．

.. literalinclude:: stat/script/stat_smooth8.r

結果::

   [1] 3.152842 3.150022 3.147202 3.144382 3.141562 3.138742 3.135922 3.133102 3.130282 3.127462 3.124642 3.121821 3.119001 3.116181 3.113361
   [16] 3.110541 3.107721 3.104901 3.102081 3.099261 3.096441 3.093621 3.090801 3.087981 3.085161 3.082341 3.079520 3.076700 3.073880 3.071060
   [31] 3.068240 3.065420 3.062600 3.059780 3.056960 3.054140 3.051320 3.048500 3.045680 3.04286 0 3.040040 3.037219 3.034399 3.031579 3.028759
   [46] 3.025939 3.023119 3 .020299 3.017479 3.014659 3.011839 3.009019 3.006199 3.003379 3.000559 2.997739 2.994918 2.992098 2.989278 2.986458
   [61] 2.983638 2.980818 2.977998 2.9 75178 2.972358 2.969538 2.966718 2.963898 2.961078 2.958258 2.955438 2.952617 2.949797 2.946977 2.944157
   [76] 2.941337 2.938517 2.935697 2.932877 2.930057 

指定して有意義である値は大体以下の値かと思います．

- y, 予測値
- ymin, 図示された信頼区間の各下辺
- ymax, 図示された信頼区間の各上辺
- se, 標準誤差

補足すると， :command:`aes` 関数は outfit という項目を含めて，
これは :command:`変数名 <<- ..欲しいもの..` とすると変数名にほしい物を入れてくれるようです．

- 何かもっとスマートな方法もありそうですが...
- 参考: http://stackoverflow.com/questions/9789871/method-to-extract-stat-smooth-line-fit
