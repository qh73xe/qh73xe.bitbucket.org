================================
ggplot2 : fortify
================================

| Last Change: 18-Dec-2014.
| author : qh73xe


これは何？
================================

このページでは :file:`ggplot2` に含まれる :command:`fortify` 系の関数について説明します．
これは R ジェネリックオブジェクトをプロット用のデータフレームに変換を行うためのメゾットです．

.. note:: ジェネリック オブジェクトに関して

    あまり聞き慣れない単語かもしれません．
    R はオブジェクト指向で記述が可能です．
    ただし， java とか， python のオブジェクト指向とは少し感覚が異なります．
    R でオブジェクト指向を使う場合，「一つの関数で色々なことをする」ことを実現しています．
    例えば， :command:`plot()`, や command`summary()` などですね．
    こういう関数をジェネリック関数と呼びます．

    これらの関数には実態はなく，引数のクラスによって実際の処理を変更させます．
    ちなみに ggplot2 の :command:`fortify` 自体もジェネリックオブジェクトです．

fortify.lm
===================

例えば，単回帰を使用します．

.. code-block :: r

    library(ggplot2)
    mod <- lm(mpg ~ wt, data = mtcars)
    head(fortify(mod))

::

                       mpg    wt       .hat   .sigma      .cooksd  .fitted .resid       .stdresid
    Mazda RX4         21.0 2.620 0.04326896 3.067494 1.327407e-02 23.28261 -2.2826106 -0.76616765
    Mazda RX4 Wag     21.0 2.875 0.03519677 3.093068 1.723963e-03 21.91977 -0.9197704 -0.30743051
    Datsun 710        22.8 2.320 0.05837573 3.072127 1.543937e-02 24.88595 -2.0859521 -0.70575249
    Hornet 4 Drive    21.4 3.215 0.03125017 3.088268 3.020558e-03 20.10265  1.2973499  0.43275114
    Hornet Sportabout 18.7 3.440 0.03292182 3.097722 7.599578e-05 18.90014 -0.2001440 -0.06681879
    Valiant           18.1 3.460 0.03323551 3.095184 9.210650e-04 18.79325 -0.6932545 -0.23148309

これは通常の plot では以下のようになります．

.. code-block :: r

    plot(mod, which = 1)

.. image :: fortify/fig/fortify1.png

このモデル自身を ggplot2 で記述するとこんな感じです．

.. code-block :: r

   ggplot(fortify(mod), aes(.fitted, .resid)) +
     geom_hline(yintercept = 0) +
     geom_smooth(se = FALSE)

.. image :: fortify/fig/fortify2.png

例えば，モデルと入力データを合わせてプロットに入れ込むことも可能です．

.. code-block :: r

   ggplot(fortify(mod, mtcars), aes(.fitted, .resid)) +
     geom_point(aes(colour = factor(cyl))) +
     geom_hline(yintercept = 0) +
     geom_smooth(se = FALSE)

.. image :: fortify/fig/fortify3.png
