library(ggplot2)
data <- iris
ggplot(data, aes(x=Sepal.Length, y=Sepal.Width, colour=Species))+
  geom_point() +
  stat_smooth(method="glm", family=Gamma)