g<- ggplot(data, aes(x=Sepal.Length, y=Sepal.Width))+
  geom_point(aes(colour=Species)) + 
  stat_smooth(method="lm", aes(outfit=fit<<-..y..))

fit

class(g)
str(g)