library(ggplot2)
data <- iris
ggplot(data, aes(x=Sepal.Length, y=Sepal.Width))+
  geom_point(aes(colour=Species)) +
  stat_smooth()