======================== 
散布図にラベルを貼る
========================

ほぼデモ用のネタなのですが,ggplotを使用している場合,散布図に時にラベルを振りたい場合があります.
これをggplotのみでやろうとすると結構手間ですが,directlabelsというパッケージを使用するとこの手間が省けます.

公式サイト: directlabels_


導入方法
-----------------------

まぁ,いつもとおりですね.

R::

   install.packages("directlabels")

使用例
-------------------------

とりあえず使用例のソースを記述して置きます.

R::

   library(ggplot2)
   library(directlabels)
   
   market.price<-c(1267,137,952,1662,139,601,1412,629)
   book.value<-c(32, 137, 824, 1278, 111, 304, 1649, 135)
   z <- c("gunho", "matsukiyo", "asahikasei", "kirin", "aoki", "shiseido", "daiichi", "sharp")
   data<-data.frame(market.price=market.price,book.value=book.value,z=z)

   q <- ggplot(data,aes(x=book.value, y=market.price))+geom_point(aes(colour=z))
   direct.label(q, "smart.grid")

ソースはhttp://sanoche16.blogspot.jp/より引用,一部改変しておきました.


.. _directlabels: http://directlabels.r-forge.r-project.org/
