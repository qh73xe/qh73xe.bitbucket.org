==============================
ggplot2 : データの可視化
==============================

このページについて
==============================

このページでは私がRにおいて一番お世話になっているggplot2に関してのTips集を記述していきます.
ggplot2はマニュアルも丁寧に整備されており,
基本的な操作に関しては,わざわざ私が書くようなことでもないかもしれないと思っています.
それほど,効果的なドキュメント整備がなされています,
そのため,基本的な操作に関しては,本家を参照したほうが速いでしょう.

http://docs.ggplot2.org/0.9.3.1/index.html

とは言いつつも, 本家のhelpはやはりhelpで, 重要なことは書いてありますが,
そこから漏れている具体的なTipsもあるかと思います.
このページではそれらのTipsの中で, 私が知っているものを記述していこうと思います.

.. warning:: READ ME

   このページでは導入方法や基本的な使用方法について記述します．
   一方，各種 Tips や 関数の説明などは下記の Tips の項目より個別のページで記述します．

Tips
============================

.. toctree::
   :maxdepth: 1
   :glob:

   tips/directlabels
   tips/fortify
   tips/reportTheme
   tips/reportTheme
   tips/stat

これは何?
===========================

ggplot2 は R 界隈で神様と名高い ハドレーウィッカム が作成した可視化用のパッケージです．
R にはデフォルトでも高度な可視化ツールがありますが，ggplot2 では可視化ツールとしての
コンセプトが少し異なります．

ggplot2 の名前の由来は Grammar of Graphis という考え方から来ているそうで，
オブジェクト指向プログラミングのように属性の継承や特定の部分での属性の上書きを行うことができます．
とりあえず :command:`plot()` とすれば何かでるデフォルトの可視化ツールに比べると，
プログラミング要素が強く，なれるまでに時間はかかるかもしれませんが，
一度文法を身につけると可視化を自由自在にこなせるようになります．

導入方法
=========================

ggplot2 は CRAN から導入可能です．

.. code-block :: r

    install.packages(ggplot2)
    install.packages(gcookbook)


使い方
==========================

ここではまず，ggplot2 の考え方をわかりやすくするように簡単なサンプルを示します．

.. code-block :: r

    >>> library(ggplot2)
    >>> ggplot(iris) + geom_point(aes(x=Sepal.Length, y=Sepal.Width))

.. image :: fig/ggplot1.png

.. code-block :: r

    >>> ggplot(iris) + geom_line(aes(x=Sepal.Length, y=Sepal.Width))

.. image :: fig/ggplot2.png

上記の２つの plot は見た目は異なりますが本質的には同じものを記述しています．
そのため，plot を行うためのコマンドもほぼ同じです．

違うのは唯一 :command:`geom_` という関数だけです．
ここでどう書くのかを指定しているのです．
例えば :command:`geom_point()` は散布図を書くように指定していますし，
:command:`geom_line()` は直線を書くようにしています．

ではこれらの前にある :command:`ggplot()` は何でしょうか？
これはいわゆるインスタンスです．
この説明がいまいち伝わらない方は要は ggplot を使用しますという宣言だ程度の理解で構いません．

.. note:: 引数の指定場所に関して

    ここではあくまでもインスタンスを作成しているだけなので，変数 (上の例の場合 :command:`iris` ) を
    ここで宣言する必要はありません．つまり，以下のように記述してもいいわけです．

    .. code-block :: r

        >>> ggplot() + geom_point(data=iris, aes(x=Sepal.Length, y=Sepal.Width))

    - R に慣れていない方のために補足しますと iris とは R でよく使用されるサンプルデータです．
        - 実際に使用する場合，ご自分のデータフレームをここに当てはめればよいです．
        - データフレームに関してはこのページでは解説しません．

２つの関数をつないでいるのは :command:`+ 演算子` です.
この演算子が意味するのは前項を後項で継承するということです．
そのため，前項で指定した引数は後ろの項でも継承されます．
例えば，こんな感じなのがわかりやすいでしょうか？

- ただし，何が継承されるかは関数によって異なります．

.. code-block :: r

    ggplot(data=iris, aes(x=Sepal.Length, y=Sepal.Width)) + 
    geom_point()

- むしろ :command:`aes()` の位置は :command:`ggplot()` の中で指定するのが一般的です．
    - 一番最初の例で筆者がこの指定を破ったのは plot を行う際に軸の指定が決まるのは :command:`geom_` が決定した以降だからです

継承は何回でもできます．

.. code-block :: r

    ggplot(data=iris, aes(x=Sepal.Length, y=Sepal.Width)) + 
    geom_point() + 
    geom_line()

このような文法を使うのが ggplot の一番の特徴だと思います．
この機能により ggplot2 ではインタラクティブに関数を試し，それをどんどん加工して
いくことが可能になります．

.. note:: :command:`aes()` に関して

    上の例で説明をしなかった :command:`aes()` はデータフレームのどの要素を何に使用するのかを指定する関数です．
    上の例では関数の内部で使用していますが， ggplot2 の基本にしたがって関数の外に記述することも可能です．

    .. code-block :: r

        ggplot(data=iris) + aes(x=Sepal.Length, y=Sepal.Width) + geom_point()

    ただし，基本的には関数内で使用するほうが一般的であるため，このサイトでは関数内で記述します．

ggplot の基本構成
======================

ggplot2 では統一的な関数が用意されています．
そのため，これを把握しておくと上達が速いかと思います．

ggplot2 の基本構成を以下に示します．

- :command:'ggplot' : 親要素の宣言です
- **geom 系** : プロットの種類
- **stat 系** : データの変形を行った後で可視化を行う
    - :doc:`tips/stat`
- scale 系: データと :command:`aes` で指定する軸のマッピングのスケール調整
- Coord 系 : 座標軸関係
- **facet 系** : 図の分割
- **position 系** : グラフの位置調整
- anotation 系 : グラフに注釈を入れる
- **fortify** 系 : data.frame 型以外のものを ggplot に適応させる
    - :doc:`tips/fortify`
- Themes : 外観部分の設定
- その他 : ユーティリティ
