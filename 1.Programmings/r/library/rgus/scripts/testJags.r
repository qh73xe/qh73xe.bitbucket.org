library(ggplot2)
x <- c(3.000000, 3.210526, 3.421053, 3.631579, 3.842105, 4.052632, 4.263158, 4.473684, 4.684211,
       4.894737, 5.105263, 5.315789, 5.526316, 5.736842, 5.947368, 6.157895, 6.368421, 6.578947, 6.789474, 7.000000)

y <- c(5, 3, 6, 7, 7, 5, 9, 9, 7, 10, 12, 8, 7, 4, 4, 11, 9, 9, 8, 6)
data <- data.frame(x=x, y=y)

# glm
ggplot(data, aes(x=x, y=y)) + stat_smooth(method='glm', family="poisson") + geom_point(size=5)

data.g <- glm(y ~ x, family=poisson, data=data)
fitted <- predict(data.g, type = "response", link = "log")