======================================
dplyr : grammar of data manipulation
======================================

| Last Change: 17-Dec-2014.
| author : qh73xe


これは何？
================================

dplyr は :file:`plyr` をより高速にする目的で開発された R ライブラリです．

- 正確には :command:`ddply` に特化して，data.frame をより上手く扱うライブラリです．

dplyrは以下の３つの目的で作成されました．

+ データ分析に必要な, 最も重要なデータ操作ツールを識別し、Rから、容易に使用する
+ C ++ でキー部分を書き込むことにより、メモリ内データの高速なパフォーマンスを提供する。
+ data.table や database内のデータも data.frame と同一のインターフェイスで操作を行う

導入方法
================================

CRAN からインストールする場合は以下の通りです．

.. code-block :: r

    install.packages("dplyr")

一方最新の開発版を利用する場合，以下のようにします．

.. code-block :: r

   if (packageVersion("devtools") < 1.6) {
     install.packages("devtools")
   }
   devtools::install_github("hadley/lazyeval")
   devtools::install_github("hadle y/dplyr")

- linux を使用していると，:command:`install_github` の部分でこける場合があります．
    - Rcurl の問題のようですが下に記述した処理をしてもこける場合は github の url を直打ちすると良いかも知れません．

.. note:: どちらを使用するべきか？

    基本的には CRAN からの導入が楽かと思います．
    しかし，最新の機能を使用できない場合もあります．
    このサイトでは CRAN 版を使用しています．

.. warning:: devtools に関して

    開発版を使用するために必要な :file:`devtools` ライブラリですが，
    Linux を使用していると，この導入が上手く行かない場合があります．

    ログを確認してみると依存ライブラリである :file:`Rcurl` の導入の際に，
    :file:`curl-config` が見つからないというエラーです．
    これは libcurl-devel を OS の方でインストールすると解消します．

    例えば OpenSUSE の場合以下の通りです．

    .. code-block :: bash

       $ sudo zypper in libcurl4 libcurl-devel

dplyr を使用する
================================

.. toctree::
   :maxdepth: 1

   tips/rdb
