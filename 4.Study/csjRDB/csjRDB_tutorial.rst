==================================
日本語話し言葉コーパス: CSJ RDB
==================================

このページではsqliteの基本操作のチュートリアル的な内容を書いていこうと思っております.
ただし,あくまでCSJRDB版を前提にしていきますので,通常のsqlite3の使い方を探している方の参考にはならないかと
思います.

.. contents:: 目次
   :depth: 3 

前準備
----------------------

CSJRDBから情報を取得するためにはsqlite3というアプリケーションが必要です.
その他のアプリケーション(例えばメモ帳など)で開こうとするとひどいことになります.
素直にsqliteをインストールしましょう.

Linuxを使用している場合はsystemのインストールアプリ(apt-getやyum)などで,普通に入手できますし,
MacOSでは標準でインストールされているはずですので,特に前準備は入りません.
問題はwindowsですが,まぁ,適当にググってみれば,必要な情報は手に入るかと.

さて,このsqliteはCGIで使うのが基本のツールです.
そのため,まずはターミナル(端末,コマンドプロンプト)を開き,そこから,csj.dbファイルのあるディレクトリに
移動します.

今回はとりあえず,~/Documents/CSJ-RDB/というディレクトリにcsj.dbファイルがあるものと考えます.

.. code-block:: bash

   $ cd ~/Documents/CSJ-RDB/
   $ sqlite3 csj.db

sqliteでデータベースを開く場合,上記のように"sqlite3 ファイル名"と入力します.
こうすると,端末上で以下のように表示されるかと思います.

.. code-block:: bash

   $ sqlite3 csj.db 
   SQLite version 3.8.0.2 2013-09-03 17:11:13
   Enter ".help" for instructions
   Enter SQL statements terminated with a ";"
   sqlite>

以下,"salite>"と書かれた部分の後ろにコマンドを入力していくことで,データベースを操作していきます.

- もちろん絶対パスでデータベースを指定することも出来ます.

なお,sqliteの操作をやめる場合は以下のコマンドを入力します.

.. code-block:: sqlite3
   
   .quit

この際に,コマンドの頭に"."をつけるのを忘れないようにするのと,最後に";"をつけないことに気をつけてください.

データベースの基本的な考え方
-------------------------------

さてここらへんでデータベースとは何かについて大まかなイメージをお話しておきます.
まぁ,開発思想等々は色々あるわけですが,データベースとは要は"エクセルのすごいやつ"だと思っていれば,
そんなに間違いはありません.

よくデータベースが出てくる技術としてwebページなどが強いためか,ローカルで使用するイメージがあまりないかもしれませんが,
結局は"色々なプログラム言語で使用しやすい","シートごとの調整をうまくやってくれる"エクセルです.
だから基本的な構成概念もエクセルをイメージすればよいです.

つまり,シートがあって,行と列があって,各セルに情報が入っている.
ただ少し用語が違くて,シートのことを"テーブル",列のことを"フィールド/カラム",行のことを"レコード"と呼びます.

CSJRDBのテーブルを確認する
-----------------------------

まずは例えば,CSJRDBに収録されているテーブルを確認してみましょう.
これはsqlite上で以下のコマンドを入力することで実行できます.

.. code-block:: sqlite3

   sqlite> .table
   infoDialog          relLUW2Bunsetsu     relPhone2SUW        segClause         
   infoImpressionMpx   relLUW2Clause       relPhoneme2Mora     segIP             
   infoImpressionSpx   relMora2AP          relPhoneme2SUW      segIPU            
   infoReadSpeech      relMora2Bunsetsu    relSUW2AP           segLUW            
   infoSpeaker         relMora2Clause      relSUW2Bunsetsu     segMora           
   infoTalk            relMora2IP          relSUW2Clause       segPhone          
   linkDepBunsetsu     relMora2IPU         relSUW2IP           segPhoneme        
   linkTone2AP         relMora2LUW         relSUW2IPU          segSUW            
   pointTone           relMora2SUW         relSUW2LUW          subsegLUW         
   relAP2IP            relPhone2Mora       segAP               subsegSUW         
   relBunsetsu2Clause  relPhone2Phoneme    segBunsetsu       

また,各テーブルのフィールド情報を確認する場合,"PRAGMA TABLE_INFO(データベース名)"と入力します.
例えば,"segSUW"に含まれているフィールドを確認してみます.

.. code-block:: sqlite3

   sqlite> PRAGMA TABLE_INFO(segSUW);
   0|TalkID|TEXT|0||0
   1|SUWID|TEXT|0||0
   2|StartTime|REAL|0||0
   3|EndTime|REAL|0||0
   4|Channel|TEXT|0||0
   5|OrthographicTranscription|TEXT|0||0
   6|word|TEXT|0||0

出力結果のうち,2列目がフィールド名ですね.
3列目も結構重要な情報で,そのフィールドに含まれるデータの型が記述されています.

SELECT文
------------------------------

さてでは,SQLの基本的な構文についての話をしていきます.
まずは,とにかく,データ自身にアクセスできないことにはどうしようもありません.
SQLでデータにアクセスする基本的な命令は"あるテーブルのあるフィールドの情報が欲しい"と教えてやることで実行されます.
基本構文は"SELECT フィールド名 FROM テーブル名" です.
例えば,先ほどフィールドを確認した"segSUW"テーブルから"word"の情報を持ってきてみましょう.

.. code-block:: sqlite3

   sqlite> SELECT word FROM segSUW;
   (F eHQto)
   X
   XX
   
   以下略

なお,基本的な命令に関しては終了に";"をつける必要があります.
当然,複数のフィールドを指定することも可能です.

.. code-block:: sqlite3
   
   sqlite3> SELECT word, OrthographicTranscription FROM segSUW limit 5;
   (F eHQto)|(F えーっと)
   X|(R ×
   XX|××


また,あるデータテーブルのすべてのフィールドの情報を持ってきたい場合"SELECT \*"とすることで,あるテーブルのすべての情報を取得できます.

.. code-block:: sqlite3
   
   sqlite3> SELECT * FROM segSUW;
   A01F0055|00000374L|0.373864|0.675959|L|(F えーっと)|(F eHQto)
   A01F0055|00000676L|0.675959|0.799158|L|(R ×|X
   A01F0055|00000799L|0.799158|1.130742|L|××|XX

さらに,あるテーブル内の数値データを四則演算した結果を表示させることも可能です.
例えば,segSUWの時間情報のうち,開始,終了時刻ではなく持続時間を表示させてみます.

.. code-block:: sqlite3

   sqlite3> SELECT StartTime - EndTime FROM segSUW;

   -0.302095
   -0.123199
   -0.331584

重複する結果を無視したい場合(uniqueをかけたい場合)以下のように入力します.

.. code-block:: sqlite3

  sqlite3> SELECT DISTINCT word FROM segSUW;

また,SELECTされた結果をカウントしたい場合は"SELECT COUNT (フィールド名)......"とします.
例えば,segPhoneテーブルからTの子音の出現回数をカウントする場合,以下のようになります.

.. code-block:: sqlite3

   SELECT COUNT (PhoneEntity) FROM segPhone WHERE PhoneEntity LIKE 't';
   96523

WHERE 句 : 行の抽出
-----------------------

基本構文
~~~~~~~~~~~

比較演算子
~~~~~~~~~~~

論理演算子
~~~~~~~~~~~

JOIN 句 : 結合
-----------------

INNER JOIN 句
~~~~~~~~~~~~~~~

LEFT OUTER JOIN 句
~~~~~~~~~~~~~~~~~~
.. note:: 例題

   外部結合

   係り先を持たない文節を抽出し、TalkID、文節ID、文節の基本形を表示

GROUP BY 句 : 集計関数
-----------------------

GROUP BY とともに用いる主な集計関数
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- COUNT : 行数を取得
- SUM : 合計値を取得
- AVG : 平均値を取得
- MAX : 最大値を取得
- MIN : 最小値を取得

ORDER BY 句 : 昇順に並び替え
-----------------------------

HAVING 句 : グループ毎のまとめ上げの後に、グループに対して条件で絞り込み
--------------------------------------------------------------------------

CASE 式 : 条件式によって異なる値を返す
----------------------------------------

CASE 式を利用した集計
~~~~~~~~~~~~~~~~~~~~~~~~~~



CSJ-RDB の構成
-----------------

親子関係テーブル
~~~~~~~~~~~~~~~~~~~

.. note:: 例題1

   二つのセグメント・テーブルの結合

   文節テーブル（ segBunsetsu ）と節単位テーブル（ segClause ）を結合し、 
   TalkID 、 文節 の基本形（ OrthographicTranscription ）、 節単位の基本形 （ OrthographicTranscription ） の列を選択せよ

.. note:: 例題2

    二つのセグメント・テーブルの結合

    文節テーブルと節単位テーブルを結合し、節単位末尾の文節を抽出し、TalkID、文節の基本形、節単位の基本形の列を選択せよ

.. note:: 例題 3 

   二つのセグメント・テーブルの結合
   
   文節テーブルと節単位テーブルを結合し、節単位末尾の文節が「思」で始まる行を抽出し、
   TalkID、文節の基本形、節単位の基本形の列を選択せよ

.. note:: 例題 4

   三つのセグメント・テーブルの結合
   
   短単位テーブル（segSUW）と文節テーブルと節単位テーブルを結合し、節単位末尾の文節の先頭の短単位の基本形（OrthographicTranscription）が
   「思」で始まる行を抽出した上で、当該短単位の継続長を計算して”Duration”という別名を付け、TalkID、 短単位の基本形、文節の基本形、節単位の基本形と合わせて表示

サブセグメント・テーブル
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note:: 例題 5

   セグメント・テーブルとサブセグメント・テーブルの結合

   短単位テーブル、サブ短単位テーブル（ subsegSUW ）、文節テーブル、節 単位 テーブルを 結合し 、
   節単位末尾の文節の先頭の短単位の品詞（ SUWPOS ）が 「名詞」である行を抽出した上で、
   当該短単位の継続長を計算して“Duration”という別名を付け、
   TalkID、短単位の基本形、文節の基本形、節単位の基本形と合わせて表示

リンク・テーブル
~~~~~~~~~~~~~~~~~~~~~~~~

.. note:: 例題 6

   係り元と係り先の文節を結合

   linkDepBunsetsu を用いて係り元と係り先の文節テーブル（いずれもsegBunsetsu）を結合し、係り元の文節の基本形が「が」で終わる行を抽出した上で、
   TalkID、係り元と係り先の文節の基本形を表示

.. note:: 例題 7

   同じセグメント・テーブルを結合
   
   節単位の先頭の文節の基本形が「は」で終わり、かつ、節単位の末尾の文節の基本形が「思」で始まる行を抽出し、
   TalkID、節単位の基本形、節単位の先頭と末尾の文節の基本形を表示

.. note:: 例題 8

   同じセグメント・テーブルを結合
   
   文節の先頭の短単位の品詞が「名詞」であり、かつ、文節の末尾の短単位の代表表記（SUWLemma）が「だ」である行を抽出し、
   文節の基本形を表示

.. note:: 例題 9

   同じセグメント・テーブルを結合
   
   節単位内の隣接する二つの短単位の基本形の組合せを抽出し、
   TalkID と合わせて表示

前後関係を取得する
--------------------------

節短単位の基本形の組合せを抽出し、TalkIDと合わせて表示



