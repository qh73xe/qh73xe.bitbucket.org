# -*- coding: utf-8 -*-

# サイトに関する説明
# 苦労する遊び人の玩具箱 documentation build configuration file, created by
# sphinx-quickstart on Fri Jan 10 14:35:19 2014.

# ライブラリのインポート
# import sys
# import os

# エクステンションの指定
extensions = [
    'sphinx.ext.autodoc',   # 各種ファイルに変換可能
    'sphinx.ext.doctest',   # 辞書の定義可能
    'sphinx.ext.mathjax',   # 数式の記述可能
    'sphinx.ext.viewcode',  # このディレクトリに置かれたコードは表示可能
    'sphinx.ext.graphviz',
]

# 基本設定
templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
project = u'苦労する遊び人の玩具箱'
copyright = u'2014, qh73xe'
version = '1'
release = '1'
exclude_patterns = ['_build']
html_static_path = ['_static']
htmlhelp_basename = 'ToolBoxofQh73xe'
html_favicon = '_static/lg.ico'
language = "ja"
html_search_language = 'ja'
html_search_language_option = {'type': 'yahoo'}

# latexの設定(基本的に気にしない)
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    'papersize': 'a4paper',
    # The font size ('10pt', '11pt' or '12pt').
    'pointsize': '10pt',
    # Additional stuff for the LaTeX preamble.
    'preamble': '',
}

latex_documents = [
    (
        'index', '苦労する遊び人の玩具箱.tex',
        '苦労する遊び人の玩具箱 Documentation',
        'qh73xe', 'manual'
    ),
]
# -- Options for manual page output ---------------------------------------
man_pages = [
    ('index', 'springcamp2014', u'苦労する遊び人の玩具箱 Documentation',
     [u'qh73xe'], 1)
]
# -- Options for Texinfo output -------------------------------------------

texinfo_documents = [
    (
        'index', '苦労する遊び人の玩具箱', '苦労する遊び人の玩具箱 Documentation',
        'qh73xe', '苦労する遊び人の玩具箱', 'One line description of project.',
        'Miscellaneous'
    ),
]

# templateの設定
html_theme = 'sphinx_rtd_theme'


# CSS の追加
def setup(app):
    app.add_stylesheet('custom.css')

# side-barの設定
html_sidebars = {
    '**': ['globaltoc.html', 'relations.html', 'sourcelink.html', 'searchbox.html'],
}
