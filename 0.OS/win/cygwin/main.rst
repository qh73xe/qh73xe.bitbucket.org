================================
cygwin : windows 用 UNIX 環境
================================

| Last Change: 05-Dec-2014.
| author : qh73xe

これは何？
=================================

要は windows で UNIX 環境を実行するツール群です．
ポイントは cygwin 自身が環境であるということですね．
簡易に使えるため，便利ではありますが，windows ネイティブではないため，GUI 周りを
使用しようとすると途端に厄介な問題が生じます．

それでもコマンドプロンプトよりはマシなので windows 環境で生きていくためには必要
なツールの一つでしょう．

- 公式サイト: https://www.cygwin.com/

インストール方法
==============================

+ 公式サイト -> Install Cygwin に移動
+ OS の bit 数に合わせて， :file:`setup.exe` を入手
    + "Installing and Updating Cygwin for ..." と記述された部分にあります．
+ ダウンロードした :file:`setup.exe` を実行
    + 実際のファイル名は setup-(それぞれのbit数に合わせた番号).exe になります
+ 実行すると色々と聞かれる
    + なんとことか分からない場合，とりあえず次へをクリック


apy-cyg の導入
==============================

Linux を使用していると例えば :command:`apt-get` のようなコマンドがあります．
これはコマンド一つで何かのアプリケーションを入れられるツールです．

* Mac で言えば Mac Port ですね．

cygwin ではデフォルトでこれらに類似するアプリケーションは存在せず，
いちいち， :file:`Cygwin Setup` を実行する必要があります．
これははじめのうちは便利ですが，だんだん面倒になってきます．
こういう問題を解決するためのツールとして :command:`apt-cyg` というツールがあります．

導入方法
---------------

:command:`apt-cyg` は :command:`git` から導入します．

- 最近，git hub にフォークしたようで最近の環境ではこちらを使用する必要があります
- apt-cyg : https://github.com/transcode-open/apt-cyg

.. code-block :: bash

    $ git clone https://github.com/transcode-open/apt-cyg
    $ cd apt-cyg
    $ install apt-cyg /bin

.. note:: 公式サイトでの導入法

    このページでは公式サイトの記述とは異なる方法でダウンロードをしています．
    以下に公式サイトの方法を記述しておきます::

        wget rawgit.com/transcode-open/apt-cyg/master/apt-cyg
        install apt-cyg /bin

    - おそらくは :command:`git` はデフォルトではインストールされていないからでしょう.
