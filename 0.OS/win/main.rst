==============================
window設定
==============================

仕事がらどうしてもwindowsを使用しなくては行けない状況というのは少なからず存在します．で，同仕様もないので色々カスタマイズしていくことになります．
ここではwindowsを何とか使用できるようにするTipsをメモしておきます．

Contents:

.. toctree::
   :maxdepth: 1

   Ctrl2Cap
   everything
   cygwin/main
