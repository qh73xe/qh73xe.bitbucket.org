================================
OpenSUSE: Tips
================================

| Last Change: 01-Dec-2014.
| author : qh73xe

zypper の Build Essential
============================

:command:`apt-get` には :command:`build essential` というものがあります．
これを入れると gcc などのツールが一括でインストールされるので大変ラクなのですが
，:command:`zypper` の場合以下のコマンドで同様のことが可能のようです．

.. code-block :: bash

   $ sudo zypper install --type pattern devel_basis

- https://forums.opensuse.org/showthread.php/413553-Build-Essential


zypper に新規レポジトリを追加する
==================================

:command:`zypper` は登録されているレポジトリを検索しアプリケーションの導入を行う
ツールです．しかし，デフォルトのレポジトリでは常に最新のライブラリが登録されてい
るとはかぎりません．

そのため，必要によっては新規にレポジトリを登録する必要があります．
新規レポジトリの登録は :command:`zypper addrepo -f <URL>` でできます．

例えば，R の公式レポジトリを登録してみます．

.. code-block :: bash

    $ VERSION=$(grep VERSION /etc/SuSE-release | sed -e 's/VERSION = //')  # OpenSUSEのバージョンを取得
    $ sudo zypper addrepo -f http://download.opensuse.org/repositories/devel\:languages\:R\:patched/openSUSE_$VERSION/R-patched

.. note:: zypper コマンドについて

    しかし，最近の OpenSUSE ではブラウザを介してGUIからアプリケーション管理が可能です.
    これはとても便利な機能ですが，いちいち手動で作業をしなくてはいけません．

    一方で，CUI での操作はつまり Shell スクリプト化できるわけで決め打ちの環境を
    一気に作成する際には便利です．




ディストリビューションを 13.1 から 13.2 にアップグレードする
===============================================================

OS のアップグレードは以下の３つの手順が必要です．

1. バックアップをとる
2. サードパーティ製のレポジトリをオフにする
3. OS をアップグレードする

まず，現在のディストリビューションの確認をするには以下のコマンドを入力します::

   $ cat /etc/os-release

例えばこのように出てくるかと思います．

| NAME=openSUSE
| VERSION="13.1 (Bottle)
| "VERSION_ID="13.1
| "PRETTY_NAME="openSUSE 13.1 (Bottle) (x86_64)"
| ID=opensuse
| ANSI_COLOR="0;32"
| CPE_NAME= "cpe:/o:opensuse:opensuse:13.1"
| BUG_REPORT_URL="https://bugs.opensuse.org"
| HOME_URL="https://opensuse.org/"
| ID_LIKE="suse"

続いて OS のアップグレードが可能であるかを確かめます．::

    $ zypper repos --uri

openSUSE-13.1-Update が yes になっていればよいです．

続いて現在のバージョンをとりあえず最新版にしておきます．::

    $ sudo zypper refresh
    $ sudo zypper update

サードパーティ製のレポジトリを一旦停止します．::

    $ zypper mr -da

最後に新しいバージョンのレポジトリを登録し OS をアップデートします．::

    $ sudo zypper ar http://download.opensuse.org/distribution/13.2/repo/non-oss/non-oss-13.2
    $ sudo zypper ar http://download.opensuse.org/distribution/13.2/repo/oss/ oss-13.2
    $ sudo zypper ar http://download.opensuse.org/update/13.2/ update-13.2
    $ sudo zypper ar http://download.opensuse.org/update/13.2-non-oss/ update-non-oss-13.2
    $ sudo zypper up
    $ sudo zypper dup

参考
------------------

- http://imanudin.net/2014/11/06/how-to-upgrade-opensuse-13-1-to-opensuse-13-2/
- http://www.unixmen.com/upgrade-opensuse-13-2-opensuse-13-1/
