================================
Leap motion を使う
================================

| Last Change: 03-Dec-2015.
| author : qh73xe
|

色々あって、つい Leap motion を購入してしまいました。
で、よくよく見ると公式のドライバーは debian/ubuntu 用です。

あれ？
ふぇどらは...

というわけで、このページでは Fedora を使用して Leap motion を使ってみる方法について記述します。

.. contents:: 目次

ドライバーのインストール
=============================

上で色々書きましたが公式サイトの QA に解答がありました。

- https://support.leapmotion.com/entries/40467976-Linux-Installation

とりあえず該当部分を引用します::

    How can I install on Fedora?
    For Fedora 21 and later, first install alien and convert Leap-*-x64.deb into a rpm package as follows.

要は一旦 deb ファイルを rpm にコンバードしてね、ということらしいです。
具体的には以下の操作で、これが可能です。

.. code-block:: bash

   sudo yum update
   sudo yum install alien
   sudo alien -rv --scripts Leap-*-x64.deb
   Next, run:
   sudo rpm -ivh --nodeps --force leap-*-2-x86_64.rpm

動作テスト
====================

fedora において、 Leap motion を使うには、
まず、通信機を起動させる必要があるようです。

で取り敢えず Leap motion のデモを確認するには :command:`Visualizer` を使用します。

.. code-block:: bash

   $ sudo leapd &
   $ Visualizer

これで色々試せるかと思います。
