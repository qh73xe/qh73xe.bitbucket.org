================================
Fedora23
================================

| Last Change: 14-May-2016.
| author : qh73xe
|

.. meta::
   :description: Linux / fedora23 についての情報です。

11/4 日に fedora 23 がリリースされました.
早速試してみたので, その雑感をば.

.. contents:: 目次


導入方法
===================

最近の(確か 21 位から) fedora は 既存のバージョンからのアップグレードが簡単になっています.
基本的に以下の手順で更新は終了します.

.. code-block:: bash

   $ sudo dnf update --refresh
   $ sudo dnf install dnf-plugin-system-upgrade
   $ sudo dnf system-upgrade download --releasever=23
   $ sudo dnf system-upgrade reboot

なんということでしょう.
これでOSのアップグレードは終了です.

- ちなみに 21 -> 22 の際に使用した :command:`fedup` は削除された模様です.

.. note:: 上記方法でうまく行かない場合

   fedora21 から 22 に上げた上で上記設定を行うとうまく行かない場合があります.
   具体的には dnf-plugin-system-upgrade が入っているはずなのに dnf system-upgrade の段階で
   プラグインの読み込みに失敗する場合です.

   その場合(多分,そのうち解決しますし, 公式で勧められている方法ではない ですが) 以下のようにすると,
   あるいは幸せになるかもしれません.

   .. code-block:: bash

      # rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-23-$(uname -i)
      # dnf upgrade
      # dnf clean all
      # dnf --releasever=23 --setopt=deltarpm=false distro-sync

   - `参考サイト <https://fedoraproject.org/wiki/Upgrading_Fedora_using_package_manager?rd=Upgrading_Fedora_using_yum#Fedora_22_-.3E_Fedora_23>`_

導入後
=================

とりあえず,導入後に走らせておくべきコマンドです.

.. code-block:: bash

   sudo rpm --rebuilddb
   sudo dnf distro-sync --setopt=deltarpm=0
   sudo dnf install rpmconf
   sudo rpmconf -a


まず, 古い rpm のデータベースがあるのは気持ちが悪いので再構築させます.

二行目ではディストリビューションの同期を行い, バージョンの違いを回避させます.
もし, プロキシ周りの何かがある場合, /etc/dnf/dnf.conf で設定が行えます.

rpmconf は .rpmnew, .rpmsave 及び .rpmorig を検索し, 現在のバージョンを保持するか, 古いバージョンを使うか, 差分を確認しマージするかを選択できます.
ここまでの設定で, dnf 周りはとりあえず綺麗になるかと思います.

google chrome
------------------------

クロームを使用している場合,再度インストールしておいたほうが安全かと思います.

.. code-block:: bash

   sudo dnf remove google-chrome-\* && sudo dnf install google-chrome-[beta,stable,unstable] 


Adobe Flash
--------------------------

flash に関しては基本的に 22 のものと同様で良さそうです.

- https://www.if-not-true-then-false.com/2010/install-adobe-flash-player-10-on-fedora-centos-red-hat-rhel/comment-page-10/


Activate RPMFusion repo
----------------------------

.. code-block:: bash

   sudo rpm -ivh http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-stable.noarch.rpm

Java Plugins for web
--------------------------

java プラグインに関しては以下の通りです.

.. code-block:: bash

   sudo dnf install icedtea-web java-openjdk


気がついた点
======================

以下, 私的に気がついた点を示します.

- 設定の詳細が詳しくなった
- アカウント設定が追加
- 日本語入力のデフォルトが andy から kkc に変更された
- dnf で入れた vim に + python がついたし iflua

fedora23 Tips
========================

ここでは、 fedora23 を使用していて、気になったり調べたりしたことで、
どこに書くべきか悩んだことを記述します。

ibus-kkc で語彙登録
-------------------------

fedora23 を使ってみて、ようやく ibus 用のデフォルト IME が kkc であることに気がつきました。
そもそもこれ、形態素解析エンジン KAKASI のことなんですね（昔、単語の分かち書きをするときにお世話になった）。

- https://ja.wikipedia.org/wiki/KAKASI

基本使い勝手は悪くないと思います。
最初からかなり優秀（というか網羅的）な辞書を使っていますから。
でも、良くも悪くも辞書依存なので、時々、あれ？ということがあります。

で、そういう時に普通思いつく手段って、単語辞書作成じゃないですか？
でも kkc にはそもそもユーザー辞書って存在しません。
ではどのように 読み仮名と漢字の対応を作成するかというと、

1. なんとかして、最終的に変換したい漢字を入力
2. alt + R
3. 普通に読みを入力
4. なぜか覚えている

- 漢字の優先順位も変換回数を覚えてくれていて、そこから考えるようです。

ある意味優秀すぎますが、やや、人類には早過ぎる気もしないでもないです。

rescue mode で起動する
-------------------------

Linux をいじっている宿命みたいなもので
どうしても、設定でミスって gnome があれになる場合があります（特にオーディオ系）。
このようなときには、実は OS 自体は走っているわけで CUI 上で設定を修正すればよいわけです。
しかし、何故か私の環境の場合、rescue モードを選択しても GUI が起動してしまい、起動に失敗します。

- 多分 fedup 等でバージョンアップをしているので、この辺が反映されていないのでしょう。

このような際には以下の手順を踏みます。

1. PC を再起動
2. grub2の起動画面で "e" を打つ
3. grub2の設定ファイルの画面に移行するので、Linux と書かれた行に、systemd.unit=rescue.target と記述
     - この際、英字配列であるので注意
4. F10 で起動

JACK Audio Connection Kit
----------------------------

上記にも書いた通り、Linux のサウンド関係は結構複雑です。
ここでは JACK 周りの設定を記述していきます。

とりあえず以下の二つを導入しておく必要があります。
まず JACK 用のサウンドカードを指定してみましょう。

.. code-block:: bash

   $cat /proc/asound/cards

   0 [MID            ]: HDA-Intel - HDA Intel MID
                        HDA Intel MID at 0xff87c000 irq 32
   1 [DACHA200       ]: USB-Audio - DAC-HA200
                        ONKYO Corporation DAC-HA200 at usb-0000:00:1d.0-1.7, full speed

上記のコマンドで現在 PC が認識しているサウンドカードが一覧になります。
例えば私の場合、 USB-Audio のデバイスを接続していますので、
DACHA200 の表記があります。

デフォルトのままでもよいのですが、折角なので設定を書き換えてみます。
QjackCtl を起動し、設定から 'Interface' テキストボックスを選択します。
ここで先程確認したサウンドカードを選択し、保存をクリックします。

続いては PulseAudio を JACK と統合します。
PulseAudio は例えば Flash で音を再生する際などに使用します。

- あと pyAudio などを使用する際にも利用されますね。

上記設定を行う為には pulseaudio-module-jack が必要になります。
これを導入した後で、 /etc/pulse/default.pa を編集します。

.. code-block:: bash

   $ sudo vim /etc/pulse/default.pa

このファイルの #load-module module-alsa-sink と書かれている行の下に以下の二行を追加します。

.. code-block:: bash
   :caption: /etc/pulse/default.pa

   #load-module module-alsa-sink
   load-module module-jack-sink
   load-module module-jack-source

編集が修了したらば、PulseAudio を再起動します。これには以下のコマンドを使用します。

.. code-block:: bash

   killall pulseaudio

gvim で日本語入力に困る
-------------------------

私個人は大の vimmer で vim がないと何もできません。
で日本語ドキュメントもよく書くわけで日本語固有モードを使用していました。
基本的に cui vim を使用しているのですが、web 系の記述には vimp のエディタを使用します。
こうするとデフォルトでは gvim が起動します。
で、この場合、gvim の設定はとくに何も自分では書いていなかったのですが、
デフォルトで set imdisable が設定されているようです。

このため日本語固有モードのコマンドがキャンセルされてしまい、一度
インサートモードから抜けると日本語入力ができなくなる問題に遭遇しました。
解決策としては ~/.gvimrc の上記設定をなくせばよいです。

