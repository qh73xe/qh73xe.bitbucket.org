================================
Fedora 23
================================

:Last Change: 26-Jun-2016.
:Author: qh73xe
:Reference: https://fedoramagazine.org/upgrading-fedora-23-workstation-to-fedora-24/

6/21 日に Fedora24 が公式リリースをしました。
このページでは Fedora23 から Fedora24 にアップグレードする方法と、その後の処理を記述します。

アップグレード
================================

アップグレードに関しては前回と同様、dnf から行うことができます。
とりあえず以下の手順でよいでしょう。

.. code-block:: bash

   $ sudo dnf upgrade --refresh
   $ sudo dnf install dnf-plugin-system-upgrade
   $ sudo dnf system-upgrade download --releasever=24

ここまでの処理で fedora23 から fedora24 用のレポジトリに登録が切り替わります。
ただし、 独自レポジトリを登録している場合、切り替えが上手くいかない場合があるようです。

このような場合、--allowreasing オプションを追加します。

.. warning:: Fedy

   Fedy を利用している場合、
   上記のオプションを付けてもエラーが吐かれます。
   私の場合は、一度 Fedy 自身をアンインストールすることで
   上記の問題を回避しました。

最後に以下のコマンドを走らせることで、
アップデートを実行します。

.. code-block:: bash

   sudo dnf system-upgrade reboot

その後の処理
=================================

ここでは、私が利用している Fedora 環境を作成するための手順をメモしておきます。
そのため、基本的には個人の趣味でどれを実行するのかは異なるかと思います。

RPMFusion Repos
------------------------

fedora は厳密な意味で oos に準拠しています。
ここから外れるものは通常 dnf からインストールすることはできません。

これには mp3 を始めとする各種のエンコーディングを含みます。
発想自体は嫌いではないのですが、少々問題でもありますので、
これをインストールできるようにします。

以下二行を実行すると、大体のエンコードに耐えられるかと思います。

.. code-block::

   $ sudo dnf install http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
   $ sudo dnf install gstreamer-plugins-bad gstreamer-plugins-bad-free-extras gstreamer-plugins-bad-nonfree gstreamer-plugins-ugly gstreamer-ffmpeg gstreamer1-libav gstreamer1-plugins-bad-free-extras gstreamer1-plugins-bad-freeworld gstreamer1-plugins-base-tools gstreamer1-plugins-good-extras gstreamer1-plugins-ugly gstreamer1-plugins-bad-free gstreamer1-plugins-good gstreamer1-plugins-base gstreamer1

Flash player
-------------------

同種の問題に flash player があります。
そのためこれも解決します。

.. code-block::

   $ sudo rpm -ivh http://linuxdownload.adobe.com/adobe-release/adobe-release-x86_64-1.0-1.noarch.rpm
   $ sudo rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-adobe-linux
   $ sudo dnf install flash-plugin alsa-plugins-pulseaudio libcurl

なお、このサイトでは 64 bit を前提にしています。
32 bit を利用する場合、一行目の url が変るので注意してください。

Fedy
---------------------------

fedy とは fedora において、種々ある面倒な処理（Chrome とか Dropdox とかのインストール等） を楽にする
アプリケーションです。これを導入するには以下のコマンドを使用します。

.. code-block::

   $ bash -c 'su -c "curl http://folkswithhats.org/fedy-installer -o fedy-installer && chmod +x fedy-installer && ./fedy-installer"''
