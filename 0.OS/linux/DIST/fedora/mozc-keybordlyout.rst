==============================
日本語キーボードを設定する
==============================

問題
------------------------------------------

mozcを使用すると何故か日本語キーボードレイアウトにならない.

詳細
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

日本語キーボードのレイアウト.
インストール時から日本語にすることが可能です.

ただし,mozcを使用する場合はダメです.
これが盲点でした.何故か,"/etc/vconsole.conf"を"jp106"にしても日本語になりません.
それもそのはずで,mozcに関しては別の設定ファイルをいじらないと行けないようです.

解決
------------------------------------------

端的に解決法を言えばmozcの設定ファイルをjpに変更すればよいようです.

方法
~~~~~~~~~~~~~~~~

"/usr/share/ibus/component/mozc.xml"を変更します.

.. code-block:: bash

   <language>ja</language>
   <icon>/usr/share/ibus-mozc/product_icon.png</icon>
   <symbol>あ</symbol>
   <layout>jp</layout>
   <name>mozc-jp</name>
   <hotkeys>Control+space,Zenkaku_Hankaku</hotkeys>
   <longname>Mozc</longname>

設定ファイルを上記のように編集したあと,再起動すれば問題ないはずです.
