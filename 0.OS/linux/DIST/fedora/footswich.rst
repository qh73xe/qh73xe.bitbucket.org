================================
フットスイッチを利用する
================================

| Last Change: 09-Dec-2015.
| author : qh73xe
|

.. contents:: 目次

問題
=================

先日、アマゾンよりフットスイッチというものを購入してしまいました。
完全に衝動買いというやつです。

- ルートアール USB3連フットペダルスイッチ マウス操作対応 RI-FP3BK
- http://www.amazon.co.jp/dp/B004HAFZ1I/ref=pd_lpo_sbs_dp_ss_1?pf_rd_p=187205609&pf_rd_s=lpo-top-stripe&pf_rd_t=201&pf_rd_i=B00350KFZW&pf_rd_m=AN1VRQENFRJN5&pf_rd_r=0TDRTFN7XNT5THKMM8PA

これは、普段PC使いが使わない足に3つのボタンを付けられるわけで、
面倒な alt, ect, super をうまいこと使えるかな？などと欲がでたわけです

で、購入してみて気が付いたのですが、LINUX 対応していないわけですよね。当然。
デフォルトでも認識はするのですが、 a,b,c に関連づけられており、つかいもんになんねーと。

というわけで、今回はこのツールの設定に関してメモをしておきます（日本で一体誰が必要とする情報なんだろう）。


とりあえず、デバイスIDを取得する
====================================

なにはともあれ、デバイスのIDを確認します。
これができれば、あとはそのIDに関連してキー設定を変えればよいだけです。

以下のコマンドを使うと、接続しているデバイスのIDがわかります。

.. code-block:: bash

   lsusb

   Bus 002 Device 007: ID 04f9:0059 Brother Industries, Ltd 
   Bus 002 Device 006: ID 0c45:7403 Microdia Foot Switch
   Bus 002 Device 005: ID 046d:c52b Logitech, Inc. Unifying Receiver
   Bus 002 Device 004: ID 1058:1140 Western Digital Technologies, Inc. My Book Essential (WDBACW)
   Bus 002 Device 003: ID 04b4:0101 Cypress Semiconductor Corp. Keyboard/Hub
   Bus 002 Device 002: ID 8087:0020 Intel Corp. Integrated Rate Matching Hub
   Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
   Bus 001 Device 003: ID 058f:6362 Alcor Micro Corp. Flash Card Reader/Writer
   Bus 001 Device 002: ID 8087:0020 Intel Corp. Integrated Rate Matching Hub
   Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

なんか色々私の趣味がバレてしまいそうですが、重要なのは Microdia Foot Switch の部分です。
ID 0c45:7403 が フットスイッチの ID です。

rgerganov/footswitch を使用する
=================================

で、盛大に手を抜いてしまって申し訳ないのですが
上記のデバイスID を持つフットペダルに対しては、もう既に便利な設定用コマンドが用意されています。

- https://github.com/rgerganov/footswitch

導入
-------------

このツールは hidapi library に依存しています。
これは Fedora では標準に入っていません。
そのため、導入しておきます。

.. code-block:: bash

    sudo dnf install hidapi-devel-0.7.0-5.a88c724.fc23

その上で以下の手順でビルドをします。

.. code-block:: bash

   git clone https://github.com/rgerganov/footswitch.git
   cd footswitch
   make
   sudo make install

使い方
-----------------

いか footswitch コマンドの使い方です。
多分、具体例の方がよいと思います。

.. code-block:: bash

   sudo footswitch -1 -k a -2 -k b -3 -k c
   sudo footswitch -1 -m ctrl -k a -3 -m alt -k f4

例えば一行目の設定ですと、一つ目のペダルにキーボードの a を、二つ目のペダルにキーボードの b を... と設定しています。
一方、二行目の設定では、一つ目のペダルにメタキーの ctrl と キーボードの a を...となっています。

基本的に悩むことはないと思いますが、 メタ を使う場合と その他のキーボードを使う場合では、オプションが異なることに注意してください。

- なお念のための確認ですが、Enter, Space は -k の方です。

ここで一度設定を行うと、再起動しても設定は保存されます。
