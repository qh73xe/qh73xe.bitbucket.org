================================
Linux で HDD を認識する
================================

| Last Change: 11-Jan-2016.
| author : qh73xe
|

一般に日本で売られている USB HDD は windows 用にフォーマットがかけられています。
ただ、 Linux からこれにアクセスする場合、 Linux のファイルシステムフォーマットに変換しておいた方が何かと便利です。

このページでは、購入した HDD を Linux ようにフォーマットする方法について記述します。

ハードディスクのデバイス名を確認する
----------------------------------------

とりあえず、接続した HDD が Linux 上でどのように認識されているのかを確認します。
これには以下のコマンドを使用します。

.. code-block:: bash

   sudo fdisk -l

例えば私の場合以下の様な出力になります::

   モデル: ATA WDC WD5000AAKS-7 (scsi)
   ディスク /dev/sda: 500GB
   セクタサイズ (論理/物理): 512B/512B
   パーティションテーブル: msdos
   ディスクフラグ:

   番号  開始    終了   サイズ  タイプ   ファイルシステム  フラグ
    1    1049kB  525MB  524MB   primary  ext4              boot
    2    525MB   500GB  500GB   primary                    lvm


   モデル: WD My Book 1140 (scsi)
   ディスク /dev/sdf: 3001GB
   セクタサイズ (論理/物理): 4096B/4096B
   パーティションテーブル: gpt
   ディスクフラグ:

モデルの所に各種 HDD が記述され、ディスクの部分に
linux 上で認識されたディスク名が記述されます。
