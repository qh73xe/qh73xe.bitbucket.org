================================
Fedora
================================

:Last Change: 04-May-2016.
:Author: qh73xe
:Reference: https://getfedora.org/

このページでは Linux/Fedora に関する使い方を記述していきます。
私個人的には、Linux のディストリの中でもとくに気に入っている子です。
(愛称 ふぇどら様、多分金髪碧眼縦ロール... 何でもありません)

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   *
