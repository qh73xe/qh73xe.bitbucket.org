================================
Ubuntu 16.04: Xenial Xerus
================================

:Last Change: 04-May-2016.
:Author: qh73xe

2016年4月21日 に Ubuntu 16.04 がリリースされました。
ここでは私が Ubuntu 16.04 を使用し始めた以降に気が付いたことをメモしていきます。

.. contents::
   :depth: 2
   :local:

How To Upgrade to Ubuntu 16.04 LTS
===================================

:Reference: https://www.digitalocean.com/community/tutorials/how-to-upgrade-to-ubuntu-16-04-lts

とりあえず Ubuntu 16.04 への Upgrade 方法です。
なお、OS を アップグレードする場合、バックアップは重要ですが、
ここでは省略します。

まずは既存の OS にインストールしたアプリケーションをアップデートしておきましょう。

.. code-block:: bash
   :linenos:
   :caption: Upgrade Currently Installed Package
   :name: UCIP

   $ sudo apt-get update
   $ sudo apt-get upgrade
   $ sudo apt-get dist-upgrade

続いて、do-release-upgrade を導入します.
これは OS を upgrade するためのコマンドです。

.. code-block:: bash

   $ sudo apt-get install update-manager-core

- どうも最近の流行として OS をアップデートするコマンドを各ディストリ毎に用意しているようですね。

最後にこのコマンドを利用して OS のアップグレードを行います。

.. code-block:: bash
   :linenos:
   :caption: Use Ubuntu's do-release-upgrade Tool to Perform Upgrade
   :name: UUTPU

   $ sudo do-release-upgrade

暫くカチャカチャして、再起動が促されます。
再起動を行うと、OS の Upgrade が修了します。

Things To Do After Installing Ubuntu
==========================================

:Reference: http://www.webupd8.org/2016/04/things-to-do-after-installing-ubuntu-1604-lts-xenial-xerus.html

以下、Ubuntu 16.04 をインストールした後に行う作業をメモして起きます。

Install codecs, Java, and encrypted DVD playback
--------------------------------------------------

とりあえず、設定の面倒臭い系です。

.. code-block:: bash

   $ sudo apt install ubuntu-restricted-extras
   $ sudo apt install libavcodec-extra
   $ sudo apt install icedtea-8-plugin openjdk-8-jre
   $ sudo apt install libdvd-pkg

.. note:: apt コマンドに関して

   そういえば apt-get 系は apt で通るようになってますね。
   地味に文字数多かったので助かります。

Setup Cloud Sync (Dropbox or Google Drive), Skype
--------------------------------------------------

続いて オンライン系の設定です。

まず System Settings > Software & Updates から、"Other Softwar" のタブに移動します。
その後、"Canonical Partners" を有効にします。

.. code-block:: bash

   sudo apt install nautilus-dropbox
   sudo apt install gnome-control-center
   sudo apt install skype
   sudo apt install gtk2-engines-murrine:i386 gtk2-engines-pixbuf:i386

ここで Unity を使用している方は gnome-control-center を導入していることに
驚くかもしれません。これは ファイルシステム上で google drive を利用するために必要です。
これを導入した後に、端末上から gnome-control-center を機動し、
オンラインアカウントを設定します。

ここで google の設定を行うと google Drive を ファイラーから参照できるようになります。

Install GDebi GTK and Synaptic
--------------------------------------------------

ubuntu 16.04 から Ubuntu Software ではなく、
GNOME Software が利用されるようになりました。

しかし、 いくつか不具合があるようなので、
それに対応するライブラリを導入しておきます。

.. code-block:: bash

   $ sudo apt install synaptic
   $ sudo apt install gdebi


Install Ubuntu Make
--------------------------------------------------

Ubuntu Make は
IntelliJ IDEA, Eclipse, Android Studio 等の導入を簡単にするための
Ubuntu 公式の Command line tool です

.. code-block:: bash

   $ sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
   $ sudo apt update
   $ sudo apt install ubuntu-make


Install Tex
-------------------------------------------

Tex 環境、なくてもいいならいらないんですけどね。
でも仕方がないので導入します。
そのまま導入するとデカいので、日本語環境のみを導入します。

.. code-block:: bash

   $ sudo apt install texlive-lang-japanese texlive-luatex texlive-xetex texlive xzdectlmgr init-usertree

- tlmgr (TeX Live のパッケージ管理ツール) の実行には xzdec パッケージが必要です．
