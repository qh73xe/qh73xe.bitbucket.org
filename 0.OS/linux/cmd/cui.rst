================================
Linux: コマンドライン環境
================================

:Last Change: 13-May-2016.
:author: qh73xe

多分ですが、趣味で Linux を使用する人間って、大抵はあれが、便利だというより、
かっこいいから使っていると思うんですよね。
というか、私はそうです。

うん、Mac より断然。

で、Linux にカッコよさを求める人間って、結局 GUI より、 CUI の方が良い感じに思いますよね。
なんというか、昔映画でみた、「すーぱーはっかー」がいじっていたコンソール的な。
だれになんと言われようが、あれはかっこいいです。

このページでは、Linux の CUI 環境を色々と紹介していこうと思います。
目標は各種のツールの詳しい使い方を概説するというより、ツールがあること自体の紹介です。

- なお、このページで紹介するツールは基本的に vim ライクな操作が可能なもののみです。
    - そのため、人によっては結構使い難いかと。
    - これは完全に筆者の趣味です。

.. note:: 各種の詳しい使い方は別途ページを作成し、リンクでも張ると思います。

基本ツール
==============

ここでいう **基本** とは色んなツールのインターフェースという意味です。
CUI ツールの最大の魅力は、要は shell から使うコマンドでしょ？ということだと思っています。
で、そのコマンドまでのアクセス制御とか、どこに表示するのか（或いはしないのか）を設定するインターフェースを基本であると考えます。

tmux
--------------

:doc:`../../../Tools/tmux/main`

ターミナルマルチプレクサとは、誤解を恐れずに言えばCUI版ウィンドウマネージャのようなものです。
要は一つの端末の画面を複数に分て、それぞれに別のプロセスを走らせることが可能です。

tmuxinator
~~~~~~~~~~~~~~~~

tmuxinator は上記の tmux をメタ的に設定するためのツールです。
便利だという人間が周りには多いので紹介しておきます。

- http://tactosh.com/2014/01/tmux-window-pane-tmuxinator/
    - 私的には、tmux のコマンドを shell に書いて使用することを好むのですが...

ファイラー
=================

シェルコマンドを一番最初に習うときって、大抵 ls とか cd とか mkdir とか教わると思います。
アレらのコマンドって、確かに使い所多いですし、あれがあれば、大抵のファイル操作はできます。

でも、ファイル操作を行う時に、シェルコマンドと、ファイラー操作だったら、多分ファイラーを使いますよね？
大量のファイルを扱うときとか、正規表現を使いたいときではなく、日常使用では。
なんでかを考えると、多分、実際にファイルをみながら、どこに置きたいのかを考えながら（ディレクトリ設計をしながら）、作業をしたいからですよね。
そういうことを行えるという意味でファイラーって価値のあるツールなのだと思います。

で、CUI のファイラーがあれば最高ではないですか。
というわけで、2つのファイラーを紹介します。

Ranger: 高機能 CUI ファイラー
-------------------------------

:doc:`../../../Tools/ranger/main`

ファイラーとしての機能では vifm より優秀です。
特に、プレビュー機能は大変素晴らしく、 CUI 上で pdf や png のプレビューが確認できます。

- 注意がいるのは、 gnome-terminal では画像の表示を認めていないので、xterm とか別の端末を使う必要があることです。
    - gnome-terminal では アスキーアートで表示されます。

vifm: 2画面ファイラー
----------------------

vifm はその名の通り、 vim ライクなキーバインドで操作可能なファイラーです。
後述する ranger と比べ、機能は少ないですが、同時にシンプルで、且つ、2画面です。
そのため、ファイルのコピーや、移動を行う場合は、楽かもしれません。


音楽プレイヤー
===================

音楽管理もCUIから可能です。
というか、基本的に GUI よりも CUI の方が幸せになれる場合が多いのではないでしょうか？
GUI 音楽プレイヤーって基本重いですから。
「web 接続とかいらない」という方は、一考の余地があるかと思います（無駄なことしない分、音質が良い気がします）。

- なお、単純に音を鳴らすだけなら aplay コマンドが便利だと思います。

cmus
----------------------------

:doc:`../../../Tools/cmus/main`

cmus は、現状、私が最も気に入っている音楽プレイヤーです。
かなり多くの形式の音楽ファイルに対応しており、 Flac の再生も可能です。
わーい。

.. note:: fedora を使う場合

   cmus を fedora に導入する場合には github のソースからビルドした方がいいかと思います。

   - https://github.com/cmus/cmus

ブラウザ
================

ウェブブラウジングも CUI でできます。
GUI で同じことをするより、仕事をしている感がでるのでよいです。
欠点を言えば動画鑑賞をしにくいとか js の恩恵を受けにくい処だと思います。

w3m
--------------------

:doc:`../../../Tools/w3m/main`

私の感覚では一番色々なツールと連携できるブラウザかと思います。
設定次第で画像の表示もできますし、 vim との連携も可能です。

メーラー
======================

PC を使って生きていく場合、大抵、嫌々ながらメールを使用せざる終えません。
メールアドレスは勝手に自己増殖しますから（社内オリジナルなメアドとか欲くない...）管理が大変です。
まぁ、今更部外秘情報をメールで送る人間も少ないと思っておりますので、大体 gmail に一元化しています。

- 時々、明示的にダメって言われる

でここで紹介するのは、要は gmail を cui で操作するツールです。

mutt
-----------------------

http://www.mutt.org/
