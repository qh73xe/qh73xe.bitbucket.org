================================
Linux Brew
================================

| Last Change: 09-Sep-2015.
| author : qh73xe
|

このページでは Linuxbrew の紹介を行います．

これは何？？
=================

Linuxbrew は簡単に言ってしまえば Linux 版の HomeBrew です．
一見すると apt-get や yum でいいじゃんとか思いますが，
sudo 権限のないサーバーを扱っている場合には少し話が変わります．
HomeBrew は名前の通り，開発環境を Home 以下に作成します．
これが何を意味しているかというと sudo 権限のないユーザーも自由にアプリケーションの導入が可能になります．

なんと，サーバー管理者泣かせのツールなのでしょう．
わーい．

導入方法
============

事前の環境として以下のものが入っている必要があります．
これだけはなんとか管理者をそそのかして導入しましょう．

.. code-block:: bash

   $ sudo apt-get install build-essential curl git m4 ruby texinfo libbz2-dev libcurl4-openssl-dev libexpat-dev libncurses-dev zlib1g-dev


.. note:: これが最大の問題ですよねー

で本題のアプリケーションは ruby から導入します．

.. code-block:: bash

   $ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/linuxbrew/go/install)"

これは sudo いらないのです．
あとは shell の設定を書き加えます．
例えば bash の場合 .bash_profile に書くのが適当でしょう．

.. code-block:: bash
   :caption: .bash_profile
   :name: bash_profile

   export PATH="$HOME/.linuxbrew/bin:$PATH"
   export MANPATH="$HOME/.linuxbrew/share/man:$MANPATH"
   export INFOPATH="$HOME/.linuxbrew/share/info:$INFOPATH"
   export LD_LIBRARY_PATH="$HOME/.linuxbrew/lib:$LD_LIBRARY_PATH"

最後にうまく設定が言っているのかを確認します．:file:`.bash_profile` を読み込むため，一度ログインし直すか， :command:`source ~/.bash_profile` を行います．
その上で以下のコマンドを入力します．

.. code-block:: bash

   $ brew doctor

これで，幸せになりました．

アプリケーションの導入方法
==============================

Linuxbrew を使用してアプリケーションを導入するには以下のコマンドを使用します．

.. code-block:: bash

    brew install <入れたいアプリケーション>


