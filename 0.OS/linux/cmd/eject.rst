======================================
eject:リムーバブルメディアを取り出す
======================================

まぁ,タイトルの通りのコマンドです.
自宅のデスクトップを購入した際にはそもそもLinuxをメイン機にするとは夢にも思っていませんでした.
で,当時購入したPCは一体型のデスクトップPCです.
解像度もよく,CPU,メモリ,HDDも当時のトップクラスのやつです.
当時はまだwindows7が出たばかりでしたのでタッチパネルなんてあまり主流の機能ではなかったのですが,何故かハードとしては対応しています(そういえば,最近のLinuxは大抵デフォで反応しますね).
で,何を思ったのが一年ほど前に,これをLinuxにしてしまいました.
当時は,｢なんかよくわからないけどとりあえずマイクロソフトは敵である｣とか思っていましたから(Linuxを使用し始めると一度はかかる病ですね),当然デュアルブートとか考えず.
結果,付属のDVDドライバーが使えなくなりました.
いや,開けなかったのですよ.お恥ずかしい.

で,そんな時に助けられるのがこのコマンドですね.
とりあえず端末上で以下のコマンドを打つとDVDドライバーが開きます.

.. code-block:: bash

   $ eject

これで幸せになれますね.


