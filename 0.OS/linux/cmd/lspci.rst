==============================
lspci:PCのハード情報を調べる
==============================

時々必要になるコマンドです.買うときには気にしていても使っている最中にはマシンスペックとか気にしない人間なので.
そういう時に情報を吐き出させるコマンドですね.

.. code-block:: bash

   $ lspco
