================================
ネットワーク関連メモ
================================

| Last Change: 03-Mar-2016.
| author : qh73xe
|

ポート管理
==========================

/etc/sysconfig/iptables で設定を行う。

例えば 22 番ポートを解放するには以下の通り

.. code-block:: ini

   -A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT

当然設定をしたら iptables を再起動する

/etc/init.d/iptables restart
