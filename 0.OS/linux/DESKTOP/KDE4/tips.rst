================================
KDE4: Tips
================================

| Last Change: 10-Dec-2014.
| author : qh73xe

このページには KDE$ の設定に関してメモしておきます.

.. contents:: 目次
   :depth: 2

Konsole の フォントを変更する
==============================

KDE4 のターミナルでは GUI 上で固定幅フォントを変更することができないようです．

そのため，設定ファイルを直接編集します．

:file:`/home/qh73xw/.kde4/share/config/konsolerc`::

    defaultfont=[フォント名],10,-1,5,50,0,0,0,0,0

ディレクトリ名を英語にする
============================

OS を入れてとりあえず行う作業はディレクトリ名を英語に変更することではないでしょうか？

これには以下のコマンドを使用します．

.. code-block :: python

   $ LANG=C xdg-user-dirs-update --force

ただし，日本語ディレクトリは残るため，これを削除する必要があります．

.. code-block :: bash
   
   $ rmdir ~/デスクトップ
   $ rmdir ~/ダウンロード
   $ rmdir ~/ダウンロード
   $ rmdir ~/ドキュメント
   $ rmdir ~/テンプレート
   $ rmdir ~/音楽
   $ rmdir ~/画像
   $ rmdir ~/公開
