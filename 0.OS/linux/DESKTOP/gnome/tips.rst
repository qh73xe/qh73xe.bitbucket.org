================================
Tips
================================

| Last Change: 23-May-2016.
| author : qh73xe
|

.. contents::

google music を gnome から操作する
===========================================

私は個人的に google music のサービスを利用しています。
手持ちの音楽ファイル管理は勿論、とりあえず音を鳴らしたいときにとても便利ですし、
複数台の端末を使用する場合には本当に重宝します。

ただ、PC から使用している際にはブラウザを使用する必要がありますので、
これが少々不便です。

ここでは gnome の拡張として Media player indicator を使用し、
裏で、Nuvola Player / google music frame を利用することを考えます。

まず、 Nuvola Player に関しては現状 fedora では dnf から導入することはできませんので、レポジトリを追加します。
基本的には以下のサイトの指示に従えばよいでしょう。

https://tiliado.eu/nuvolaplayer/repository/

これで nuvolaplayer のレポジトリを追加できるようになるので、
ここから nuvolaplayer を導入できます。

続いて、これを gnome から利用するために
Media player indicator を導入します。

https://extensions.gnome.org/extension/55/media-player-indicator/

これで gnome のメニュー画面から google music を利用できるようになります。

