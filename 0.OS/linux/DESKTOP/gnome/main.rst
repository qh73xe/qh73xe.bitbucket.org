================================
Gnome3
================================

| Last Change: 23-May-2016.
| author : qh73xe
|

Gnome3 は Linux において、最もスタンダードなディスクトップ環境の一つです。
基本的な路線としては、軽量というよりも高機能を重視しているため、
好みが分かれるところです。

ただ、私個人としては何だかんだで一番好きです（何分派手好きなもので）。
なお、このページは今のところ gnome3 前提としているので注意してください。

.. toctree::
   :caption: ファイラー関係
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./Nautilus/*


.. toctree::
   :caption: ターミナル関係
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./gnomeTerminal/*

.. toctree::
   :caption: Extensions関係
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./gnomeExtensions/*

.. toctree::
   :caption: その他
   :glob:
   :titlesonly:
   :maxdepth: 1

   ./*
