================================
鍵設定を自動化する
================================

:Last Change: 23-May-2016.
:Author: qh73xe
:Reference:

通常 Linux では、 ssh 接続を行う際にパスワードが必要です。
これは毎回入力していたものです。

それ自身は情報漏洩的な問題で都合がよいのですが、
偶に面倒なときがあります。

このような際には、GNOME Keyring を使用すると便利な場合があります。
これはディスプレイ環境を gnome にしている場合、大抵はデフォルトで入っていると思います。

.. contents::
    :depth: 2
    :local:

SSH 鍵
================================

ssh キーを登録する場合、以下のコマンドを入力します。

.. code-block:: bash

   $ ssh-add ~/path/to/your-sshKey

そうすると、パスワードを聞かれるので、
それに答えます。

以後、ログアウトするまではこのパスワードが使用できます。

- 一度ログアウトをすると情報が消されるので注意してください。
- gnome のページに記述した通り、基本、GNOME Keyring での説明をしています。
      - Mac の場合、同名で同じ機能を持ちならが、異るコマンドなので注意してください。

GNOME Keyring と Git 連携
================================

git での認証で、 gnome-keyring と連携をしたい場合、
ヘルパーを導入する必要があります。

とりあえず、コンパイルに必要なライブラリを導入します。
そのうえで github からソースを入手し適当にコンパイルしておきます。
最後に、git の設定でこれを利用するようにすればよいです。
こちらも一度認証を通してしまえば、入力が不要になります。


.. code-block:: bash

   sudo dnf install libgnome-keyring-devel
   git clone https://github.com/shugo/git-credential-gnomekeyring.git
   cd git-credential-gnomekeyring/
   make
   git config --global credential.helper /path/to/git-credential-gnomekeyring
