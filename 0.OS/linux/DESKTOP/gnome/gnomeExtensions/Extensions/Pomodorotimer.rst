================================
A Pomodoro timer
================================

:Last Change: 23-May-2016.
:Author: qh73xe
:Reference: http://gnomepomodoro.org/

Pomodoro テクニックという時間管理方法があります。
これは 25 分間何かに集中して作業を行い、 15 分間休憩をするというのを
一連の作業として行っていく仕事方法です。

ここで紹介する Exception はこの Pomodoro 用のタイマーを Gnome に付与します。

.. contents:: 目次

導入方法
==========================

導入は dnf や apt から可能です。

For Fedora23::
   .. code-block:: bash

      $ sudo dnf install gnome-shell-extension-pomodoro

For Ubuntu 16.4::
   .. code-block:: bash

      $ sudo apt-get install gnome-shell-pomodoro

