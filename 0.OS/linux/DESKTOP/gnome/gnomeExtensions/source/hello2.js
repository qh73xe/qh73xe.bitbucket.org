#! /usr/bin/env gjs
const Clutter = imports.gi.Clutter;
Clutter.init(null);
let stage = new Clutter.Stage();
stage.connect("destroy", Clutter.main_quit);
stage.title = "Hello World";
stage.show();
Clutter.main();
