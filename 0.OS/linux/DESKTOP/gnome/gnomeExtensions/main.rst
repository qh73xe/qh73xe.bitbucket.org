================================
Gnome Extensions
================================

:Last Change: 23-May-2016.
:Author: qh73xe

このページでは Gnome Extensions について記述します。

.. toctree::
   :glob:
   :caption: おすすめの Extensions
   :maxdepth: 1
   :titlesonly:

   ./extensions/*


.. toctree::
   :glob:
   :caption: Gnome Extensions Tips
   :maxdepth: 1
   :titlesonly:

   ./*
