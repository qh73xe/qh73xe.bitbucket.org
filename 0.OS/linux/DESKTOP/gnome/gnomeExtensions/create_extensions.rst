================================
Create Gnome Extensions
================================

:Last Change: 23-May-2016.
:Author: qh73xe
:Reference: https://wiki.gnome.org/Projects/GnomeShell/Extensions/StepByStepTutorial#fromScratch

このページでは、 Gnome Extensions を自作する方法に関してまとめておきます。

gjs
==========================

まず、そもそも論ですが、 Gnome-Extensions は JS を使用して記述します。
C でも、 Java でも python でもありません。

JS は一般に WEB アプリでよく使われる言語であってローカルアプリで使用する感じは
あまりないように思いますが、 Gnome Shell は JS です。

一般的な動的言語、例えば python とか ruby とか perl ではそれ独自のプラットホームを持ちますが
JS はそうではありません。
そのため、gnome shell では 独自プラットホームとして gjs を用意しています。

.. note:: プラットホーム

   あまり難しいことを考えず説明すると、例えば python を実行する時には適当な
   python スクリプトを記述して python hoge.py のような感じで実行すると思います。

   しかし、 JS の場合、コマンドラインで実行するための環境を独自に用意はしていない
   （一般に WEB ブラウザ側で処理をするので） ので、 Gnome 独自の実行環境を作成し
   ているという話です。

Hello World
--------------------

とりあえず、 gjs を使用してみましょう。

以下に gtk を使用して hello World を表示するサンプルコードを示します。

.. literalinclude:: ./source/hello.js
   :linenos:
   :language: js
   :caption: hello.js

これを実行するには端末から以下の操作を行います。

.. code-block:: bash

   $ gjs-console hello.js

無事実行される場合、 window が立ち上がり、 Hello world が表示されると思います。

clutter
==================

Gnome-Extensions を作成するために、
もう一つ知っておかないといけないものに clutter があります。

これは、C programming API で gtk のように GUI を作成する部分を担います。

これを使用したサンプルを以下に示します。

.. literalinclude:: ./source/hello2.js
   :linenos:
   :language: js
   :caption: hello2.js
今度はタイトルの部分が Hello World になっているウインドウが表示されると思います。
