================================
Linux: OS
================================

:Last Change: 14-May-2016.
:Author: qh73xe

.. meta::
    :description: Linux 関連の情報をまとめた Index ページです。
    :keywords: Linux, linux

Linux いいですよね.
私の PC 歴的には win 行って Linux 行って帰ってこれなくなってしまった人間です.
楽だし,自由だし,かっこいいし, 人様の善意で成り立ってる世界な感じがしてとても好きです.
このページでは Linux を使用していく段階での交々なメモを残して行きます.

ちなみに私の場合研究室サーバーが debian で, 個人的には fedora (SUSE に浮気しまし
たがやっぱり fedora に戻りました) を使っています．

あと, デスクトップ環境には gnome3 を利用しています.
以下, 一応,区分けをしておりますが, あまり整理していないので参考程度に


Distribution:
-----------------------------

ディストリビューション固有の話は以下に書きます.

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   DIST/fedora/*
   DIST/opensuse/*

DeskTop System:
-----------------------------

デスクトップシステム関連の話は以下の書きます

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   DESKTOP/gnome/main
   DESKTOP/KDE4/*

Tips
-----------------------------

コマンド周りのことや, Linux を使っていての TIPs など

.. toctree::
   :glob:
   :titlesonly:
   :maxdepth: 1

   cmd/*
   others/*
